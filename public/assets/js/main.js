jQuery(function($){
    $('.sidebar_menu li a').on('click',function(){
      $(this).toggleClass('down');
      $(this).parent().siblings().children().removeClass('down');
      $(this).parent().find('ul').slideToggle();
      $(this).parent().siblings().find('ul').slideUp();
    });


var rocMenu = function() {
    var ww = $(window).width();
    // Desktop
    if  (ww >= 1500) {
        $('body').addClass('menu-desktop');
            $('body').addClass('opened');
            $('body').removeClass('colapsed');
            $('body').removeClass('offcanvas');
                    $(".ham_menu").click(function(){
                        $('body').toggleClass('opened');
                        $('body').toggleClass('colapsed');
                    });
        $('body').removeClass('menu-laptop');            
        $('body').removeClass('menu-mobile');
    }
    // Laptop
    if  (ww < 1500 && ww >= 768) {
        $('body').removeClass('menu-desktop');
        $('body').addClass('menu-laptop');
            $('body').removeClass('opened');
            $('body').addClass('colapsed');
            $('body').removeClass('offcanvas');
                $(".ham_menu").click(function(){
                        $('body').toggleClass('colapsed');
                        $('body').toggleClass('opened');
                    });
        $('body').removeClass('menu-mobile');
    }
    // Laptop
    if  (ww < 768) {
        $('body').removeClass('menu-desktop');        
        $('body').removeClass('menu-laptop');
        $('body').addClass('menu-mobile');
            $('body').removeClass('opened');
            $('body').removeClass('colapsed');
            $('body').addClass('offcanvas');
                    $(".ham_menu").click(function(){
                        $('body').toggleClass('offcanvas');
                    });
    }
    
//  auto scrollbar for left menu   
var winHeight = $(window).outerHeight();
var topMenuHeight = winHeight - 250 + 'px';
if (winHeight < 650){
	$('.fixed-bottom-menu').css({
         position:'relative',
         bottom:'auto'
        });
        $('.sidebar_menu').mCustomScrollbar();
        $('.sidebar_menu .mCustomScrollBox').css('overflow','visible');
        $('.sidebar_menu .mCSB_container').css('overflow','visible');
    }
    else {
        $('.top_menu > ul').css('min-height',topMenuHeight);
    }   
    
};

rocMenu();
$(window).on('resize', function(){
    rocMenu();
});



// sticky left inner menu 
if($(window).outerWidth() > 767){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 0) {          
            $('.inner-left-menu').css({
              top:'80px',
              possition:'absolute'              
            });
        } else {
            $('.inner-left-menu').css('top','auto');
        }
    });
};

  $( "#datepicker" ).datepicker({
        responsive: true
    });

  // $('.site_search input').on('focus',function(){
  
  //     $(this).parent().css('width','364px');
  //     $('.srch_drop').show();
  // });



$(".site_search input").click(function(e){
    $(this).parent().css('width','364px');
    $(".srch_drop").show();
     e.stopPropagation();
});

$(".srch_drop").click(function(e){
    $(this).parent().css('width','364px');
    e.stopPropagation();
});

$(document).click(function(){
    $(".srch_drop").hide();
});




  $('#example').DataTable( {
        "paging":   false,
        "search": false,
        "info":     false
    } );


  $('header nav.nav .list-inline-item span.mdi-chat').on('click',function(){
      $('.direct-chat-warning').css('height','360px');
  });
 
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});

  function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});


function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
             $('#cross1').css('visibility', 'visible');
            $('#imagePreview1').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview1').hide();
            $('#imagePreview1').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload1").change(function() {
    readURL1(this);
});

function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#cross2').css('visibility', 'visible');
            $('#imagePreview2').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview2').hide();
            $('#imagePreview2').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
} 

$("#imageUpload2").change(function() {
    readURL2(this);
}); 

function readURL3(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#cross3').css('visibility', 'visible');
            $('#imagePreview3').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview3').hide();
            $('#imagePreview3').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload3").change(function() {
    readURL3(this);
}); 

function readURL4(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#cross4').css('visibility', 'visible');
            $('#imagePreview4').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview4').hide();
            $('#imagePreview4').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload4").change(function() {
    readURL4(this);
}); 




function readURL5(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#cross5').css('visibility', 'visible');
            $('.video-preview').css('visibility', 'visible');
        
            var $source = $('#videoSource');
  $source[0].src = URL.createObjectURL(this.files[0]);
  $source.parent()[0].load();
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#videoUpload").change(function() {
    readURL5(this);
}); 


$(document).on("change", "#videoUpload", function(evt) {
      $('#cross5').css('visibility', 'visible');
  var $source = $('#videoSource');
  $source[0].src = URL.createObjectURL(this.files[0]);
  $source.parent()[0].load();

});






$('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})

$('#radioBtn1 a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})


});

/* anup */
$(document).ready(function(){
  $('.user-type ul li a').click(function(){
    $('li').removeClass("active");
    $(this).parent().addClass("active");
});
});

/* Manab */
$(function(){
    $('.header-search-options li').click(function(){
        $('.header-search-options li').removeClass('active');
        $(this).addClass('active');
    });

    $('.dis-com').click(function(){
      $('display-company').addClass('dis-com-none');
    });

    $('.very-mob-no').click(function(){
      $(".very-btn").show();
      $(".very-mob").hide();
    });

});

$(".close-header-search").click(function(){
    $(".srch_drop").css('display','none');
    $(".site_search").css('width','182px');
    $(".site_search input").val('');
});
    
// switch for displayCompany > landlord > profile > about
// var check9='N';
// $('#displayCompany a').on('click', function(){
//     var sel = $(this).data('title');
//     var tog = $(this).data('toggle');
//     $('#'+tog).prop('value', sel); 
    
//     if(check9==sel){
//       console.log('no change');
//     }   
//     else{
//     $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
//     $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
//   $("#toggleDisplayCompany").toggleClass('hidden');
//   $("#toggleDisplayCompany2").toggleClass('hidden');
//   check9= sel;
//   console.log(sel);
//     }
// });





var check3='Y';
$('#confirmemployment a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel); 
    
    if(check3==sel){
      console.log('no change');
    }   
    else{
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
  $("#toggleconfirmemployment").toggleClass('hidden');
 
  check3= sel;
  console.log(sel);
    }
});


var check4='Y';
$('#companyname a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel); 
    
    if(check4==sel){
      console.log('no change');
    }   
    else{
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
  $("#togglecompanyname").toggleClass('hidden');
 
  check4= sel;
  console.log(sel);
    }
});


var check5='Y';
$('#correctperson a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel); 
    
    if(check5==sel){
      console.log('no change');
    }   
    else{
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
  $("#togglecorrectperson").toggleClass('hidden');
 
  check5= sel;
  console.log(sel);
    }
});


var check6='Y';
$('#tenancyduration a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel); 
    
    if(check6==sel){
      console.log('no change');
    }   
    else{
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
  $("#toggletenancyduration").toggleClass('hidden');
  $("#toggletenancyduration1").toggleClass('hidden');
  
 
  check6= sel;
  console.log(sel);
    }
});


var check7='Y';
$('#rentontime a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel); 
    
    if(check7==sel){
      console.log('no change');
    }   
    else{
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
  $("#togglerentontime").toggleClass('hidden');
  
 
  check7= sel;
  console.log(sel);
    }
});


var check1='N';
$('#rentmorethan a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel); 
    
    if(check1==sel){
      console.log('no change');
    }   
    else{
      $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
 
  
 
  check1= sel;
  console.log(sel);
    }
});


var check2='N';
$('#problem a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel); 
    
    if(check2==sel){
      console.log('no change');
    }   
    else{
      $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
 
  
 
  check2= sel;
  console.log(sel);
    }
});








// switch for preferences > landlord > listing > rental details

$('#preferences a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    // $('#family').addAttr('value', sel); 
    $('#'+tog).prop('value', sel); 
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
  $(".togglepreferences").toggleClass('hidden');
  console.log(sel);
  

});




// // switch for preferences > landlord > listing > rental details
// var check='N';
// $('#preferences a').on('click', function(){
//     var sel = $(this).data('title');
//     var tog = $(this).data('toggle');
//     $('#'+tog).prop('value', sel); 
    
//     if(check==sel){
//       console.log('no change');
//     }   
//     else{
//     $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
//     $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
//   $(".togglepreferences").toggleClass('hidden');
//   check= sel;
//   console.log(sel);
//     }
// });


// switch for displayCompany > landlord > profile > stripe
$('#goCardLess a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
    console.log(sel);
});

// switch for displayCompany > landlord > profile > goCardLess
$('#stripeCard a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
    console.log(sel);
});


// switch for > renter > profile > other info
$('#SmokingYesNo a').on('click', function(){
  var sel = $(this).data('title');
  var tog = $(this).data('toggle');
  $('#'+tog).prop('value', sel);
  
  $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
  $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
  
});
$('#DisabilityYesNo a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
    
});
$('#DSSBenefitYesNo a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
});




var rentontime1='N';

$('#rentontime1 a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel); 
    if(rentontime1==sel){
      console.log('no change1');
    } 
    else{

      $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
      $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $(".togglerentontime1").toggleClass('hidden');

    rentontime1= sel;

    console.log(sel);
    }
       
       
});


var rentontime10='Y';

$('#rentontime1 a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel); 
    if(rentontime10==sel){
      console.log('no change1');
    } 
    else{

      $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
      $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $(".togglerentontime10").toggleClass('hidden');
    rentontime10= sel;

    console.log(sel);
    }
       
       
});







$('#protectAccountYesNo a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
	
});

var z=4;
// add multiple row for document upload 
$(".addMoreDocument").click(function(){
	var moredoc = '<div class="row">'
  +'<div class="col-xl-4">'
	  +'<div class="form-group">'
		+'<select class="form-control"  name="doc'+z+'">'
			+'<option>Select Document</option>'
			+'<option value="1">Passport</option>'
			+'<option>Utility bill</option>'
			+'<option>Bank / Card Statement</option>'
			+'<option>Education Certificate</option>'
			+'<option>Professional Certificate</option>'
			+'<option>Accredited Membership</option>'
			+'<option>Employment Document</option>'
			+'<option>Business Financial Statement</option>'
			+'<option>Other</option>'
		+'</select>'
		+'</div>'
	+'</div>'	
  +'<div class="col-xl-4">'
	  +'<div class="form-group">'
		+'<input type="text" id="dd" class="form-control" placeholder="Document Name">'
		+'</div>'
	+'</div>'
  +'<div class="col-xl-4">'
  +'<div class="row">'
  +'<div class="col-8">'
	  +'<div class="form-group">'
		 +'<div class="custom-file" id="customFile" lang="es">'
				+'<input type="file"  class="custom-file-input" id="exampleInputFile" aria-describedby="fileHelp">'
				+'<label class="custom-file-label" for="exampleInputFile">'
				  
				+'</label>'
		+'</div>'
		+'</div>'
	+'</div>'
	+'<div class="col-4">'
	+'<a href="javascript:;" class="btn btn-danger btn-sm removeDocument"><i class="fa fa-times"></i></a>'
 +'</div>'	 
 +'</div>'                  
 +'</div>';
  $(".doc-upload-wrapper").append(moredoc);
  //$(".doc-upload-wrapper select").attr("onChange","this.onChangeDoc");
  $(".doc-upload-wrapper select").on("change", function(event) { 
    // onChangeDoc(this);
    this.onChangeDoc();
  });
  $(".doc-upload-wrapper .custom-file-input").on("change", function(event) { 
    onChangePic(this);
  });

  
  z++;
	});
	
$(".removeDocument").click(function(){
  $(this).parent().parent().remove();
  

});

// add multiple row for membership - servicePro, persona profile, Accreditation
$(".addMoreMembership").click(function(){
        var moreMem = '<div class="row">'
                            +'<div class="col-xl-4">'
                               +'<div class="form-group">'
                                  +'<select id="aa" class="form-control">'
                                    +'<option>Select Member Organization</option>'
                                    +'<option>Membership 1</option>'
                                    +'<option>Membership 2</option>'
                            +'        <option>Membership 3</option>'
                            +'      </select>'
                            +'    </div>'
                            +'  </div>'
                            +'    <div class="col-xl-4">'
                            +'        <div class="form-group">'
                            +'            <input type="text" class="form-control" placeholder="Document number">'
                            +'        </div>'
                            +'    </div>'
                            +'    <div class="col-xl-4">'
                            +'        <div class="form-group">'
                            +'          <div class="input-group">'
                            +'            <div class="input-group-prepend">'
                            +'              <div class="input-group-text"><i class="fa fa-calendar"></i></div>'
                            +'            </div>'
                            +'            <input type="text" id="dd" class="form-control" placeholder="Valid till">'
                            +'          </div>'
                            +'        </div>'
                            +'      </div>'
                            +'</div>';
        $(".accreditationWrapper").append(moreMem);
        });
        
// add more other info servicePro, persona profile, Other Info
$(".addMoreOtherInfo").click(function(){
	var moreOI = '<hr style="margin-top: 0">'
                            +'<div class="row">'
                            +'    <div class="col-xl-6">'
                            +'        <div class="form-group">'
                            +'            <select id="aa" class="form-control">'
                            +'                <option>Select </option>'
                            +'            </select>'
                            +'        </div>'
                            +'    </div>'
                            +'    <div class="col-xl-6">'
                            +'        <div class="form-group">'
                            +'            <input type="text" class="form-control" placeholder="Provider Name">'
                            +'        </div>'
                            +'    </div>'
                            +'    <div class="col-xl-4">'
                            +'        <div class="form-group">'
                            +'            <input type="text" class="form-control" placeholder="Policy Number">'
                            +'        </div>'
                            +'    </div>'
                            +'    <div class="col-xl-4">'
                            +'        <div class="form-group">'
                            +'            <div class="input-group">'
                            +'                <div class="input-group-prepend">'
                            +'                    <div class="input-group-text"><i class="mdi mdi-currency-gbp"></i></div>'
                            +'                </div>'
                            +'                <input type="number" class="form-control" placeholder="Policy Amount">'
                            +'            </div>'
                            +'        </div>'
                            +'    </div>'
                            +'    <div class="col-xl-4">'
                            +'        <div class="form-group">'
                            +'            <div class="input-group">'
                            +'                <div class="input-group-prepend">'
                            +'                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>'
                            +'                </div>'
                            +'                <input type="text" id="dd" class="form-control" placeholder="Valid till">'
                            +'            </div>'
                            +'        </div>'
                            +'    </div>'
                            +'</div>';
	$(".otherInfoPLI").append(moreOI);
	});

// add more other info servicePro, persona profile, Other Info
$(".addMoreRefContact").click(function(){
	var moreRC = '<div class="row">'
                    +'    <div class="col-xl-4">'
                    +'        <div class="form-group">'
                    +'            <input type="text" id="dd" class="form-control" placeholder="Contact Name">'
                    +'        </div>'
                    +'    </div>'
                    +'    <div class="col-xl-4">'
                    +'        <div class="form-group">'
                    +'            <input type="text" id="dd" class="form-control" placeholder="Phone number">'
                    +'        </div>'
                    +'    </div>'
                    +'    <div class="col-xl-4">'
                    +'        <div class="form-group">'
                    +'            <input type="text" id="dd" class="form-control" placeholder="Email">'
                    +'        </div>'
                    +'    </div>'
                    +'</div>';
	$(".refContactWrapper").append(moreRC);
	});


// two view of Profile Cards, for 3 type of users 
$(".custom_tab li:first-child").click(function(){    
   $(".active-about-tab").removeClass('hidden');
   $(".active-connect-tab").addClass('hidden');
});


$(".custom_tab li:nth-child(2)").click(function(){    
   $(".active-about-tab").addClass('hidden');
   $(".active-connect-tab").removeClass('hidden');
});
$("#connect2").click(function(){    
  $(".active-about-tab").addClass('hidden');
  $(".active-connect-tab").removeClass('hidden');
});
$(".custom_tab li:nth-child(3)").click(function(){    
   $(".active-about-tab").addClass('hidden');
   $(".active-connect-tab").removeClass('hidden');
});

// profile card height when only text is coming 


if ($(window).outerWidth() > 991){
var tabConHeight = function() {
var docHeight = $(window).outerHeight();
var contentheight = docHeight - 127 + 'px';
    $(".active-about-tab").css({
    'max-height' : contentheight,
    'overflow':'auto'
});
$(".active-about-tab").mCustomScrollbar();
};
tabConHeight();
}


            var bigimage = $("#big");
            var thumbs = $("#thumbs");
            //var totalslides = 10;
            var syncedSecondary = true;
          
            bigimage
              .owlCarousel({
              items: 1,
              slideSpeed: 2000,
              nav: true,
              autoplay: true,
              dots: false,
              loop: true,
              responsiveRefreshRate: 200,
              navText: [
                '<img src="../../assets/images/prev-arrow.png" ></img>',
                '<img src="../../assets/images/next-arrow.png" ></img>'
              ]
            })
              .on("changed.owl.carousel", syncPosition);
          
            thumbs
              .on("initialized.owl.carousel", function() {
              thumbs
                .find(".owl-item")
                .eq(0)
                .addClass("current");
            })
              .owlCarousel({
              items: 4,
              dots: false,
              nav: true,
              navText: [
                '<img src="../../assets/images/previous.png" ></img>',
                '<img src="../../assets/images/next.png" ></img>'
              ],
              smartSpeed: 200,
              slideSpeed: 500,
              slideBy: 4,
              responsiveRefreshRate: 100
            })
              .on("changed.owl.carousel", syncPosition2);
          
            function syncPosition(el) {
              //if loop is set to false, then you have to uncomment the next line
              //var current = el.item.index;
          
              //to disable loop, comment this block
              var count = el.item.count - 1;
              var current = Math.round(el.item.index - el.item.count / 2 - 0.5);
          
              if (current < 0) {
                current = count;
              }
              if (current > count) {
                current = 0;
              }
              //to this
              thumbs
                .find(".owl-item")
                .removeClass("current")
                .eq(current)
                .addClass("current");
              var onscreen = thumbs.find(".owl-item.active").length - 1;
              var start = thumbs
              .find(".owl-item.active")
              .first()
              .index();
              var end = thumbs
              .find(".owl-item.active")
              .last()
              .index();
          
              if (current > end) {
                thumbs.data("owl.carousel").to(current, 100, true);
              }
              if (current < start) {
                thumbs.data("owl.carousel").to(current - onscreen, 100, true);
              }
            }
          
            function syncPosition2(el) {
              if (syncedSecondary) {
                var number = el.item.index;
                bigimage.data("owl.carousel").to(number, 100, true);
              }
            }
            // $(function () {
            //   $('#datetimepicker4').datetimepicker();
            //   });

          
            thumbs.on("click", ".owl-item", function(e) {
              e.preventDefault();
              var number = $(this).index();
              bigimage.data("owl.carousel").to(number, 300, true);
            });

            $('.option-card').click(function() {
              $(".choice").removeClass("choice");
              $(this).addClass("choice");
              //console.log($(this).attr("name"));
              var a=$(this).attr("name");
               //alert(a);
            
             $(".tick").removeAttr("src");
            $("#"+a).attr("src", "../../assets/images/tick1.png");
            });
            
    // var date_input=$('input[id="date"]'); //our date input has the name "date"
    // date_input.datepicker({
    //   format: 'mm/dd/yyyy',
    //   todayHighlight: true,
    //   autoclose: true,
    // });

   
      $('#agree').change(function () {
          if (this.checked) {
          
          document.getElementById("disable").style.pointerEvents = "auto";
          document.getElementById("disable1").style.pointerEvents = "auto";
          jQuery('#disable').css('opacity', '1');
          jQuery('#disable1').css('opacity', '1');
          $("#savenext").removeAttr( "disabled" );
          
          
          }
          
          else{
          document.getElementById("disable").style.pointerEvents = "none";
          document.getElementById("disable1").style.pointerEvents = "none";
          jQuery('#disable').css('opacity', '0.5');
          jQuery('#disable1').css('opacity', '0.5');
          $("#savenext").attr( "disabled", true);
          
          
          }
          
      });


      


    
// js end



       
            
               
            

           
          
          