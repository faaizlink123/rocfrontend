$( document ).ready(function() {
	// Tab-Pane change function
	 var tabChange = function(){
		 var tabs = $('#1st > a');
		 var active = tabs.filter('.active');
		 var next = active.next('a').length? active.next('a') : tabs.filter(':first-child');
		 // Use the Bootsrap tab show method
		 next.tab('show')
	 }
	 // Tab Cycle function
	 var tabCycle = setInterval(tabChange, 3000)
	 // Tab click event handler
	 $(function(){
		 $('#1st a').click(function(e) {
			 e.preventDefault();
			 // Stop the cycle
			 clearInterval(tabCycle);
			 // Show the clicked tabs associated tab-pane
			 $(this).tab('show')
			 // Start the cycle again in a predefined amount of time
			 setTimeout(function(){
				 tabCycle = setInterval(tabChange, 3000)
			 }, 30000);
		 });
	 });

	 //2nd
	 // Tab-Pane change function
	 var tabChange = function(){
		var tabs = $('#2nd > a');
		var active = tabs.filter('.active');
		var next = active.next('a').length? active.next('a') : tabs.filter(':first-child');
		// Use the Bootsrap tab show method
		next.tab('show')
	}
	// Tab Cycle function
	var tabCycle = setInterval(tabChange, 3000)
	// Tab click event handler
	$(function(){
		$('#2nd a').click(function(e) {
			e.preventDefault();
			// Stop the cycle
			clearInterval(tabCycle);
			// Show the clicked tabs associated tab-pane
			$(this).tab('show')
			// Start the cycle again in a predefined amount of time
			setTimeout(function(){
				tabCycle = setInterval(tabChange, 3000)
			}, 30000);
		});
	});

	 //3rd
	 // Tab-Pane change function
	 var tabChange = function(){
		var tabs = $('#3rd > a');
		var active = tabs.filter('.active');
		var next = active.next('a').length? active.next('a') : tabs.filter(':first-child');
		// Use the Bootsrap tab show method
		next.tab('show')
	}
	// Tab Cycle function
	var tabCycle = setInterval(tabChange, 3000)
	// Tab click event handler
	$(function(){
		$('#3rd a').click(function(e) {
			e.preventDefault();
			// Stop the cycle
			clearInterval(tabCycle);
			// Show the clicked tabs associated tab-pane
			$(this).tab('show')
			// Start the cycle again in a predefined amount of time
			setTimeout(function(){
				tabCycle = setInterval(tabChange, 3000)
			},30000);
		});
	});

 });