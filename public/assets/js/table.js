jQuery(function($){
$(document).ready(function() {
 $('#billing_table2').DataTable();
 $('#billing_table1').DataTable();
 $('#billing_table3').DataTable();
 $('#billing_table4').DataTable();
//  $('#task_list').DataTable();
 var table = $('#billing_table').DataTable( {        
     //buttons: [ 'excel',csv, 'print' ]
     //dom: 'Bfrtip',
     buttons: [
         {
             extend:    'print',
             text:      '<i class="fa fa-print"></i>&nbsp;Print',
             titleAttr: 'Print'
         },
         
         {
             extend:    'csv',
             text:      '<i class="fas fa-file-excel"></i>&nbsp;CSV',
             titleAttr: 'CSV'
         }            
     ],
     columnDefs: [
{ orderable: false, targets: -1 }
]
 } );

 table.buttons().container()
     .appendTo( '.dataTables_length label' );
});


$('#task_list').DataTable( {
    bPaginate: false,
    bLengthChange: false,
    bFilter: true,
    bInfo: false,
    bSort:true,
    bAutoWidth: false,
    columnDefs: [
    {orderable: false, targets:[0,1,2,3,6,7,-1]}
    ],
    "oLanguage": { 
    "sSearch":""
    }
    });
    
    $('.dataTables_filter').appendTo( '.dt_search' );
    setTimeout(function (){
    $('.dataTables_filter').find('input').attr('placeholder','Search here...');
    },2000);

});