// toggle left nav 
$(function(){
  $(".navToggler").click(function(){
     $('body').toggleClass('hideleftnav');   
      });
  });
  
  
  if ($(window).outerWidth() < 768) {
  $(document).ready(function(){
      $('body').addClass('hideleftnav');
  });
  };
  
  
  $(document).ready(function () {
      $(window).scroll(function () {
          didScroll = true;
          if ($(this).scrollTop() > 500)
          {
              $('.scroll_To_Top').fadeIn(); 
              $('.main-header2').addClass('hdrshadow')
          }
          else
          {
              $('.scroll_To_Top').fadeOut();
          }
      });
      $('.scroll_To_Top').click(function () {
          $('html, body').animate({scrollTop: 0}, "slow");
          return false;
      });
  });
  
  $(document).ready(function () {
      $(window).scroll(function () {
          didScroll = true;
          if ($(this).scrollTop() > 100)        
          {
              $('.main-header2').addClass('hdrshadow')
              $('.main-header2').css('padding','5px 15px')
              $('.leftnav').css('top','60px')
          }
          else
          {
              $('.main-header2').removeClass('hdrshadow')
              $('.main-header2').css('padding','20px 15px')
              $('.leftnav').css('top','70px')
          }
      });    
  });
  

  document.addEventListener('scroll', function (event) {
    if (event.scrollTop() > 100) { // or any other filtering condition        
      $('.main-header2').addClass('hdrshadow')
      $('.main-header2').css('padding','5px 15px')
      $('.leftnav').css('top','60px')
    }
}, true
  )
  
  jQuery(function ($) {
      $('.sidebar_menu li a').on('click', function () {
          $(this).toggleClass('down');
          $(this).parent().siblings().children().removeClass('down');
          $(this).parent().find('ul').slideToggle();
          $(this).parent().siblings().find('ul').slideUp();
      });    
  
      $(".site_search input").click(function(e){
        $(this).parent().css('width','364px');
        $(".srch_drop").show();
         e.stopPropagation();
    });
    
  
    $(".srch_drop").click(function(e){
      $(this).parent().css('width','364px');
      e.stopPropagation();
  });
  
  
      $(document).click(function () {
          $(".srch_drop").hide();
          $(".site_search").css('width','182px');
      });
  
      $(".close-header-search").click(function(){
        $(".srch_drop").css('display','none');
        $(".site_search").css('width','182px');
        $(".site_search input").val('');
    });
      
      
  
      $(document).ready(function () {
          $('[data-toggle="tooltip"]').tooltip();
      });
  
  
  });
  
  
  
  $(document).ready(function() {
    var owl = $('#testimonial');
    owl.owlCarousel({
    margin: 0,
    items:3,
    nav: false,
    autoplay:true,
    autoplayTimeout:7000,
    animate: 'slide',	
    loop: true,
          dots:true,        
    responsiveClass:true,
    navText: [
      "<i class='fa fa-chevron-left'></i>",
      "<i class='fa fa-chevron-right'></i>"
      ],
    responsive: {	  
      0: {
      items: 1
      },
      991: {
      items: 2
      },
      1200: {
      items: 3
      }	  
    }
    });
  });
  
  //$(document).ready(function() {
  //  var owl = $('#partners');
  //  owl.owlCarousel({
  //	margin: 30,
  //	items:6,
  //	nav: false,
  //	autoplay:false,	
  //	animate: 'slide',
  //        autoWidth:true,	
  //	responsiveClass:true,
  //	navText: [
  //	  "<i class='fa fa-chevron-left'></i>",
  //	  "<i class='fa fa-chevron-right'></i>"
  //	  ],
  //	responsive: {
  //	  0: {
  //		items: 2
  //	  },
  //	  600: {
  //		items: 2
  //	  },
  //	  768: {
  //		items: 3
  //	  },
  //	  1000: {
  //		items: 4
  //	  },	  
  //          1200: {
  //		items: 6
  //	  }	  
  //	}
  //  });
  //});
  
  
  $(document).ready(function () {
      var owl = $('#endtoendpropmanagement');
      owl.owlCarousel({
          loop: false,
          nav: false,
          margin: 10,
          items: 1,
          dots:false
      });
      owl.on('mousewheel', '.owl-stage', function (e) {
        if (e.deltaY > 0) {
            owl.trigger('prev.owl.carousel', [1]);
        } else {
            owl.trigger('next.owl.carousel', [1]);
        }
          e.preventDefault();
      });
  });


// ===========================
// Scroll Magic & Tween Slider
// ===========================
var wh = window.innerHeight,
// Landing page main sections
$innerS1 = $('.innerS1'),
$innerS2_1 = $('.innerS2-1'),
$innerS2_2 = $('.innerS2-2'),
$innerS2_3 = $('.innerS2-3'),
$innerS3 = $('.innerS3'),
$innerS4 = $('.innerS4'),
$innerS5 = $('.innerS5'),
$innerS6 = $('.innerS6'),
$innerS7 = $('.innerS7'),
// Mac & title
$title = $('#tween-title'),
$mac = $('#mac'),
// 9 Mac text box
$txt1 = $('.txt1'),
$txt2 = $('.txt2'),
$txt3 = $('.txt3'),
$txt4 = $('.txt4'),
$txt5 = $('.txt5'),
$txt6 = $('.txt6'),
$txt7 = $('.txt7'),
$txt8 = $('.txt8'),
$txt9 = $('.txt9');
// Fixed Mac 9 Mac srcs
$src1 = $('.src1'),
$src2 = $('.src2'),
$src3 = $('.src3'),
$src4 = $('.src4'),
$src5 = $('.src5'),
$src6 = $('.src6'),
$src7 = $('.src7'),
$src8 = $('.src8'),
$src9 = $('.src9');
// Init
var ctrl = new ScrollMagic.Controller({ globalSceneOptions: { triggerHook: 'onLeave'} });
// All main sections - create scene
$("section").each(function() { new ScrollMagic.Scene({ triggerElement:"section", duration: '75%'}).setPin(this).addTo(ctrl); });

// Mac & Title intro animation Tween
var macIntro = TweenMax.from($mac, 1, {opacity: 0, xPercent: 0, yPercent: 0, left:'0', top:'50%', ease: Cubic.easeOut });
var titleIntro = TweenMax.from($title, 1, {scale:1, opacity: 1, xPercent: 0, yPercent: 0, left:'0', top:'60px', ease: Cubic.easeOut });

// Mac intro animation Timeline
var section_1 = new TimelineMax();
section_1
  .from($title, 0.5, {opacity: 0, scale: 0.1, yPercent: -200, xPercent: -120, top:'100vh', ease: Cubic.easeInOut})
  .to($innerS1, 0.5, {opacity: 0.5, yPercent: -70, scale: 0.7},'0')
  .from($mac, 1, {yPercent: 200, xPercent: 120,top:'100%', ease: Cubic.easeInOut})
  .to($innerS1, 1, {opacity: 0.5, yPercent: -70, scale: 0.7},'0').duration(4);
// Mac back to stylesheet position
new ScrollMagic.Scene({ duration: '70%'}).setTween(section_1).triggerElement($('body')[0]).addTo(ctrl);

// Window Scroll
$(document).ready(function() {
  var section = $('.scrollmagic-pin-spacer').height();
  function onScroll() {
    //console.log('scroll top = ' + $(window).scrollTop());
    if (($(window).scrollTop() > section/2) && ($(window).scrollTop() < section * 6 - section/2 - section/5)) {
        $('#tween-title,#mac').show();
    }  else {
        $('#tween-title,#mac').hide();
    }
    // mac section-2_1, section-2_2, section-2_3
    if (($(window).scrollTop() >= section * 4 + section/2 + section/3)) {
      $('.slide,.txt').removeClass('active');
      $('#slide9,.t9').addClass('active');
    } else if (($(window).scrollTop() >= section * 4 + section/2)) {
      $('.slide,.txt').removeClass('active');
      $('#slide8,.t8').addClass('active');
    } else if (($(window).scrollTop() >= section * 4 + section/2 - section/3)) {
      $('.slide,.txt').removeClass('active');
      $('#slide7,.t7').addClass('active');
    } else if (($(window).scrollTop() >= section * 3 - section/7 + section/3)) {
      $('.slide,.txt').removeClass('active');
      $('#slide6,.t6').addClass('active');
    } else if (($(window).scrollTop() >= section * 3 - section/7)) {
      $('.slide,.txt').removeClass('active');
      $('#slide5,.t5').addClass('active');
    } else if (($(window).scrollTop() >= section * 3 - section/7 - section/3)) {
      $('.slide,.txt').removeClass('active');
      $('#slide4,.t4').addClass('active');
    } else if (($(window).scrollTop() >= section * 2 - section/2 + section/3)) {
      $('.slide,.txt').removeClass('active');
      $('#slide3,.t3').addClass('active');
    } else if (($(window).scrollTop() >= section * 2 - section/2)) {
      $('.slide,.txt').removeClass('active');
      $('#slide2,.t2').addClass('active');
    } else if (($(window).scrollTop() >= section * 2 - section/2 - section/3)) {
      $('.slide,.txt').removeClass('active');
      $('#slide1,.t1').addClass('active');
    } else {
      $('.slide,.txt').removeClass('active');
    }
  }
  window.addEventListener('scroll', onScroll, false);
});