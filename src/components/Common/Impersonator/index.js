import React from "react";
import cookie from "react-cookies";

const ImpersonatingUserExist = props => {
  const exit = () => {
    cookie.save("token", cookie.load("user_token_main"));

    window.location.reload();
  };

  return (
    <p>
      {props.children} <button onClick={exit}>Exit</button>
    </p>
  );
};

export default ImpersonatingUserExist;
