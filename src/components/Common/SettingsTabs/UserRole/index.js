import React from "react";
import { Collapse, CardBody, Card, CardHeader } from "reactstrap";
import _ from "lodash";
import { message } from "antd";
import "./style.scss";

class UserRole extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      openCollapse: {
        openSupport: false,
        openAccountant: false,
        "openPortfolio Manager": false,
        openViewer: false
      },
      userRoles: []
    };
  }

  componentDidMount() {
    this.setState({ userRoles: this.props.userRoles });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return { userRoles: nextProps.userRoles };
  }

  toggleCollapse = key => {
    console.log(key);
    let openCollapseData = this.state.openCollapse;
    Object.keys(openCollapseData).map((colKey, i) => {
      if (colKey === key) {
        openCollapseData[colKey] = true;
      } else {
        openCollapseData[colKey] = false;
      }
    });

    this.setState({ openCollapse: openCollapseData });
    this.forceUpdate();
  };

  inviteUser = async data => {
    this.props.contextData.startLoading();
    let role = data.role;
    let email = data.event.target[0].value;

    const inviteUserQuery = await this.props.client.mutate({
      mutation: this.props.UserRoleQuery.inviteUser,
      variables: { name: role.name, email: email, roleId: role._id }
    });

    if (_.get(inviteUserQuery, "data.sendInvitation.success")) {
      this.props.contextData.endLoading();
      message.success(`Invitation to ${email} was sent successfully!`);
    } else {
      this.props.contextData.endLoading();

      message.error(
        `An error occured - ${_.get(
          inviteUserQuery,
          "data.sendInvitation.message"
        )}`
      );
    }
  };

  render() {
    let { openCollapse, userRoles } = this.state;

    return (
      <>
        <div className="role">
          <div className="">
            <div className="">
              <div className="box box-form-wrapper">
                <div className="row">
                  <div className="col-lg-12">
                    <h3 className="mt-4 mb-4 text-center text_head">
                      Invite or enable a new user to access your account on
                      rentoncloud
                    </h3>
                    <div
                      className="d-flex"
                      style={{ marginBottom: "35px", marginTop: "35px" }}
                    >
                      <img
                        src={"/assets/images/icon-notification3.png"}
                        alt="img"
                      />
                      <div className="details">
                        <h4>Rajiv Singh</h4>
                        <label className="admin admin__orange">
                          ADMINISTRATOR
                        </label>
                        <br></br>
                        <label className="email">rajivsingh@gmail.com</label>
                      </div>
                    </div>

                    {userRoles.map((role, i) => {
                      return (
                        <Card style={{ marginBottom: "1rem" }}>
                          <CardHeader
                            onClick={() =>
                              this.toggleCollapse(`open${role.name}`)
                            }
                            data-type="collapseLayout"
                            className="card__title"
                          >
                            <div className="d-flex">
                              <img
                                src={"/assets/images/icon-notification2.png"}
                                alt="img"
                              />
                              <div className="details">
                                <h4>{role.name}</h4>
                                <span>{role.title}</span>
                                <br></br>
                                <label className="content">
                                  {" "}
                                  {role.description}
                                </label>
                              </div>
                              <div className="details__icons">
                                <i className="mdi mdi-chevron-down"></i>
                              </div>
                              <div className="details__icons active">
                                <i className="mdi mdi-chevron-up"></i>
                              </div>
                            </div>
                          </CardHeader>
                          <Collapse isOpen={openCollapse[`open${role.name}`]}>
                            <CardBody className="no__padd">
                              <div className="wrapper_table">
                                <div className="d-flex">
                                  <form
                                    onSubmit={e => {
                                      e.preventDefault();
                                      this.inviteUser({
                                        role,
                                        event: e
                                      });
                                    }}
                                    className="form-inline"
                                  >
                                    <input
                                      type="email"
                                      className="form-control cardinput"
                                      placeholder="Enter Email Address"
                                      required
                                    />

                                    <input
                                      className="btn btn-warning text-white mr-5"
                                      type="submit"
                                      value="Invite User"
                                    />
                                  </form>
                                </div>
                              </div>
                            </CardBody>
                          </Collapse>
                        </Card>
                      );
                    })}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default UserRole;
