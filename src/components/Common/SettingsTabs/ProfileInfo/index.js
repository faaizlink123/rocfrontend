import React, { useState, useEffect } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import About from "./About";
import BankCard from "./BankAndCard";
import Connect from "./Connect";
import "react-tabs/style/react-tabs.css";

const ProfileInfo = props => {
  const [currentActiveTab, setActiveTab] = useState(
    props.currentActiveAboutTab
  );

  useEffect(() => {
    setActiveTab(props.currentActiveAboutTab);
  }, [props.currentActiveAboutTab, props.setActiveTab]);

  return (
    <Tabs selectedIndex={currentActiveTab}>
      <TabList>
        <Tab onClick={() => setActiveTab(0)}>About</Tab>
        <Tab onClick={() => setActiveTab(1)}>Connect</Tab>
        <Tab onClick={() => setActiveTab(2)}>Bank & card</Tab>
      </TabList>

      <TabPanel>
        <About {...props} />
      </TabPanel>
      <TabPanel>
        <Connect {...props} />
      </TabPanel>
      <TabPanel>
        <BankCard {...props} />
      </TabPanel>
    </Tabs>
  );
};

export default ProfileInfo;
