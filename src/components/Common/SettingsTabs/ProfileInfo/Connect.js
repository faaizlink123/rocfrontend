import React, { useState } from "react";
import LoqateAddress from "../../../../config/AddressAutoCompleteLoqate";
import LoqateAddressFull from "../../../../config/LoqateGetFullAddress";
import { Formik, Form, Field } from "formik";

import TelegramLoginButton from "react-telegram-login";
import SuggestionDropdown from "./Downshift";
import "./stylesConnect.scss";

const Connect = props => {
  const findAddress = async address => {
    let fullAddress = await LoqateAddressFull(address.Id);
    let completeAddress = fullAddress.Items[0];
    console.log(completeAddress);
    setLoqateData({
      fullAddress: completeAddress.Label,
      addressLine1: completeAddress["Company"]
        ? completeAddress["Company"]
        : completeAddress["Line1"],
      addressLine2: completeAddress["Company"]
        ? `${completeAddress["Line1"]}, ${completeAddress["Line2"]}`
        : completeAddress["Line2"],
      city: completeAddress["City"],
      state: completeAddress["Province"],
      zip: completeAddress["PostalCode"],
      country: completeAddress["CountryName"]
    });
  };

  const saveData = formData => {
    props.saveConnectProfileData(formData);
  };

  const handleTelegramResponse = data => {
    // console.log("telegram response", data);
  };

  const [loqateData, setLoqateData] = useState({});

  console.log(loqateData);
  return (
    <>
      <div class="connect_page">
        <div class="container">
          <div class="main_header">
            <h3 class="mt-4 mb-4 text-center">
              Connect your Social profile to build your online social
              reputation.
            </h3>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="notify_settings d-flex flex__middle">
                <i class="fab fa-facebook-f"></i>
                <div className="details">
                  <h4>
                  <i class="fa fa-check-circle" aria-hidden="true"></i> Connected with Facebook{" "}
                  </h4>
                  <div className="btns">
                    <input
                      type="button"
                      className="btn btn-primary"
                      value="Reconnect"
                    />
                    <input
                      type="button"
                      className="btn btn-danger"
                      value="Disconnect"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="notify_settings d-flex flex__middle">
                <i class="fab fa-linkedin-in"></i>
                <div className="details">
                  <h4>
                    Connect with LinkedIn{" "}
                    {/* <i class="fa fa-check-circle" aria-hidden="true"></i> */}
                  </h4>
                  <div className="btns">
                    {/* <input type="button" className="btn btn-primary" value="Reconnect" /> <input type="button" className="btn btn-danger" value="Disconnect" /> */}
                    <input
                      type="button"
                      className="btn btn-primary"
                      value="Connect"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="notify_settings d-flex flex__middle">
                <i class="fab fa-twitter"></i>
                <div className="details">
                  <h4>
                    Connect with Twitter{" "}
                    {/* <i class="fa fa-check-circle" aria-hidden="true"></i> */}
                  </h4>
                  <div className="btns">
                    {/* <input type="button" className="btn btn-primary" value="Reconnect" /> <input type="button" className="btn btn-danger" value="Disconnect" /> */}
                    <input
                      type="button"
                      className="btn btn-primary"
                      value="Connect"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="notify_settings d-flex flex__middle">
                <TelegramLoginButton
                  dataOnauth={handleTelegramResponse}
                  botName="ftestapp_2248bot"
                />
                {/* <i class="fab fa-telegram-plane"></i> */}
                <div className="details">
                  <h4>
                    Connect with Telegram{" "}
                    {/* <i class="fa fa-check-circle" aria-hidden="true"></i> */}
                  </h4>
                  <div className="btns">
                    {/* <input type="button" className="btn btn-primary" value="Reconnect" /> <input type="button" className="btn btn-danger" value="Disconnect" /> */}
                    <input
                      type="button"
                      className="btn btn-primary"
                      value="Connect"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <h4 class="mt-4 mb-4">Current Address</h4>
          <div class="form-group">
            <SuggestionDropdown
              LoqateAddress={LoqateAddress}
              findAddress={findAddress}
              loqateData={loqateData}
            />
            {/* <input
              name="search"
              type="text"
              id="search"

              // autoComplete="none"
              class="form-control"
              placeholder="Start typing your address and select from option"
            /> */}
          </div>
          <Formik
            enableReinitialize
            initialValues={loqateData}
            onSubmit={(values, { validateForm, setSubmitting }) => {
              setSubmitting(true);
              console.log(values);
              saveData(values);
            }}
          >
            {({ isSubmitting, setFieldValue, values, errors }) => (
              <Form>
                <div class="form-group">
                  <Field
                    type="text"
                    id="label"
                    name="fullAddress"
                    class="form-control"
                    placeholder="Full Address"
                    disabled
                  />
                </div>
                <div class="form-group">
                  <Field
                    type="text"
                    id="line1"
                    name="addressLine1"
                    class="form-control"
                    placeholder="Address Line 1*"
                    disabled
                  />
                </div>
                <div class="form-group">
                  <Field
                    type="text"
                    id="line2"
                    name="addressLine2"
                    class="form-control"
                    placeholder="Address Line 2"
                    disabled
                  />
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <Field
                        type="text"
                        id="city"
                        name="city"
                        class="form-control"
                        placeholder="City *"
                        disabled
                      />
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <Field
                        type="text"
                        id="zip"
                        name="zip"
                        class="form-control"
                        placeholder="Post Code/ZIP *"
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <Field
                        type="text"
                        id="province"
                        name="state"
                        class="form-control"
                        placeholder="Country / State / Region"
                        disabled
                      />
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <Field
                        type="text"
                        id="country"
                        name="country"
                        class="form-control"
                        placeholder="Country *"
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div class="row mb-1 mt-4">
                  <div class="col-md-6"></div>
                  <div class="col-md-6">
                    <button class="btn btn-primary btn-block">
                      {" "}
                      Save &amp; next{" "}
                    </button>
                  </div>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </>
  );
};

export default Connect;
