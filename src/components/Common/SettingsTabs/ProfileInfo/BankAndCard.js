import React, { useState, useRef } from "react";
import MyNumberInput from "../../../../config/CustomNumberInput";
import { Formik, Form, Field } from "formik";
import moment from "moment";
import { message } from "antd";
import _ from "lodash";
import CreditCardInput from "react-credit-card-input";

import "./stylesBankCard.scss";

const BankCard = props => {
  let refContainer = useRef();

  let currentYear = moment().format("YYYY");

  const [cardNumber] = useState();
  const [cardErrorMsg, setCardError] = useState("");

  const saveCardDetails = () => {
    let formValues = _.get(refContainer, "current.state.values");

    let ar = Object.keys(formValues);
    if (cardErrorMsg === "") {
      if (
        !ar.includes("cardNumber") &&
        !ar.includes("cvc") &&
        !ar.includes("expireMonth") &&
        !ar.includes("expireYear")
      ) {
        message.error("Please fill all the required fields!");
      } else {
        formValues.expireMonth = Number(formValues.expireMonth);
        formValues.expireYear = Number(formValues.expireYear);

        props.saveCardDetails(formValues);
      }
    }
  };

  const cardError = e => {
    setCardError(e);
  };

  return (
    <>
      <Formik
        ref={refContainer}
        enableReinitialize
        initialValues={{ ...props.bankDetails }}
        onSubmit={(values, { validateForm, setSubmitting }) => {
          setSubmitting(true);
          props.saveBankData(values);
        }}
      >
        {({ isSubmitting, setFieldValue, values, errors }) => (
          <Form>
            <div className="card_wrap">
              <div className="header">
                <h3 className="mt-4 mb-4 text-center font-weight-400">
                  {" "}
                  We do not take any payment without your authorisation of
                  transaction. This detail is used for profile verification and
                  once you are ready to rent, it can further be used for direct
                  debit rental payment authorisation.
                </h3>
              </div>
              <div className="row">
                <div className="col-sm-6">
                  <p className="label__title">Bank *</p>
                </div>
                <div className="col-sm-6 text-right">
                  <div className="btn btn-warning">Edit</div>
                </div>
              </div>

              <div className="form-row">
                <div className="form-group col-md-6">
                  <label>Enter Bank Code</label>

                  <MyNumberInput
                    placeholder="Bank Code/Sort code"
                    className={
                      errors && errors["bankCode"]
                        ? "form-control error__field_show"
                        : "form-control"
                    }
                    format="##-##-##"
                    mask="_"
                    value={values.bankCode}
                    onValueChange={val => setFieldValue("bankCode", val.value)}
                  />
                </div>
                <div className="form-group col-md-6">
                  <label>Enter Account Number</label>
                  <MyNumberInput
                    placeholder="Bank Account Number"
                    className={
                      errors && errors["accountNumber"]
                        ? "form-control error__field_show"
                        : "form-control"
                    }
                    format="########"
                    mask="_"
                    value={values.accountNumber}
                    onValueChange={val => {
                      setFieldValue("accountNumber", val.value);
                    }}
                  />
                </div>
              </div>
              <p className="label__title">Card</p>
              <div className="card_main col-lg-10 card__bg">
                <div className="d-flex">
                  <button
                    type="button"
                    disabled
                    className="btn card__edit--btn"
                  >
                    <i className="fas fa-pencil-alt"></i>
                  </button>
                  <button
                    onClick={saveCardDetails}
                    type="button"
                    className="btn card__save--btn"
                  >
                    <i className="far fa-save"></i>
                  </button>
                </div>

                <div className="form-row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label className="card__label">Card Number</label>

                      <CreditCardInput
                        cardNumberInputProps={{
                          defaultValue: cardNumber,
                          onError: e => cardError(e),
                          onChange: e => {
                            let value = e.target.value;
                            let finalval = value
                              ? value.split(/[0-9]+/).join("") +
                                value.match(/(\d+)/g).join("")
                              : "";

                            setCardError("");
                            setFieldValue(
                              "cardNumber",
                              String(finalval.trim())
                            );
                          }
                        }}
                        cardExpiryInputProps={{
                          hidden: true
                        }}
                        cardCVCInputProps={{
                          hidden: true
                        }}
                        inputClassName="full__width__card_input"
                      />
                    </div>
                  </div>
                </div>

                <div className="form-row">
                  <div className="col-4 justify-content-left align-self-center">
                    <div className="form-group">
                      <label>Expiry Date</label>
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <Field
                        component="select"
                        name={`expireMonth`}
                        className="form-control card__select"
                        required
                      >
                        <option selected disabled>
                          {" "}
                          MM{" "}
                        </option>
                        {Array.from({ length: 12 }).map((y, i) => {
                          return <option value={i + 1}>{i + 1}</option>;
                        })}
                      </Field>
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <Field
                        component="select"
                        name={`expireYear`}
                        required
                        className="form-control card__select"
                      >
                        <option selected disabled>
                          {" "}
                          YYYY{" "}
                        </option>
                        {Array.from({ length: 30 }).map((y, i) => {
                          return (
                            <option value={Number(currentYear) + i}>
                              {Number(currentYear) + i}
                            </option>
                          );
                        })}
                      </Field>
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-8 justify-content-left align-self-center">
                    <div className="form-group">
                      <label>Enter CVV Number</label>
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <MyNumberInput
                        placeholder="XXXX"
                        type="password"
                        className="form-control card__select"
                        format="####"
                        value={values.cvc}
                        onValueChange={val =>
                          setFieldValue("cvc", String(val.floatValue))
                        }
                      />
                    </div>
                  </div>
                  <div className="col-12 mt-4">
                    <p className="text-toc">
                      By providing your card details, you agree to Stripe's{" "}
                      <a
                        rel="noopener noreferrer"
                        target="_blank"
                        href="https://stripe.com/gb/legal"
                      >
                        Terms &amp; Conditions
                      </a>{" "}
                    </p>
                  </div>
                </div>
              </div>

              <div className="row pay_sec mt-3">
                <div className="col-8">
                  <div className="pay_sec_inner">
                    <img src={"#"} alt="img" className="mr-2" />
                    <span>GoCardless</span> <i className="fas fa-info"></i>
                  </div>
                </div>
                <div className="col-4"></div>
              </div>
              <div className="row mb-1 mt-4">
                <div className="col-md-6 btns">
                  <a
                    className="btn btn-outline-primary btn-block mb-3"
                    href="/"
                  >
                    {" "}
                    Skip at this moment{" "}
                  </a>
                </div>
                <div className="col-md-6 btns">
                  <button type="submit" className="btn btn-primary btn-block">
                    {" "}
                    Save &amp; next{" "}
                  </button>
                </div>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default BankCard;
