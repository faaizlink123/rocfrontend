import React, { useState, useEffect } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import CustomInput from "../../../../config/FormikInput";
import { Datepicker } from "react-formik-ui";
import AboutFormSchema from "../../../../config/FormSchemas/About";
import MyNumberInput from "../../../../config/CustomNumberInput";
import UploadProfilePic from "../../../../config/DropzoneAll";
import _ from "lodash";
import moment from "moment";
import "./stylesAbout.scss";

const About = props => {
  const [showCompany, setCompanyStatus] = useState(
    props.userDataAbout.isCompany
  );

  let [profilePicture, uploadProfilePicture] = useState({
    filename: "",
    avatar: _.get(props.userDataAbout, "avatar")
  });

  let [userDataAbout, setUserData] = useState({ ...props.userDataAbout });

  useEffect(() => {
    setCompanyStatus(_.get(props.userDataAbout, "isCompany"));
    uploadProfilePicture({
      filename: "",
      avatar: _.get(props.userDataAbout, "avatar")
    });
    setUserData(props.userDataAbout);
  }, [props.userDataAbout]);

  const sendOTP = number => {
    // axios
    //   .post(
    //     "https://verificationapi-v1.sinch.com/verification/v1/verifications",
    //     {
    //       identity: { type: "number", endpoint: "+918349218516" },
    //       method: "sms"
    //     },
    //     {
    //       headers: {
    //         Authorization: `Application d8c028de37b14dc3ba1614bcd1ab39ed:${process.env.REACT_APP_SINCH_API_KEY}`,
    //         "X-Timestamp": date.toISOString()
    //       }
    //     }
    //   )
    //   .then(res => {
    //     console.log("res", res);
    //   });
  };
  return (
    <Formik
      enableReinitialize
      initialValues={{ ...userDataAbout }}
      validationSchema={AboutFormSchema}
      onSubmit={(values, { validateForm, setSubmitting }) => {
        setSubmitting(true);
        values.uploadAvatar = false;
        if (profilePicture.avatar.includes(";base64")) {
          values.uploadAvatar = true;
          values.avatar = profilePicture;
        }
        values.dob = moment(values.dob).format();
        values.isCompany = showCompany;
        values.phoneNumber = String(values.phoneNumber);
        props.updateProfileAbout(values);
      }}
    >
      {({ isSubmitting, setFieldValue, values, errors }) => (
        <Form>
          <div className="tab__details">
            <h3>Let’s start with basic information first</h3>
            <div className="row">
              <div className="col-lg-6">
                <UploadProfilePic
                  key={"profilePic"}
                  getValue={val => uploadProfilePicture(val)}
                />
                <img
                  src={profilePicture.avatar}
                  alt="avatar"
                  className="profile__img--box"
                />
              </div>
              <div className="col-lg-6">
                <div className="form-group">
                  <label className="tab__deatils--label">
                    First Name <span>*</span>
                  </label>
                  <CustomInput
                    placeholder="Enter first name"
                    type="text"
                    name="firstName"
                    errors={errors}
                  />
                  <ErrorMessage
                    name="firstName"
                    component="div"
                    className="all__errors"
                  />
                </div>
                <div className="form-group">
                  <label className="tab__deatils--label">
                    Middle Name <span>*</span>
                  </label>
                  <Field
                    placeholder="Enter middle name"
                    type="text"
                    name="middleName"
                    className="tab__deatils--input"
                    {...props}
                  />
                </div>
                <div className="form-group">
                  <label className="tab__deatils--label">
                    Last Name <span>*</span>
                  </label>
                  <CustomInput
                    placeholder="Enter last name"
                    type="text"
                    name="lastName"
                    errors={errors}
                  />
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-6">
                <div className="form-group yes__no--toogle">
                  <label className="tab__deatils--label">
                    Display Company ?
                  </label>
                  <div class="btn-group">
                    <a
                      href
                      className={
                        showCompany
                          ? "btn btn-success btn-md active activeShowCompany"
                          : "btn btn-success btn-md active notActiveShowCompany"
                      }
                      data-toggle="company"
                      data-title="Y"
                      onClick={() => setCompanyStatus(true)}
                    >
                      YES
                    </a>
                    <a
                      href
                      className={
                        !showCompany
                          ? "btn btn-light btn-md no_activeShowCompany"
                          : "btn btn-light btn-md no_notActiveShowCompany"
                      }
                      data-toggle="company"
                      data-title="N"
                      onClick={() => setCompanyStatus(false)}
                    >
                      NO
                    </a>
                  </div>
                </div>
                {showCompany && (
                  <>
                    <div className="form-group">
                      <label className="tab__deatils--label">
                        Company Registration Number <span>*</span>
                      </label>
                      <Field
                        placeholder="Company Registration Number"
                        type="text"
                        name="companyRegistrationNumber"
                        className="tab__deatils--input"
                      />
                    </div>
                  </>
                )}

                <div className="form-group">
                  <label className="tab__deatils--label">
                    Mobile Number <span>*</span>
                  </label>
                  <MyNumberInput
                    placeholder="Your Contact Number"
                    className={
                      errors && errors["phoneNumber"]
                        ? "tab__deatils--input error__field_show"
                        : "tab__deatils--input"
                    }
                    format="(###) ###-####"
                    mask="_"
                    value={values.phoneNumber}
                    onValueChange={val =>
                      setFieldValue("phoneNumber", val.floatValue)
                    }
                  />
                </div>

                <div className="form-group">
                  <label className="tab__deatils--label">
                    Date of Birth <span>*</span>
                  </label>
                  <Datepicker
                    name="dob"
                    todayButton="Today"
                    showYearDropdown
                    dateFormat="dd/MM/yyyy"
                    showMonthDropdown
                    dropdownMode={"select"}
                    disabledKeyboardNavigation={false}
                    className={
                      errors && errors["dob"]
                        ? "tab__deatils--input error__field_show"
                        : "tab__deatils--input"
                    }
                  />
                </div>

                <div className="form-group">
                  <label className="tab__deatils--label">
                    Select Nationality <span>*</span>
                  </label>
                  <Field
                    component="select"
                    name="nationality"
                    className={
                      errors && errors["nationality"]
                        ? "tab__deatils--input error__field_show"
                        : "tab__deatils--input"
                    }
                  >
                    <option selected disabled>
                      Select Nationality
                    </option>
                    {props.nationalities &&
                      props.nationalities.map((n, i) => {
                        return (
                          <option value={n.nationality}>{n.nationality}</option>
                        );
                      })}
                  </Field>
                </div>
              </div>

              <div className="col-lg-6">
                {showCompany && (
                  <div className="form-group">
                    <label className="tab__deatils--label">
                      Company Name <span>*</span>
                    </label>
                    <Field
                      placeholder="Company Name"
                      type="text"
                      name="companyName"
                      className="tab__deatils--input"
                    />
                  </div>
                )}

                <div className="form-group">
                  <label className="tab__deatils--label">
                    Email Address <span>*</span>
                  </label>
                  <Field
                    placeholder="Email Address"
                    type="email"
                    name="email"
                    className="tab__deatils--input"
                    disabled
                  />
                </div>

                <div className="form-group verify__marg">
                  <div className="btn btns__orange" onClick={sendOTP}>
                    {" "}
                    Verify your mobile number{" "}
                  </div>
                </div>

                <div className="form-group">
                  <label className="tab__deatils--label">
                    Select Gender <span>*</span>
                  </label>
                  <Field
                    component="select"
                    name="gender"
                    className="tab__deatils--input"
                  >
                    <option selected disabled>
                      --------------Select Gender-----------------
                    </option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                    <option value="other">Other</option>
                  </Field>
                </div>
              </div>
            </div>

            <div className="row row__marg--top">
              <div className="col-lg-6"></div>
              <div className="col-lg-6">
                <div className="form-group">
                  <button type="submit" className="btn btns__blue">
                    Save & next
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default About;
