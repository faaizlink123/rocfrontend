import React from "react";
import FormQuery from "../../../../config/queries/account";
import PasswordResetSchema from "../../../../config/FormSchemas/resetPassword";
import _ from "lodash";
import showNotification from "../../../../config/Notification";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { withApollo } from "react-apollo";

const Security = props => {
  const resetPassword = async formData => {
    const queryResponse = await props.client.mutate({
      mutation: FormQuery.resetPassword,
      variables: { ...formData }
    });

    if (
      !_.isEmpty(queryResponse.data.changePassword) &&
      _.get(queryResponse, "data.changePassword.success")
    ) {
      showNotification(
        "success",
        "Password updated!",
        "Your password has been reset successfully!"
      );
    } else {
      showNotification(
        "error",
        "An Error Occured",
        _.get(queryResponse, "data.changePassword.message")
      );
    }
  };

  return (
    <div className="security_setting">
      <div className="">
        <div className="row">
          <div className="col-xl-7">
            <div className="box box-form-wrapper">
              <Formik
                validationSchema={PasswordResetSchema}
                onSubmit={(values, { setSubmitting }) => {
                  setSubmitting(true);
                  resetPassword(values);
                }}
              >
                {({ isSubmitting, errors }) => (
                  <>
                    <Form>
                      <div className="form-group">
                        <label className="labels__global">Current Password</label>
                        <div className="input-group">
                          <div className="input-group-prepend">
                            <div className="input-group-text">
                              <i className="fa fa-unlock"></i>
                            </div>
                          </div>
                          <Field
                            type="password"
                            name="currentPassword"
                            className="form-control"
                          />
                        </div>
                        <ErrorMessage
                          name="currentPassword"
                          component="div"
                          className="all__errors"
                        />
                      </div>

                      <div className="form-group">
                      <label className="labels__global">Enter New Password</label>
                        <div className="input-group">
                          <div className="input-group-prepend">
                            <div className="input-group-text">
                              <i className="fa fa-lock"></i>
                            </div>
                          </div>

                          <Field
                            type="password"
                            name="newPassword"
                            className="form-control"
                          />
                        </div>
                        <ErrorMessage
                          name="newPassword"
                          component="div"
                          className="all__errors"
                        />
                      </div>

                      <div className="form-group">
                      <label className="labels__global">Enter New Password Again</label>
                        <div className="input-group">
                          <div className="input-group-prepend">
                            <div className="input-group-text">
                              <i className="fa fa-lock"></i>
                            </div>
                          </div>

                          <Field
                            type="password"
                            name="newPasswordConfirm"
                            className="form-control"
                          />
                        </div>
                        <ErrorMessage
                          name="newPasswordConfirm"
                          component="div"
                          className="all__errors"
                        />
                      </div>
                      <div className="form-group mb-30">
                        <div className="row">
                          <div className="col-8 justify-content-left align-self-center">
                            Protect your account with an extra layer of
                            security. You'll be required to enter both your
                            password and an authentication code to log in to
                            your account.
                          </div>
                          <div></div>
                        </div>
                      </div>
                      <div className="form-group text-right">
                        <button type="submit" className="btn btn-primary">
                          Save
                        </button>
                      </div>
                    </Form>
                  </>
                )}
              </Formik>
            </div>
          </div>
          <div className="col-xl-5">
            <div className="blue__box">
              <div className="text-center mb-3">
                <div className="col-12 verify_img">
                  <img src={"/assets/images/img-2-step-auth.png"} alt="img" />
                </div>
              </div>
              <div className="lr__gap">
              <div className="row mb-1">
                <div className="col-12">
                  <h4>Protect your account with 2-Step Verification</h4>
                  <p>
                    Each time you sign in to your account, you'll need your
                    password and a verification code.
                    <a
                      href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&amp;hl=en_IN"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      Learn more...
                    </a>
                  </p>
                </div>
              </div>
              <div className="row mb-1">
                <div className="col-3">
                  <img src={"/assets/images/auth1.png"} alt="img" />
                </div>
                <div className="col-9">
                  <p>
                    <strong>Step 1:</strong> Download Google Authenticator App
                    from app-store,{" "}
                  </p>
                  <p>
                    <strong>Step 2:</strong> Scan bar code and Step 3: enter
                    verification code.
                  </p>
                </div>
              </div>
              <div className="row mb-3">
                <div className="col-3">
                  <img src={"/assets/images/auth2.png"} alt="img" />
                </div>
                <div className="col-9">
                  <p>
                    After enabling, each time you login to your account, you
                    will need to enter password along with code generated by
                    this app.
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-12 text-center">
                  <button className="btn btn-block btn-warning btns__warning w60 m-auto">
                    Get Started
                  </button>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withApollo(Security);
