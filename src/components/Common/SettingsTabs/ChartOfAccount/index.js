import React, { useState, useRef, useEffect } from "react";
import ReactToPrint from "react-to-print";
import { Drawer } from "antd";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import ChartOfAccountTabs from "./coaTabs";
import { Formik, Form, Field } from "formik";
import { CSVLink, CSVDownload } from "react-csv";

const ChartOfAccount = props => {
  const printBusinessExpenses = useRef();
  const printBusinessIncome = useRef();
  const printCapitalInflow = useRef();
  const printCapitalOverflow = useRef();

  let [chartOfAccounts, setChartOfAccounts] = useState(props.chartOfAccounts);
  let [activeRef, setActiveRef] = useState(printBusinessExpenses);
  let [activeTab, setActiveTab] = useState(0);
  let [drawerType, setDrawerType] = useState("new");
  let [updateData, setUpdateData] = useState({});

  let printAr = [
    printBusinessExpenses,
    printBusinessIncome,
    printCapitalOverflow,
    printCapitalInflow
  ];

  let finalData = [
    chartOfAccounts["businessExpenses"],
    chartOfAccounts["businessIncome"],
    chartOfAccounts["capitalOverflow"],
    chartOfAccounts["capitalInflow"]
  ];

  useEffect(() => {
    if (props.closeCoaDrawer) {
      setCoaDrawer(false);
    }

    if (props.chartOfAccounts) {
      setChartOfAccounts(props.chartOfAccounts);
    }
  }, [props.chartOfAccounts, props.closeCoaDrawer]);

  let [openAddCoaDrawer, setCoaDrawer] = useState(false);

  let businessIncomeCategories = [
    {
      title: "Property Income",
      value: "propertyIncome"
    }
  ];

  let businessCapitalOutflowCategories = [
    {
      title: "Capital Expenses",
      value: "capitalExpenses"
    }
  ];

  let businessCapitalInflowCategories = [
    {
      title: "Capital Expenses",
      value: "capitalExpenses"
    }
  ];

  let businessExpensesCategories = [
    {
      title: "Legal and Professional costs",
      value: "legalProfessionalCosts"
    },
    {
      title: "Finance Charges",
      value: "financeCharges"
    },
    {
      title: "Rent rates, insurance, ground rent",
      value: "rentRatesInsurance"
    },
    {
      title: "Repair, Maintenance, and renewals",
      value: "repairMaintenance"
    },
    {
      title: "Other Expenses",
      value: "otherExpenses"
    },
    {
      title: "Costs of services provided, inlcuding wages",
      value: "costOfServicesProvided"
    }
  ];

  const downloadCSV = data => {
    console.log("dadaad", data);
    let obj = finalData[data];

    console.log("aSDADASDASDASD", obj);
  };

  const onChange = data => {
    let value = data.target.value;

    value === "Business Expenses" &&
      changeAccountType(businessExpensesCategories);
    value === "Business Income" && changeAccountType(businessIncomeCategories);
    value === "Capital Overflow" &&
      changeAccountType(businessCapitalOutflowCategories);
    value === "Capital Inflow" &&
      changeAccountType(businessCapitalInflowCategories);
  };

  let [accountType, changeAccountType] = useState(businessExpensesCategories);

  const closeDrawer = () => {
    setCoaDrawer(false);
  };
  const openDrawer = (type, data) => {
    setCoaDrawer(true);
    setDrawerType(type);
    setUpdateData(data);
  };

  const setActiveTabValue = tab => {
    setActiveRef(printAr[tab]);
    setActiveTab(tab);
    downloadCSV(tab);
  };

  return (
    <>
      <div className="row">
        <div className="col-md-12 text-right">
          <div className="print__download">
            <ReactToPrint
              trigger={() => <button>Print this out!</button>}
              content={() => activeRef.current}
            />
            <button onClick={() => openDrawer("new", {})}>Add New</button>
          </div>
        </div>
      </div>

      <Tabs className="charact__tabs" defaultIndex={activeTab}>
        <TabList>
          <Tab onClick={() => setActiveTabValue(0)}>Business Expenses</Tab>
          <Tab onClick={() => setActiveTabValue(1)}>Business Income</Tab>
          <Tab onClick={() => setActiveTabValue(2)}>Capital Overflow</Tab>
          <Tab onClick={() => setActiveTabValue(3)}>Capital Inflow</Tab>
        </TabList>

        <TabPanel>
          <ChartOfAccountTabs.BusinessExpenses
            coaData={chartOfAccounts["businessExpenses"]}
            testRef={printBusinessExpenses}
            openDrawer={openDrawer}
          />
        </TabPanel>
        <TabPanel>
          <ChartOfAccountTabs.BusinessIncome
            coaData={chartOfAccounts["businessIncome"]}
            testRef={printBusinessIncome}
            openDrawer={openDrawer}
          />
        </TabPanel>
        <TabPanel>
          <ChartOfAccountTabs.CapitalOverflow
            coaData={chartOfAccounts["capitalOverflow"]}
            testRef={printCapitalOverflow}
            openDrawer={openDrawer}
          />
        </TabPanel>
        <TabPanel>
          <ChartOfAccountTabs.CapitalInflow
            coaData={chartOfAccounts["capitalInflow"]}
            testRef={printCapitalInflow}
            openDrawer={openDrawer}
          />
        </TabPanel>
      </Tabs>
      <Drawer
        title="Add Chart Of Account"
        level={"#root"}
        width={500}
        zIndex={99999}
        placement="right"
        closable={false}
        onClose={closeDrawer}
        visible={openAddCoaDrawer}
        destroyOnClose={true}
      >
        <Formik
          initialValues={updateData}
          validationSchema={props.ChartOfAccountSchema}
          onSubmit={(values, { setSubmitting, resetForm }) => {
            setSubmitting(true);
            if (drawerType === "new") {
              props.addChartOfAccount(values);
            }

            if (drawerType === "update") {
              let obj = {};
              obj.chartId = updateData._id;
              obj.accountType = values.accountType;
              obj.accountName = values.accountName;
              obj.category = values.category;
              props.updateChartOfAccount(obj);
            }
          }}
        >
          {({ isSubmitting, setFieldValue, errors, values }) => (
            <Form>
              <label className="labels__global">Account Type</label>
              <div className="form-group">
                <select
                  name="accountType"
                  defaultValue={values["accountType"]}
                  component="select"
                  className={
                    errors["accountType"]
                      ? "form-control error__field_show"
                      : "form-control tab__select"
                  }
                  onChange={e => {
                    onChange(e);
                    setFieldValue("accountType", e.target.value);
                  }}
                >
                  <option selected disabled value>
                    Please select account type
                  </option>
                  <option value="Business Expenses">Business Expenses</option>
                  <option value="Business Income">Business Income</option>
                  <option value="Capital Overflow">Capital Overflow</option>
                  <option value="Capital Inflow">Capital Inflow</option>
                </select>
              </div>

              <label className="labels__global">Select Category</label>
              <div className="form-group">
                <Field
                  name="category"
                  component="select"
                  className={
                    errors["category"]
                      ? "form-control error__field_show"
                      : "form-control tab__select"
                  }
                >
                  <option selected disabled value>
                    Please select category
                  </option>
                  {accountType.map((o, i) => {
                    return <option value={o.title}>{o.title}</option>;
                  })}
                </Field>
              </div>

              <label className="labels__global">Enter Name</label>
              <div className="form-group">
                <Field
                  placeholder="Enter name"
                  type="text"
                  name="accountName"
                  className={
                    errors["accountName"]
                      ? "form-control error__field_show"
                      : "form-control tab__select"
                  }
                />
              </div>

              <div className="save__cancel--btn">
                <div className="form-group">
                  <button type="submit" className="btn save__btn">
                    {drawerType === "new" ? "" : "Update"} Save
                  </button>
                  <button className="btn cancel__btn">Cancel</button>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </Drawer>
    </>
  );
};

export default ChartOfAccount;
