import BusinessExpenses from "./BusinessExpenses";
import BusinessIncome from "./BusinessIncome";
import CapitalOverflow from "./CapitalOverflow";
import CapitalInflow from "./CapitalInflow";

export default {
  BusinessExpenses,
  BusinessIncome,
  CapitalOverflow,
  CapitalInflow
};
