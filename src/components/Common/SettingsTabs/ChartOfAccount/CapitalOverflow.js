import React from "react";
import { Table } from "antd";

class CapitalOverlow extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      coaData: []
    };
  }

  componentDidMount() {
    this.setState({ coaData: this.props.coaData });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return { coaData: nextProps.coaData };
  }

  render() {
    let { coaData } = this.state;

    const columns = [
      {
        title: "Name",
        dataIndex: "accountName",
        align: "center"
      },
      {
        title: "Account Type",
        dataIndex: "accountType",
        align: "center"
      },
      {
        title: "Category",
        dataIndex: "category",
        align: "center"
      },
      {
        title: "Action",
        align: "center",
        render: text => (
          <>
            {text.isCreatedByAdmin ? (
              <i class="fas fa-lock"></i>
            ) : (
              <i
                onClick={() => this.props.openDrawer("update", text)}
                class="fas fa-edit"
              ></i>
            )}
          </>
        )
      }
    ];
    return (
      <>
        <h2>Capital Overflow</h2>
        <Table
          ref={this.props.testRef}
          locale={{ emptyText: "No Data!" }}
          columns={columns}
          dataSource={coaData}
          bordered
        />
      </>
    );
  }
}

export default CapitalOverlow;
