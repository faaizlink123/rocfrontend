import React, { useState, useEffect, useRef } from "react";
import { Formik, Form, Field } from "formik";
import _ from "lodash";
import "./style.scss";

const AccountSetting = props => {
  let refContainer = useRef();

  props.history.listen(async (location, action) => {
    if (refContainer.current && props.shouldUpdateAccountSettings) {
      let formValues = _.get(refContainer, "current.state.values");
      let areValuesTheSame = _.isEqual(accountPreferences, formValues);
      !areValuesTheSame && submitAccountSettings(formValues);
    }
  });

  let [accountPreferences, setAccountPreferences] = useState(
    props.accountPreferences
  );

  const submitAccountSettings = values => {
    let mainObj = accountPreferences;
    Object.keys(values).map((t, i) => {
      mainObj[t] = values[t];
    });
    // props.accountPreferences
    props.updateAccountSettings(mainObj);
  };

  useEffect(() => {
    setAccountPreferences(props.accountPreferences);
  }, [props.accountPreferences]);
  return (
    <div className="account_setting">
      <div className="">
        <div className="info_wrap">
          <Formik
            enableReinitialize
            ref={refContainer}
            name="AccountSettings"
            initialValues={accountPreferences}
            onSubmit={(values, { validateForm, setSubmitting }) => {
              setSubmitting(true);
              submitAccountSettings(values);
            }}
          >
            {({
              handleSubmit,
              isSubmitting,
              setFieldValue,
              values,
              errors
            }) => (
              <Form>
                <div className="row">
                  <div className="col-sm-4 col-md-4">
                    <div className="form-group">
                      <label className="labels__global">
                        Currency <i className="fas fa-info"></i>
                      </label>
                      <div className="input-group mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="fas fa-pound-sign"></i>
                          </span>
                        </div>
                        <Field
                          component="select"
                          name="currency"
                          className="form-control"
                          disabled
                        >
                          <option value="United Kingdom Pounds">
                            United Kingdom Pounds
                          </option>
                        </Field>
                      </div>
                    </div>
                  </div>

                  <div className="col-sm-4 col-md-4">
                    <div className="form-group">
                      <label className="labels__global">Select Time-zone</label>
                      <div className="input-group mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="fas fa-clock"></i>
                          </span>
                        </div>

                        <Field
                          component="select"
                          name="timeZone"
                          className="form-control"
                        >
                          {props.timezones &&
                            props.timezones.map((zone, i) => {
                              return <option value={zone}>{zone}</option>;
                            })}
                        </Field>
                      </div>
                    </div>
                  </div>

                  <div className="col-sm-4 col-md-4">
                    <div className="form-group form__group--global">
                      <label className="labels__global">Time Format</label>
                    </div>
                    <div className="custom-control custom-radio custom-control-inline">
                      <input
                        type="radio"
                        disabled=""
                        checked={_.get(values, "timeFormat") === "am"}
                        onChange={() => setFieldValue("timeFormat", "am")}
                        id="customRadio1"
                        name="timeFormat"
                        className="custom-control-input"
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="customRadio1"
                      >
                        AM/PM
                      </label>
                    </div>

                    <div className="custom-control custom-radio custom-control-inline">
                      <input
                        type="radio"
                        checked={_.get(values, "timeFormat") === "24hours"}
                        onChange={() => setFieldValue("timeFormat", "24hours")}
                        disabled=""
                        id="customRadio2"
                        name="timeFormat"
                        className="custom-control-input"
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="customRadio2"
                      >
                        24 Hours
                      </label>
                    </div>
                  </div>

                  <div className="col-sm-4 col-md-4">
                    <div className="form-group">
                      <label className="labels__global">
                        Select Date Format
                      </label>
                      <div className="input-group mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="fas fa-calendar"></i>
                          </span>
                        </div>

                        <Field
                          component="select"
                          name="dateFormat"
                          className="form-control"
                        >
                          <option value="DD-MM-YYYY">DD-MM-YYYY</option>
                          <option value="YY-MM-DD">YY-MM-DD</option>
                        </Field>
                      </div>
                    </div>
                  </div>

                  <div className="col-sm-4 col-md-4">
                    <div className="form-group form__group--global">
                      <label className="labels__global">
                        Unit of Measurement
                      </label>
                    </div>
                    <div className="custom-control custom-radio custom-control-inline">
                      <input
                        type="radio"
                        checked={
                          _.get(values, "measurementUnit") === "Imperial"
                        }
                        onChange={() =>
                          setFieldValue("measurementUnit", "Imperial")
                        }
                        id="customRadio3"
                        name="measurementUnit"
                        className="custom-control-input"
                        disabled=""
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="customRadio3"
                      >
                        Imperial
                      </label>
                    </div>
                    <div className="custom-control custom-radio custom-control-inline">
                      <input
                        type="radio"
                        checked={_.get(values, "measurementUnit") === "Metric"}
                        onChange={() =>
                          setFieldValue("measurementUnit", "Metric")
                        }
                        id="customRadio4"
                        name="measurementUnit"
                        className="custom-control-input"
                        disabled=""
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="customRadio4"
                      >
                        Metric
                      </label>
                    </div>
                  </div>
                </div>
              </Form>
            )}
          </Formik>
          <hr></hr>
          <div className="account_set">
            <div className="row mt-4">
              <div className="col-xl-4">
                <div className="form-group">
                  <button className="btn btn-block btn-outline-success mb-2 btns__success">
                    Export Data
                  </button>
                  <span className="help-text">
                    It can take some time to prepare files for the export. You
                    can leave the export screen and we'll notify you when the
                    download file is ready.
                  </span>
                </div>
              </div>
              <div className="col-xl-4">
                <div className="form-group">
                  <button className="btn btn-block btn-outline-warning mb-2 btns__warning">
                    Deactivate Account
                  </button>
                  <span className="help-text">
                    Account will be deactivated. User can come back and login /
                    reactivate account.
                  </span>
                </div>
              </div>
              <div className="col-xl-4">
                <div className="form-group">
                  <button className="btn btn-block btn-outline-danger mb-2 btns__danger">
                    Delete Account
                  </button>
                  <span className="help-text">
                    All personal details will be deleted and application will
                    reset to default settings. All your information will be lost
                    and this can't be undone. Some reference link will sill be
                    there in few table necessary to run the business
                    application.
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AccountSetting;
