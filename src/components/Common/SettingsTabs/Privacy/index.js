import React, { useState, useEffect } from "react";
import _ from "lodash";
// import Switch from "react-switch";
import "./style.scss";

const PrivacySetting = props => {
  let [updatePrivacySettings, setUpdatePrivacySetting] = useState(false);

  let profilePicture = _.get(props, "privacyInitial.profilePicture");
  let gender = _.get(props, "privacyInitial.gender");
  let age = _.get(props, "privacyInitial.age");
  let work = _.get(props, "privacyInitial.work");
  let references = _.get(props, "privacyInitial.references");
  let selfDeclaration = _.get(props, "privacyInitial.selfDeclaration");
  let socialConnect = _.get(props, "privacyInitial.socialConnect");

  props.history.listen((location, action) => {
    if (updatePrivacySettings === true) {
      props.savePrivacy();
    }
  });

  useEffect(() => {
    setUpdatePrivacySetting(props.updatePrivacySettings);
  }, [props]);

  return (
    <div className="privacy_setting">
      <div className="">
        <div className="table_wrap">
          <div className="box box-form-wrapper">
            <div className="table-responsive">
              <table className="table table-bordered table-acc-settings">
                <thead>
                  <tr>
                    <th scope="col" className="th__width">
                      Particulars
                    </th>
                    <th className="text-center" scope="col">
                      Only Me
                    </th>
                    <th className="text-center" scope="col">
                      My Connections{" "}
                      <span
                        className="info-circle"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Connection will be enabled once appointment is booked with landlord or order accepted by ServicePro or on user request"
                      >
                        <i className="fas fa-info"></i>
                      </span>
                    </th>
                    <th className="text-center" scope="col">
                      Public
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">Profile Picture</th>
                    <td className="text-center">
                      <label class="switch" for="profilePictureMe">
                        <input
                          onChange={e =>
                            props.setPrivacy("profilePicture", "onlyMe", e)
                          }
                          checked={profilePicture.onlyMe}
                          type="checkbox"
                          id="profilePictureMe"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                    <td className="text-center">
                      <label class="switch" for="profilePictureConnection">
                        <input
                          checked={profilePicture.connection}
                          onChange={e =>
                            props.setPrivacy("profilePicture", "connection", e)
                          }
                          type="checkbox"
                          id="profilePictureConnection"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                    <td className="text-center">
                      <label class="switch" for="profilePicturePublic">
                        <input
                          checked={profilePicture.public}
                          onChange={e =>
                            props.setPrivacy("profilePicture", "public", e)
                          }
                          type="checkbox"
                          id="profilePicturePublic"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Gender</th>
                    <td className="text-center">
                      <label class="switch" for="genderMe">
                        <input
                          checked={gender.onlyMe}
                          onChange={e =>
                            props.setPrivacy("gender", "onlyMe", e)
                          }
                          type="checkbox"
                          id="genderMe"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                    <td className="text-center">
                      <label class="switch" for="genderConnection">
                        <input
                          checked={gender.connection}
                          onChange={e =>
                            props.setPrivacy("gender", "connection", e)
                          }
                          type="checkbox"
                          id="genderConnection"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                    <td className="text-center">
                      <label class="switch" for="genderPublic">
                        <input
                          checked={gender.public}
                          onChange={e =>
                            props.setPrivacy("gender", "public", e)
                          }
                          type="checkbox"
                          id="genderPublic"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Age</th>
                    <td className="text-center">
                      <label class="switch" for="ageMe">
                        <input
                          checked={age.onlyMe}
                          onChange={e => props.setPrivacy("age", "onlyMe", e)}
                          type="checkbox"
                          id="ageMe"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                    <td className="text-center">
                      <label class="switch" for="ageConnection">
                        <input
                          checked={age.connection}
                          onChange={e =>
                            props.setPrivacy("age", "connection", e)
                          }
                          type="checkbox"
                          id="ageConnection"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                    <td className="text-center">
                      <label class="switch" for="agePublic">
                        <input
                          checked={age.public}
                          onChange={e => props.setPrivacy("age", "public", e)}
                          type="checkbox"
                          id="agePublic"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      Work / Employment{" "}
                      <span
                        className="info-circle"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Default setting public for ServicePro and Landlord, for Renter its my connection"
                      >
                        <i className="fas fa-info"></i>
                      </span>
                    </th>
                    <td className="text-center">
                      <label class="switch" for="workMe">
                        <input
                          checked={work.onlyMe}
                          onChange={e => props.setPrivacy("work", "onlyMe", e)}
                          type="checkbox"
                          id="workMe"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                    <td className="text-center">
                      <label class="switch" for="workConnection">
                        <input
                          checked={work.connection}
                          onChange={e =>
                            props.setPrivacy("work", "connection", e)
                          }
                          type="checkbox"
                          id="workConnection"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                    <td className="text-center">
                      <label class="switch" for="workPublic">
                        <input
                          checked={work.public}
                          onChange={e => props.setPrivacy("work", "public", e)}
                          type="checkbox"
                          id="workPublic"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      References{" "}
                      <span
                        className="info-circle"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="References received from verified past landlord, verified employee, verified Renters and verified ServicePro"
                      >
                        <i className="fas fa-info"></i>
                      </span>
                    </th>
                    <td className="text-center">
                      <label class="switch" for="referencesMe">
                        <input
                          checked={references.onlyMe}
                          onChange={e =>
                            props.setPrivacy("references", "onlyMe", e)
                          }
                          type="checkbox"
                          id="referencesMe"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                    <td className="text-center">
                      <label class="switch" for="referencesConnection">
                        <input
                          checked={references.connection}
                          onChange={e =>
                            props.setPrivacy("references", "connection", e)
                          }
                          type="checkbox"
                          id="referencesConnection"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                    <td className="text-center">
                      <label class="switch" for="referencesPublic">
                        <input
                          checked={references.public}
                          onChange={e =>
                            props.setPrivacy("references", "public", e)
                          }
                          type="checkbox"
                          id="referencesPublic"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      Self-declaration{" "}
                      <span
                        className="info-circle"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Only for Renters - Number of dependents, Pets, Smoking, car, disability, benefit etc. "
                      >
                        <i className="fas fa-info"></i>
                      </span>
                    </th>
                    <td className="text-center">
                      <label class="switch" for="declarationMe">
                        <input
                          checked={selfDeclaration.onlyMe}
                          onChange={e =>
                            props.setPrivacy("selfDeclaration", "onlyMe", e)
                          }
                          type="checkbox"
                          id="declarationMe"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                    <td className="text-center">
                      <label class="switch" for="declarationConnection">
                        <input
                          checked={selfDeclaration.connection}
                          onChange={e =>
                            props.setPrivacy("selfDeclaration", "connection", e)
                          }
                          type="checkbox"
                          id="declarationConnection"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                    <td className="text-center">
                      <label class="switch" for="declarationPublic">
                        <input
                          checked={selfDeclaration.public}
                          onChange={e =>
                            props.setPrivacy("selfDeclaration", "public", e)
                          }
                          type="checkbox"
                          id="declarationPublic"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Social Connect</th>
                    <td className="text-center">
                      <label class="switch" for="socialMe">
                        <input
                          checked={socialConnect.onlyMe}
                          onChange={e =>
                            props.setPrivacy("socialConnect", "onlyMe", e)
                          }
                          type="checkbox"
                          id="socialMe"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                    <td className="text-center">
                      <label class="switch" for="socialConnection">
                        <input
                          checked={socialConnect.connection}
                          onChange={e =>
                            props.setPrivacy("socialConnect", "connection", e)
                          }
                          type="checkbox"
                          id="socialConnection"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                    <td className="text-center">
                      <label class="switch" for="socialPublic">
                        <input
                          checked={socialConnect.public}
                          onChange={e =>
                            props.setPrivacy("socialConnect", "public", e)
                          }
                          type="checkbox"
                          id="socialPublic"
                        />
                        <div class="slider round"></div>
                      </label>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PrivacySetting;
