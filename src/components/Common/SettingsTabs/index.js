import React from "react";
import { Drawer } from "antd";

import { Route, Switch, Link, Redirect } from "react-router-dom";
import _ from "lodash";
import UserRoleQuery from "../../../config/queries/userRole";

import UserContext from "../../../config/UserContext";
import CardData from "../Card";
import ProfileInfo from "./ProfileInfo";
import PersonaProfile from "./PersonaProfile";
import AccountSetting from "./AccountSetting";
import SecuritySetting from "./Security";
import PrivacySetting from "./Privacy";
import NotificationSettings from "./Notifications";
import SubscriptionSetting from "./Subscription";
import ProfileCompleteness from "./ProfileCompleteness";
import ChartOfAccount from "./ChartOfAccount";
import UserRole from "./UserRole";
import { withRouter } from "react-router-dom";
import axios from "axios";

import AccountQueries from "../../../config/queries/account";
import ShowLoadingMessage from "../../../config/ShowLoadingMessage";

import "./style.scss";
import validateBank from "../../../config/ValidateBankAccount";
import ChartOfAccountSchema from "../../../config/FormSchemas/ChartOfAccount";
import showNotification from "../../../config/Notification";

const NoLeftDivAr = [
  "accountsetting",
  "security",
  "privacy",
  "notifications",
  "subscriptions",
  "chartOfAccount",
  "userRole"
];

class Settings extends React.PureComponent {
  static contextType = UserContext;

  constructor(props) {
    super(props);

    this.state = {
      ProfileCompletenessData: {},
      closeCoaDrawer: false,
      monthlyPlans: [],
      userRoles: [],
      yearlyPlans: [],
      shouldUpdateAccountSettings: true,
      showProfileCompletenessModal: false,
      currentUserRole: "",
      currentUserData: {},
      bankDetails: {},
      currentActiveAboutTab: 0,
      LandlordTabs: [],
      chartOfAccounts: {
        businessExpenses: [],
        businessIncome: [],
        capitalInflow: [],
        capitalOverflow: []
      },
      nationalities: [],
      userDataAbout: {
        firstName: "",
        lastName: "",
        companyName: "",
        companyRegistrationNumber: "",
        isCompany: false
      },
      accountPreferences: {
        timeZone: "Europe/London",
        timeFormat: "am",
        measurementUnit: "Metric",
        currency: "United Kingdom Pounds",
        dateFormat: "DD-MM-YYYY"
      },
      updateNotifications: false,
      pathname: "info",
      updatePrivacySettings: false,
      privacyInitial: {
        profilePicture: {
          onlyMe: false,
          connection: false,
          public: false
        },
        gender: {
          onlyMe: false,
          connection: false,
          public: false
        },
        age: {
          onlyMe: false,
          connection: false,
          public: false
        },
        work: {
          onlyMe: false,
          connection: false,
          public: false
        },
        references: {
          onlyMe: false,
          connection: false,
          public: false
        },
        selfDeclaration: {
          onlyMe: false,
          connection: false,
          public: false
        },
        socialConnect: {
          onlyMe: false,
          connection: false,
          public: false
        }
      },
      notificationPreferences: [
        { appointment: { email: true, whatsApp: true, sms: true } },
        { taskReminder: { email: true, whatsApp: true, sms: true } },
        { screeningReport: { email: true, whatsApp: true, sms: true } },
        { newsLetter: { email: true, whatsApp: true, sms: true } }
      ]
    };
  }

  componentDidMount() {
    let activeTab =
      this.props.location.pathname.includes("/landlord/settings/info") &&
      this.props.location.pathname.split("/landlord/settings/info/")[1];

    if (activeTab) {
      this.setState({ currentActiveAboutTab: Number(activeTab) });
    }

    let pathname = window.location.pathname.split(
      `/${_.get(this.context, "userData.authentication.data.role")}/settings/`
    )[1];

    this.setState({ pathname });

    // Fetch Initial Profile Data
    this.fetchPreferences();
    this.fetchUserRoles();
    this.fetchPreferencesAccountSettings();
    this.fetchPreferencesPrivacy();
    this.getBankDetails();
    this.getProfileCompleteness();
    this.fetchAbout();
    this.getSubscriptionPlans();

    if (
      _.get(this.context, "userData.authentication.data.role") === "landlord" ||
      _.get(this.context, "userData.authentication.data.role") === "admin"
    ) {
      this.fetchChartOfAccount();
    }

    // Fetch Timezones and Nationalities
    axios.get("http://worldtimeapi.org/api/timezone").then(res => {
      this.setState({ timezones: res.data });
    });

    axios
      .get("https://api.github.com/gists/2aae12314d5419365bc3cae033239273")
      .then(res => {
        this.setState({
          nationalities: JSON.parse(
            res.data.files["nationalities.json"].content
          )
        });
      });

    this.setState({
      currentUserRole: this.context.userData.authentication.data.role,
      currentUserData: this.context.userData.authentication.data
    });
  }

  getProfileCompleteness = async () => {
    const fetchProfileCompleteness = await this.props.client.query({
      query: AccountQueries.fetchProfileCompleteness
    });

    if (
      _.get(fetchProfileCompleteness, "data.getProfileCompleteness.success")
    ) {
      this.setState({
        ProfileCompletenessData: _.get(
          fetchProfileCompleteness,
          "data.getProfileCompleteness.data"
        )
      });
    }

    console.log("RPPOOOFIEL COMEOMTE", fetchProfileCompleteness);
  };

  fetchUserRoles = async () => {
    const fetchProfileAboutQuery = await this.props.client.query({
      query: UserRoleQuery.fetchUserRoles
    });

    if (_.get(fetchProfileAboutQuery, "data.getAccessRoles.success")) {
      this.setState({
        userRoles: _.get(fetchProfileAboutQuery, "data.getAccessRoles.data")
      });

      this.forceUpdate();
    }
  };

  fetchAbout = async () => {
    this.context.startLoading();

    const fetchProfileAboutQuery = await this.props.client.query({
      query: AccountQueries.fetchProfileAbout
    });

    if (
      !_.isEmpty(fetchProfileAboutQuery.data.getProfileInformation) &&
      _.get(fetchProfileAboutQuery, "data.getProfileInformation.success")
    ) {
      let userData = _.get(
        fetchProfileAboutQuery,
        "data.getProfileInformation.data"
      );

      this.setState({
        userDataAbout: userData
      });
      this.forceUpdate();
      this.context.endLoading();
    }
  };

  addChartOfAccount = async formData => {
    this.context.startLoading();
    const addChartOfAccount = await this.props.client.mutate({
      mutation: AccountQueries.addChartOfAccount,
      variables: formData
    });

    if (_.get(addChartOfAccount, "data.createChartOfAccount.success")) {
      showNotification("success", "Chart of Account Added!", "");
      this.context.endLoading();
      this.setState({ closeCoaDrawer: true });
      this.fetchChartOfAccount();
    } else {
      showNotification(
        "error",
        "An error occured",
        _.get(addChartOfAccount, "data.createChartOfAccount.message")
      );
    }
  };

  updateChartOfAccount = async formData => {
    this.setState({ closeCoaDrawer: false });
    this.context.startLoading();
    const updateChartOfAccount = await this.props.client.mutate({
      mutation: AccountQueries.updateChartOfAccount,
      variables: formData
    });

    if (_.get(updateChartOfAccount, "data.updateChartOfAccount.success")) {
      showNotification("success", "Chart of Account Updated!", "");
      this.context.endLoading();
      this.setState({ closeCoaDrawer: true });
      this.fetchChartOfAccount();
    } else {
      showNotification(
        "error",
        "An error occured",
        _.get(updateChartOfAccount, "data.updateChartOfAccount.message")
      );
    }
  };

  fetchChartOfAccount = async formData => {
    const getChartOfAccount = await this.props.client.query({
      query: AccountQueries.fetchChartOfAccount
    });

    if (_.get(getChartOfAccount, "data.getChartOfAccount.success")) {
      let chartOfAccounts = _.get(
        getChartOfAccount,
        "data.getChartOfAccount.data"
      );
      this.setState({ chartOfAccounts });
      this.forceUpdate();
    }
  };

  fetchPreferences = async () => {
    const fetchPreferences = await this.props.client.query({
      query: AccountQueries.fetchNotificationPreferences
    });

    if (
      !_.isEmpty(fetchPreferences.data.notifications) &&
      _.get(fetchPreferences, "data.notifications.success")
    ) {
      let ar = [];
      ar.push(_.get(fetchPreferences, "data.notifications.data"));
      this.setState({
        notificationPreferences: ar
      });
    }
  };

  getSubscriptionPlans = async () => {
    const getSubscriptionPlansQuery = await this.props.client.query({
      query: AccountQueries.getSubscriptionPlans
    });

    if (_.get(getSubscriptionPlansQuery, "data.getPlanDetails.success")) {
      let allPlans = _.get(
        getSubscriptionPlansQuery,
        "data.getPlanDetails.data"
      );
      let monthlyPlans = _.filter(allPlans, { interval: "monthly" });
      let yearlyPlans = _.filter(allPlans, { interval: "yearly" });

      this.setState({ monthlyPlans, yearlyPlans });
    }
  };

  getBankDetails = async () => {
    const getBankDetailsQuery = await this.props.client.query({
      query: AccountQueries.getBankDetail
    });

    if (_.get(getBankDetailsQuery, "data.getBankDetail.success")) {
      this.setState({
        bankDetails: _.get(getBankDetailsQuery, "data.getBankDetail.data")
      });
    }
  };

  saveBankData = async bankData => {
    this.context.startLoading();

    ShowLoadingMessage("Validating Bank Details...");

    let validateBankResponse = await this.validateBankAccount(
      bankData.bankCode,
      bankData.accountNumber
    );

    let isCorrectBankDetails = _.filter(validateBankResponse.data.Items, {
      IsCorrect: true
    });

    if (isCorrectBankDetails && isCorrectBankDetails.length === 0) {
      let details = _.filter(validateBankResponse.data.Items, {
        IsCorrect: false
      });
      showNotification(
        "error",
        "Bank Details Are Incorrect!",
        _.get(details[0], "StatusInformation")
      );
      this.context.endLoading();
    } else {
      const saveBankDetailsQuery = await this.props.client.mutate({
        mutation: AccountQueries.saveBankDetails,
        variables: bankData
      });

      if (
        !_.isEmpty(saveBankDetailsQuery.data.updateBankDetail) &&
        _.get(saveBankDetailsQuery, "data.updateBankDetail.success")
      ) {
        showNotification(
          "success",
          "Details Updated!",
          "Bank Details have been updated!"
        );
        this.getBankDetails();
      } else {
        showNotification(
          "error",
          "An error occured",
          _.get(saveBankDetailsQuery, "data.updateBankDetail.message")
        );
      }
    }
  };

  fetchPreferencesPrivacy = async () => {
    const fetchPreferencesPrivacy = await this.props.client.query({
      query: AccountQueries.fetchPrivacyInformation
    });

    if (
      !_.isEmpty(fetchPreferencesPrivacy.data.getPrivacyInformation) &&
      _.get(fetchPreferencesPrivacy, "data.getPrivacyInformation.success")
    ) {
      this.setState({
        privacyInitial: _.get(
          fetchPreferencesPrivacy,
          "data.getPrivacyInformation.data"
        )
      });
    }
  };

  fetchPreferencesAccountSettings = async () => {
    const fetchPreferences = await this.props.client.query({
      query: AccountQueries.fetchAccountPreferences
    });

    if (
      !_.isEmpty(fetchPreferences.data.getAccountSetting) &&
      _.get(fetchPreferences, "data.getAccountSetting.success")
    ) {
      this.setState({
        accountPreferences: _.get(
          fetchPreferences,
          "data.getAccountSetting.data"
        )
      });
      this.forceUpdate();
    }
  };

  setNotificationPreferences = async (type, preferences) => {
    let notificationPreferences = this.state.notificationPreferences;

    if (type === "set") {
      notificationPreferences.map((n, i) => {
        if (n[preferences.type]) {
          n[preferences.type][preferences.name] =
            preferences.event.target.checked;
        }
      });

      this.setState({ notificationPreferences, updateNotifications: true });

      this.forceUpdate();
    }

    if (type === "save") {
      let r = {};
      this.setState({ updateNotifications: false });
      let updateNotifications = this.state.updateNotifications;

      if (updateNotifications) {
        ShowLoadingMessage("Saving Your Changes...");

        notificationPreferences.map((t, i) => {
          Object.keys(t).map((l, j) => {
            r[l] = { ...t[l] };
          });
        });

        let saveNotificationPreferencesQuery = await this.props.client.mutate({
          mutation: AccountQueries.setNotificationPreferences,
          variables: r
        });

        if (
          !_.isEmpty(
            saveNotificationPreferencesQuery.data.updateNotificationInformation
          ) &&
          _.get(
            saveNotificationPreferencesQuery,
            "data.updateNotificationInformation.success"
          )
        ) {
          showNotification(
            "success",
            "Preferences Updated!",
            "Notification Preferences have been updated!"
          );
          this.setState({ updateNotifications: false });
          this.fetchPreferences();
        } else {
          showNotification(
            "error",
            "An error occured",
            _.get(
              saveNotificationPreferencesQuery,
              "data.updateNotificationInformation.message"
            )
          );
          this.setState({ updateNotifications: false });
        }
      }
    }
  };

  updateProfileAbout = async formData => {
    this.context.startLoading();

    if (formData.uploadAvatar) {
      let uploadedFile = await axios.post(
        `${process.env.REACT_APP_SERVER}/api/v1/file-upload`,
        {
          file: formData.avatar.avatar,
          filename: formData.avatar.filename
        }
      );
      if (uploadedFile.data.success) {
        formData.avatar = uploadedFile.data.data;
      } else {
        formData.avatar = "";
      }

      this.uploadProfileAbout(formData);
    } else {
      this.uploadProfileAbout(formData);
    }
  };

  uploadProfileAbout = async formData => {
    delete formData["uploadAvatar"];

    let updateAboutQuery = await this.props.client.mutate({
      mutation: AccountQueries.updateProfileAbout,
      variables: { ...formData }
    });

    if (
      !_.isEmpty(updateAboutQuery.data.updateProfileInformation) &&
      _.get(updateAboutQuery, "data.updateProfileInformation.success")
    ) {
      showNotification(
        "success",
        "Profile Updated!",
        "Your Profile has been updated successfully!"
      );
      this.setState({ currentActiveAboutTab: 1 });
      this.fetchPreferencesAccountSettings();

      this.context.endLoading();
    } else {
      showNotification(
        "error",
        "An error occured",
        _.get(updateAboutQuery, "data.updateProfileInformation.message")
      );
      this.context.endLoading();
    }
  };

  setPrivacy = (key, type, value) => {
    let initialData = this.state.privacyInitial;
    initialData[key][type] = value.target.checked;
    this.setState({ privacyInitial: initialData, updatePrivacySettings: true });
    this.forceUpdate();
  };

  savePrivacy = async () => {
    this.setState({ updatePrivacySettings: false });

    let privacyData = this.state.privacyInitial;
    let updatePrivacySettings = this.state.updatePrivacySettings;

    if (updatePrivacySettings) {
      ShowLoadingMessage("Saving Your Changes...");

      let updatePrivacyQuery = await this.props.client.mutate({
        mutation: AccountQueries.updatePrivacyData,
        variables: {
          profilePicture: privacyData.profilePicture,
          gender: privacyData.gender,
          age: privacyData.gender,
          work: privacyData.work,
          references: privacyData.references,
          selfDeclaration: privacyData.selfDeclaration,
          socialConnect: privacyData.socialConnect
        }
      });

      if (
        !_.isEmpty(updatePrivacyQuery.data.updatePrivacyInformation) &&
        _.get(updatePrivacyQuery, "data.updatePrivacyInformation.success")
      ) {
        showNotification(
          "success",
          "Privacy Preferences Updated!",
          "Your Privacy Preferences have been updated successfully!"
        );
        this.setState({ updatePrivacySettings: false });
        this.fetchPreferencesPrivacy();

        this.context.endLoading();
      } else {
        showNotification(
          "error",
          "An error occured",
          _.get(updatePrivacyQuery, "data.updatePrivacyInformation.message")
        );
        this.context.endLoading();
      }
    }
  };

  saveCardDetails = async cardDetails => {
    this.context.startLoading();
    let addCardDetailsQuery = await this.props.client.query({
      query: AccountQueries.addCardDetails,
      variables: { ...cardDetails }
    });

    if (
      !_.isEmpty(addCardDetailsQuery.data.verifyCardDetail) &&
      _.get(addCardDetailsQuery, "data.verifyCardDetail.success")
    ) {
      showNotification(
        "success",
        "Details Saved!",
        "Details Saved successfully!"
      );
      this.context.endLoading();
    } else {
      showNotification(
        "error",
        "An error occured",
        _.get(addCardDetailsQuery, "data.verifyCardDetail.message")
      );
      this.context.endLoading();
    }
  };

  saveConnectProfileData = async connectData => {
    this.context.startLoading();

    let updateConnectQuery = await this.props.client.mutate({
      mutation: AccountQueries.updateConnectInformation,
      variables: { connectInput: connectData }
    });

    if (
      !_.isEmpty(updateConnectQuery.data.updateConnectInformation) &&
      _.get(updateConnectQuery, "data.updateConnectInformation.success")
    ) {
      showNotification(
        "success",
        "Details Saved!",
        "Details Saved successfully!"
      );
      this.context.endLoading();
    } else {
      showNotification(
        "error",
        "An error occured",
        _.get(updateConnectQuery, "data.updateConnectInformation.message")
      );
      this.context.endLoading();
    }
  };

  updateAccountSettings = async formData => {
    ShowLoadingMessage("Saving Your Changes...");

    this.context.startLoading();

    let updateAccountSettingsQuery = await this.props.client.mutate({
      mutation: AccountQueries.updateAccountPreferences,
      variables: { ...formData }
    });

    if (
      !_.isEmpty(updateAccountSettingsQuery.data.updateAccountSetting) &&
      _.get(updateAccountSettingsQuery, "data.updateAccountSetting.success")
    ) {
      showNotification(
        "success",
        "Profile Updated!",
        "Your Profile has been updated successfully!"
      );

      this.fetchAbout();

      this.setState({ shouldUpdateAccountSettings: false });

      this.context.endLoading();
    } else {
      showNotification(
        "error",
        "An error occured",
        _.get(updateAccountSettingsQuery, "data.updateAccountSetting.message")
      );
      this.context.endLoading();
    }
  };

  validateBankAccount = async (sortCode, acNumber) => {
    let validateResponse = await validateBank(sortCode, acNumber);
    return validateResponse;
  };

  render() {
    let {
      timezones,
      notificationPreferences,
      nationalities,
      updateNotifications,
      accountPreferences,
      userDataAbout,
      currentActiveAboutTab,
      currentUserRole,
      showProfileCompletenessModal,
      privacyInitial,
      updatePrivacySettings,
      shouldUpdateAccountSettings,
      bankDetails,
      monthlyPlans,
      yearlyPlans,
      chartOfAccounts,
      closeCoaDrawer,
      userRoles,
      ProfileCompletenessData
    } = this.state;

    const { match } = this.props;

    let pathname = window.location.pathname.split(
      `/${_.get(this.context, "userData.authentication.data.role")}/settings/`
    )[1];

    return (
      <>
        <Drawer
          title="Profile Completeness"
          placement={"right"}
          closable={true}
          width={"70%"}
          onClose={() =>
            this.setState({
              showProfileCompletenessModal: !showProfileCompletenessModal
            })
          }
          visible={showProfileCompletenessModal}
        >
          <ProfileCompleteness />
        </Drawer>

        {/* Profile Compleness Modal End */}

        <div className="profile__menu--listing">
          <ul>
            <li className={pathname === "info" && "active"}>
              <Link to={`${match.url}/info`}>Profile Info</Link>
            </li>
            <li className={pathname === "persona" && "active"}>
              <Link to={`${match.url}/persona`}>Persona Profile</Link>
            </li>
            <li className={pathname === "accountsetting" && "active"}>
              <Link to={`${match.url}/accountsetting`}>Account Setting</Link>
            </li>
            <li className={pathname === "privacy" && "active"}>
              <Link to={`${match.url}/privacy`}>Privacy</Link>
            </li>
            <li className={pathname === "security" && "active"}>
              <Link to={`${match.url}/security`}>Security</Link>
            </li>
            <li className={pathname === "subscriptions" && "active"}>
              <Link to={`${match.url}/subscriptions`}>Subscription</Link>
            </li>
            <li className={pathname === "notifications" && "active"}>
              <Link to={`${match.url}/notifications`}>Notification</Link>
            </li>
            <li className={pathname === "userRole" && "active"}>
              <Link to={`${match.url}/userRole`}>User Role</Link>
            </li>
            {(currentUserRole === "landlord" ||
              currentUserRole === "admin") && (
              <li className={pathname === "chartOfAccount" && "active"}>
                <Link to={`${match.url}/chartOfAccount`}>Chart of Account</Link>
              </li>
            )}
          </ul>
        </div>
        <div className="profile__menu--details">
          <div
            className={
              NoLeftDivAr.includes(pathname)
                ? "profile__details--left fullwidth"
                : "profile__details--left"
            }
          >
            <Switch>
              <Route exact path={`${match.url}`}>
                <Redirect to={`${match.url}/info`} />
              </Route>
              <Route path={`${match.url}/info`}>
                <ProfileInfo
                  bankDetails={bankDetails}
                  saveBankData={this.saveBankData}
                  saveCardDetails={this.saveCardDetails}
                  saveConnectProfileData={this.saveConnectProfileData}
                  currentActiveAboutTab={currentActiveAboutTab}
                  contextData={this.context}
                  userDataAbout={userDataAbout}
                  updateProfileAbout={this.updateProfileAbout}
                  nationalities={nationalities}
                  {...this.props}
                />
              </Route>
              <Route exact path={`${match.url}/persona`}>
                <PersonaProfile
                  contextData={this.context}
                  currentUserRole={currentUserRole}
                  {...this.props}
                />
              </Route>
              <Route exact path={`${match.url}/accountsetting`}>
                <AccountSetting
                  shouldUpdateAccountSettings={shouldUpdateAccountSettings}
                  accountPreferences={accountPreferences}
                  updateAccountSettings={this.updateAccountSettings}
                  timezones={timezones}
                  {...this.props}
                />
              </Route>
              <Route exact path={`${match.url}/security`}>
                <SecuritySetting {...this.props} />
              </Route>
              <Route exact path={`${match.url}/privacy`}>
                <PrivacySetting
                  savePrivacy={this.savePrivacy}
                  updatePrivacySettings={updatePrivacySettings}
                  privacyInitial={privacyInitial}
                  setPrivacy={this.setPrivacy}
                  {...this.props}
                />
              </Route>
              <Route exact path={`${match.url}/notifications`}>
                <NotificationSettings
                  pathname={pathname}
                  fetchPreferences={this.fetchPreferences}
                  notificationPreferences={notificationPreferences}
                  setNotificationPreferences={this.setNotificationPreferences}
                  updateNotifications={updateNotifications}
                  {...this.props}
                />
              </Route>
              <Route exact path={`${match.url}/subscriptions`}>
                <SubscriptionSetting
                  contextData={this.context}
                  monthlyPlans={monthlyPlans}
                  AccountQueries={AccountQueries}
                  yearlyPlans={yearlyPlans}
                  {...this.props}
                />
              </Route>

              <Route exact path={`${match.url}/userRole`}>
                <UserRole
                  contextData={this.context}
                  userRoles={userRoles}
                  UserRoleQuery={UserRoleQuery}
                  {...this.props}
                />
              </Route>

              {(currentUserRole === "landlord" ||
                currentUserRole === "admin") && (
                <Route exact path={`${match.url}/chartOfAccount`}>
                  <ChartOfAccount
                    chartOfAccounts={chartOfAccounts}
                    fetchChartOfAccount={this.fetchChartOfAccount}
                    addChartOfAccount={this.addChartOfAccount}
                    ChartOfAccountSchema={ChartOfAccountSchema}
                    closeCoaDrawer={closeCoaDrawer}
                    updateChartOfAccount={this.updateChartOfAccount}
                    {...this.props}
                  />
                </Route>
              )}
            </Switch>
          </div>

          {!NoLeftDivAr.includes(pathname) && (
            <div className="profile__details--right">
              <CardData
                ProfileCompletenessData={ProfileCompletenessData}
                showProfileCompletenessModal={() =>
                  this.setState({ showProfileCompletenessModal: true })
                }
                currentUserData={this.state.currentUserData}
                {...this.props}
              />
              {/* <h4>
                Why does RentOnCloud ask for things like your ID, photo and
                other personal info?
              </h4>
              <p>
                The RentOnCloud community is made up of landlords, renters and
                service professionals. Verifying users identity, whether for
                your banking information or for listings, or making rent
                agreement, it helps other users know it is safe to interact with
                you and send money. Given the real estate market has been faced
                with fake listings and unsuspecting renters being tricked out of
                thousands of pounds. RentOnCloud has gone the extra mile to
                protect all users, by allowing you to verify yourself, so
                Renters and Landlord feel safe in knowing it is you they are
                paying or communicating.
              </p>
              <p>
                This information is used for the exclusive and sole purpose of
                identifying you in order to provide a verified mark of
                screening. We hope you will help make RentOnCloud a safe place
                for renters by getting verified and leaving no doubt you are who
                you say you are.
              </p>
              <h4>Please note!</h4>
              <p>
                You can use your RentOnCloud account without verifying your
                identity, but before you can set up direct debit rent collection
                and list properties on public search, you need to provide a
                verification of your identity to help us prevent fraud and
                create trust for your potential renters and other RentOnCloud
                users.
              </p> */}
            </div>
          )}
        </div>
      </>
    );
  }
}

export default withRouter(Settings);
