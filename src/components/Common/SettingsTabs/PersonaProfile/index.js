import React, { useEffect, useState } from "react";
import PersonaQueries from "../../../../config/queries/personas";
import Personas from "./Personas";
import "react-tabs/style/react-tabs.css";

const PersonaProfile = props => {
  let [userRole, setUserRole] = useState(props.currentUserRole);

  useEffect(() => {
    setUserRole(props.currentUserRole);
  }, [props, props.contextData, props.setUserRole]);

  if (userRole === "renter") {
    return (
      <Personas.PersonaRenter PersonaQueries={PersonaQueries} {...props} />
    );
  } else if (userRole === "landlord") {
    return (
      <Personas.PersonaLandlord PersonaQueries={PersonaQueries} {...props} />
    );
  } else if (userRole === "servicepro") {
    return (
      <Personas.PersonaServicePro PersonaQueries={PersonaQueries} {...props} />
    );
  } else {
    return <p>404!</p>;
  }
};

export default PersonaProfile;
