import React from "react";
import ProfessionFormSection from "./ProfessionFormSection";
import DynamicFieldSet from "../DynamicFieldSet";
import AccrediationsFieldSet from "../DynamicFieldSetAccrediations";
import LandlordPersonaSchema from "../../../../../../config/FormSchemas/LandlordPersona";
import { Collapse, CardBody, Card, CardHeader } from "reactstrap";
import Axios from "axios";
import _ from "lodash";
import showMessage from "../../../../../../config/ShowLoadingMessage";
import showNotification from "../../../../../../config/Notification";

class LandlordPersona extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isSubmitting: false,
      openCollapse: {
        collapse1: true,
        collapse2: false,
        collapse3: false,
        isIncomplete: false
      },
      accrediations: {
        accrediations: [
          {
            organization: "",
            documentNumber: "",
            validTillDate: new Date()
          }
        ],
        empty: true
      },
      ProfessionFormData: {
        jobTitle: "",
        companyName: "",
        startDate: new Date(),
        companyWebsite: "",
        companyTelephone: "",
        empty: true
      },
      supportingDocuments: {
        supportingDocuments: [
          { document: "", documentNumber: "", docRaw: [], documentUrl: "" }
        ],
        empty: true
      }
    };
  }

  componentDidMount() {
    this.fetchLandlordPersona();
  }

  fetchLandlordPersona = async () => {
    const { PersonaQueries, contextData } = this.props;
    contextData.startLoading();
    const fetchLandlordPersonas = await this.props.client.query({
      query: PersonaQueries.fetchLandlordPersona
    });

    if (
      !_.isEmpty(fetchLandlordPersonas.data.getLandlordPersonaInformation) &&
      _.get(fetchLandlordPersonas, "data.getLandlordPersonaInformation.success")
    ) {
      let personaData = _.get(
        fetchLandlordPersonas,
        "data.getLandlordPersonaInformation.data"
      );

      this.setState({
        ProfessionFormData: personaData.profession,
        accrediations: { accrediations: personaData.accreditation },
        supportingDocuments: {
          supportingDocuments: personaData.supportingDocuments
        }
      });

      contextData.endLoading();
    }
  };

  toggleCollapse = key => {
    let openCollapseData = this.state.openCollapse;
    Object.keys(openCollapseData).map((colKey, i) => {
      if (colKey === key) {
        openCollapseData[colKey] = true;
      } else {
        openCollapseData[colKey] = false;
      }
    });

    this.setState({ openCollapse: openCollapseData });
    this.forceUpdate();
  };

  checkIncompleteFields = async () => {
    let isIncomplete = this.state.isIncomplete;
    let profession = this.state["ProfessionFormData"];
    isIncomplete = !profession.empty ? false : true;

    this.setState({ isIncomplete });
  };

  saveProfessionData = ProfessionFormData => {
    ProfessionFormData["empty"] = false;
    this.setState({ ProfessionFormData });
    this.toggleCollapse("collapse2");
  };

  setAccrediationData = accrediations => {
    accrediations["empty"] = false;
    this.setState({ accrediations });
    this.toggleCollapse("collapse3");
  };

  setSupportingDocuments = supportingDocuments => {
    supportingDocuments["empty"] = false;
    this.setState({ supportingDocuments });
  };

  getBase64 = file => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  };

  savePersonaLandlord = async () => {
    const {
      accrediations,
      ProfessionFormData,
      supportingDocuments
    } = this.state;

    const { PersonaQueries, contextData } = this.props;
    this.setState({ isSubmitting: true });
    contextData.startLoading();

    if (ProfessionFormData["profession"] === "Other") {
      ProfessionFormData["profession"] = ProfessionFormData["professionCustom"];
      delete ProfessionFormData["empty"];
      delete ProfessionFormData["professionCustom"];
    }

    delete ProfessionFormData["empty"];

    let uploadSupportingDocuments;
    let uploadableFiles = _.get(supportingDocuments, "supportingDocuments");

    if (!_.isEmpty(uploadableFiles)) {
      uploadSupportingDocuments = uploadableFiles.map(async (file, i) => {
        if (file.docRaw && !_.isEmpty(file.docRaw)) {
          showMessage("Uploading Documents...");

          let toUpload = file.docRaw[file.docRaw.length - 1];
          let getB64File = await this.getBase64(toUpload);

          let uploadedFile = await Axios.post(
            `${process.env.REACT_APP_SERVER}/api/v1/file-upload`,
            {
              file: getB64File,
              filename: toUpload.name
            }
          );

          if (_.get(uploadedFile, "data.success")) {
            let documentUrl = _.get(uploadedFile, "data.data");
            delete file["docRaw"];
            file["documentUrl"] = documentUrl;
          }
        } else {
          delete file["docRaw"];
          file["documentUrl"] = "";
        }
      });

      await Promise.all(uploadSupportingDocuments);
    }

    delete supportingDocuments["empty"];

    showMessage("Saving Data...");

    const queryResponse = await this.props.client.mutate({
      mutation: PersonaQueries.LandlordPersona,
      variables: {
        accreditation: accrediations.accrediations,
        profession: ProfessionFormData,
        supportingDocuments: supportingDocuments.supportingDocuments
      }
    });

    if (_.get(queryResponse, "data.updateLandlordPersonaInformation.success")) {
      showNotification(
        "success",
        "Persona Details Updated!",
        "Your Persona Details have been successfully updated!"
      );
      this.setState({ isSubmitting: false });

      contextData.endLoading();
      this.fetchLandlordPersona();
    } else {
      contextData.endLoading();
      this.setState({ isSubmitting: false });

      showNotification(
        "error",
        "An error occured",
        _.get(queryResponse, "data.updateLandlordPersonaInformation.message")
      );
    }
  };

  render() {
    const {
      openCollapse,
      ProfessionFormData,
      accrediations,
      supportingDocuments,
      isSubmitting
    } = this.state;
    return (
      <>
        <Card style={{ marginBottom: "1rem" }}>
          <CardHeader
            onClick={() => this.toggleCollapse("collapse1")}
            data-type="collapseLayout"
            className="card__title card__middle"
          >
            Profession *
            <div className="details__icons">
              <i className="mdi mdi-chevron-down down__marg"></i>
            </div>
            {/* <div className="details__icons active">
              <i className="mdi mdi-chevron-up"></i>
            </div> */}
          </CardHeader>
          <Collapse isOpen={openCollapse["collapse1"]}>
            <CardBody>
              <ProfessionFormSection
                saveProfessionData={this.saveProfessionData}
                ProfessionFormData={ProfessionFormData}
                LandlordPersonaSchema={LandlordPersonaSchema}
              />
            </CardBody>
          </Collapse>
        </Card>

        <Card style={{ marginBottom: "1rem" }}>
          <CardHeader
            onClick={() => this.toggleCollapse("collapse2")}
            data-type="collapseLayout"
            className="card__title card__middle"
          >
            Accrediations *
            <div className="details__icons">
              <i className="mdi mdi-chevron-down down__marg"></i>
            </div>
          </CardHeader>
          <Collapse isOpen={openCollapse["collapse2"]}>
            <CardBody>
              <AccrediationsFieldSet
                setAccrediationData={this.setAccrediationData}
                accrediations={accrediations}
                LandlordPersonaSchema={LandlordPersonaSchema}
              />
            </CardBody>
          </Collapse>
        </Card>

        <Card style={{ marginBottom: "1rem" }}>
          <CardHeader
            onClick={() => this.toggleCollapse("collapse3")}
            data-type="collapseLayout"
            className="card__title card__middle"
          >
            Upload Documents *
            <div className="details__icons active">
              <i className="mdi mdi-chevron-up"></i>
            </div>
          </CardHeader>
          <Collapse isOpen={openCollapse["collapse3"]}>
            <CardBody>
              <DynamicFieldSet
                setSupportingDocuments={this.setSupportingDocuments}
                supportingDocuments={supportingDocuments}
                LandlordPersonaSchema={LandlordPersonaSchema}
              />
            </CardBody>
          </Collapse>
        </Card>

        <div className="row">
          <div className="col-lg-6">
            <div className="form-group">
              <button
                onClick={this.savePersonaLandlord}
                className="btn btns__blue--outline"
              >
                Save & Verify Later
              </button>
            </div>
          </div>

          <div className="col-lg-6">
            <div className="form-group">
              <button
                disabled={isSubmitting}
                onClick={this.savePersonaLandlord}
                className="btn btns__blue"
              >
                Verify Now
              </button>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default LandlordPersona;
