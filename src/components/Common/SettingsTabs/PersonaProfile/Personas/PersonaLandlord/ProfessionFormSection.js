import React from "react";
import { Datepicker } from "react-formik-ui";
import { Formik, Form, Field } from "formik";
import CustomInput from "../../../../../../config/FormikInput";
import MyNumberInput from "../../../../../../config/CustomNumberInput";

const professionList = ["Employed", "Self Employed", "Other"];

const jobType = [
  "Full-Time",
  "Part-Time",
  "Fixed-Term Contract",
  "Temporary Contract",
  "Interim",
  "Probation"
];

const ProfessionFormSection = props => {
  return (
    <Formik
      enableReinitialize
      initialValues={{ ...props.ProfessionFormData }}
      validationSchema={props.LandlordPersonaSchema.ProfessionSchema}
      onSubmit={(values, { validateForm, setSubmitting }) => {
        setSubmitting(true);
        props.saveProfessionData(values);
      }}
    >
      {({ isSubmitting, setFieldValue, values, errors }) => (
        <Form>
          <div className="tab__details">
            <div className="row">
              <div className="col-lg-6">
                <div className="form-group">
                  <label className="tab__deatils--label">
                    Profession
                  </label>
                  {values["profession"] === "Other" ? (
                    <CustomInput
                      placeholder="Enter Profession"
                      type="text"
                      name="professionCustom"
                      errors={errors}
                    />
                  ) : (
                    <Field
                      component="select"
                      name={`profession`}
                      className={
                        errors && errors["profession"]
                          ? "form-control error__field_show"
                          : "form-control tab__deatils--select"
                      }
                    >
                      <option selected disabled>
                       Select Profession
                      </option>
                      {professionList.map((docField, index) => {
                        return (
                          <option key={index} value={docField}>
                            {docField}
                          </option>
                        );
                      })}
                    </Field>
                  )}
                </div>
              </div>

              <div className="col-lg-6">
                <div className="form-group">
                  <label className="tab__deatils--label">
                    Select Job Type
                  </label>
                  <Field
                    component="select"
                    name={`jobType`}
                    className={
                      errors && errors["jobType"]
                        ? "form-control error__field_show"
                        : "form-control tab__deatils--select"
                    }
                  >
                    <option value selected disabled>
                      Select Job Type
                    </option>
                    {jobType.map((docField, index) => {
                      return (
                        <option key={index} value={docField}>
                          {docField}
                        </option>
                      );
                    })}
                  </Field>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-6">
                <div className="form-group">
                  <label className="tab__deatils--label">
                    Company / Business Name
                  </label>
                  <CustomInput
                    placeholder="Company / Business Name"
                    type="text"
                    name="companyName"
                    errors={errors}
                  />
                </div>
              </div>

              <div className="col-lg-6">
                <div className="form-group">
                  <label className="tab__deatils--label">
                    Job title
                  </label>
                  <CustomInput
                    placeholder="Job title"
                    type="text"
                    name="jobTitle"
                    errors={errors}
                  />
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-6">
                <div className="form-group">
                  <label className="tab__deatils--label">
                    Start Date
                  </label>
                  <div className="date__flex">
                    <div className="input-group-prepend">
                      <div className="input-group-text">
                        <i className="fa fa-calendar" />
                      </div>
                    </div>
                    <Datepicker
                      name="startDate"
                      todayButton="Today"
                      showYearDropdown
                      dateFormat="dd/MM/yyyy"
                      showMonthDropdown
                      dropdownMode={"select"}
                      disabledKeyboardNavigation={false}
                      className={
                        errors && errors["startDate"]
                          ? "tab__deatils--input error__field_show"
                          : "tab__deatils--input"
                      }
                    />
                  </div>
                </div>
              </div>

              <div className="col-lg-6">
                <div className="form-group">
                  <label className="tab__deatils--label">
                    Company Website
                  </label>
                  <CustomInput
                    placeholder="Website URL"
                    type="text"
                    name="companyWebsite"
                    errors={errors}
                  />
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-6">
                <div className="form-group">
                  <label className="tab__deatils--label">
                    Office Telephone
                  </label>
                  <MyNumberInput
                    placeholder="Office Telephone"
                    className={
                      errors && errors["companyTelephone"]
                        ? "tab__deatils--input error__field_show"
                        : "tab__deatils--input"
                    }
                    format="(###) ###-####"
                    mask="_"
                    value={values.companyTelephone}
                    onValueChange={val =>
                      setFieldValue("companyTelephone", String(val.floatValue))
                    }
                  />
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-4">
                <div className="form-group">
                  <button type="submit" className="btn btns__blue--outline">
                    Next
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default ProfessionFormSection;
