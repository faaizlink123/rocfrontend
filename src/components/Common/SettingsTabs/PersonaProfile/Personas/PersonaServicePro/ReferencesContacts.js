import React from "react";
import { Formik, Form, Field, FieldArray } from "formik";
import MyNumberInput from "../../../../../../config/CustomNumberInput";

class ReferencesContacts extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      referenceContacts: {
        referenceContacts: [{ contactName: "", email: "", phoneNumber: 123 }]
      }
    };
  }

  componentDidMount() {
    this.setState({
      referenceContacts: this.props.referenceContacts
    });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.referenceContacts) {
      return {
        referenceContacts: nextProps.referenceContacts
      };
    }
  }

  render() {
    let { referenceContacts } = this.state;
    return (
      <div>
        <Formik
          enableReinitialize
          initialValues={referenceContacts}
          onSubmit={values => this.props.setReferencesContacts(values)}
          render={({ values, setFieldValue }) => (
            <Form>
              <FieldArray
                name="referenceContacts"
                render={arrayHelpers => (
                  <div>
                    {values.referenceContacts.map((supportingDoc, index) => (
                      <div key={index}>
                        <div className="row">
                          <div className="col-md-4">
                          <div className="form-group">
                            <Field
                              type="text"
                              className="form-control select__global"
                              name={`referenceContacts.${index}.contactName`}
                            />
                          </div>
                          </div>

                          <div className="col-md-4">
                          <div className="form-group">
                            <Field
                              type="email"
                              className="form-control select__global"
                              name={`referenceContacts.${index}.email`}
                            />
                          </div>
                          </div>

                          <div className="col-md-4">
                          <div className="form-group">
                            <MyNumberInput
                              placeholder="Policy Amount"
                              className="form-control select__global"
                              format="(###) ###-####"
                              mask="_"
                              value={
                                values.referenceContacts[index].phoneNumber
                              }
                              onValueChange={val =>
                                setFieldValue(
                                  `referenceContacts[${index}].phoneNumber`,
                                  String(val.floatValue)
                                )
                              }
                            />
                          </div>
                          </div>
                        </div>

                        <div className="row">
                          <div className="col-md-4">
                        {values.referenceContacts.length > 1 && (
                          <button
                            className="btn btns__addmore"
                            onClick={() => arrayHelpers.remove(index)}
                          >
                            <i className="fas fa-minus"></i> Remove
                          </button>
                        )}
                        </div>
                        </div>
                        <hr />
                      </div>
                    ))}

                    <div className="row">
                      <div className="col-md-8"></div>
                      <div className="col-md-4">
                    <button
                      className="btn btns__addmore"
                      onClick={() =>
                        arrayHelpers.push({
                          contactName: "",
                          email: "",
                          phoneNumber: 123
                        })
                      }
                    >
                     <i className="fas fa-plus"></i> Add more
                    </button>
                    </div>
                    </div>
                  </div>
                )}
              />
              <div className="row">
                <div className="col-lg-6">
                  <div className="form-group">
                    <button type="submit" className="btn btns__blue">
                      Next
                    </button>
                  </div>
                </div>
              </div>
            </Form>
          )}
        />
      </div>
    );
  }
}

export default ReferencesContacts;
