import React from "react";
import ProfessionFormSection from "./ProfessionFormSection";
import DynamicFieldSet from "../DynamicFieldSet";
import ReferencesContacts from "./ReferencesContacts";
import AccrediationsFieldSet from "../DynamicFieldSetAccrediations";
import LandlordPersonaSchema from "../../../../../../config/FormSchemas/LandlordPersona";
import { Collapse, CardBody, Card, CardHeader } from "reactstrap";
import AddSkillsTags from "./AddServiceSkills";
import OtherInformation from "./OtherInformation";
import Axios from "axios";
import _ from "lodash";
import showMessage from "../../../../../../config/ShowLoadingMessage";
import showNotification from "../../../../../../config/Notification";

class ServiceProPersona extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      ServiceOrSkillTags: [],
      openCollapse: {
        collapse1: true,
        collapse2: false,
        collapse3: false,
        collapse4: false,
        collapse5: false,
        collapse6: false,
        isIncomplete: false
      },
      referenceContacts: {
        referenceContacts: [{ contactName: "", email: "", phoneNumber: 123 }]
      },
      accrediations: {
        accrediations: [
          {
            organization: "",
            documentNumber: "",
            validTillDate: new Date()
          }
        ],
        empty: true
      },
      ProfessionFormData: {
        companyName: "",
        UTR: "",
        startDate: new Date(),
        VAT: ""
      },
      initialOtherInformationData: {
        otherInformation: [
          { policyName: "", providerName: "", validTillDate: new Date() }
        ]
      },
      supportingDocuments: {
        supportingDocuments: [
          { document: "", documentNumber: "", docRaw: [], documentUrl: "" }
        ],
        empty: true
      }
    };
  }

  componentDidMount() {
    this.fetchServiceProPersona();
  }

  toggleCollapse = key => {
    let openCollapseData = this.state.openCollapse;
    Object.keys(openCollapseData).map((colKey, i) => {
      if (colKey === key) {
        openCollapseData[colKey] = true;
      } else {
        openCollapseData[colKey] = false;
      }
    });

    this.setState({ openCollapse: openCollapseData });
    this.forceUpdate();
  };

  fetchServiceProPersona = async () => {
    const { PersonaQueries, contextData } = this.props;
    contextData.startLoading();
    const fetchServiceProPersona = await this.props.client.query({
      query: PersonaQueries.fetchServiceProPersona
    });

    if (
      !_.isEmpty(
        fetchServiceProPersona.data.getServiceProviderPersonaInformation
      ) &&
      _.get(
        fetchServiceProPersona,
        "data.getServiceProviderPersonaInformation.success"
      )
    ) {
      let personaData = _.get(
        fetchServiceProPersona,
        "data.getServiceProviderPersonaInformation.data"
      );

      this.setState({
        ProfessionFormData: personaData.profession,
        accrediations: { accrediations: personaData.accreditation },
        supportingDocuments: {
          supportingDocuments: personaData.supportingDocuments
        },
        referenceContacts: { referenceContacts: personaData.referenceContacts },
        initialOtherInformationData: {
          otherInformation: personaData.otherInformation
        },
        ServiceOrSkillTags: personaData.serviceOrSkillTags
          ? personaData.serviceOrSkillTags
          : []
      });

      contextData.endLoading();
    }
  };

  checkIncompleteFields = async () => {
    let isIncomplete = this.state.isIncomplete;
    let profession = this.state["ProfessionFormData"];
    isIncomplete = !profession.empty ? false : true;

    this.setState({ isIncomplete });
  };

  saveProfessionData = ProfessionFormData => {
    ProfessionFormData["empty"] = false;
    this.setState({ ProfessionFormData });
    this.toggleCollapse("collapse2");
  };

  setAccrediationData = accrediations => {
    accrediations["empty"] = false;
    this.setState({ accrediations });
    this.toggleCollapse("collapse4");
  };

  setSupportingDocuments = supportingDocuments => {
    supportingDocuments["empty"] = false;
    this.setState({ supportingDocuments });
    this.toggleCollapse("collapse5");
  };

  setOtherInformation = initialOtherInformationData => {
    initialOtherInformationData["empty"] = false;
    this.setState({ initialOtherInformationData });
    this.toggleCollapse("collapse6");
  };

  setReferencesContacts = referenceContacts => {
    referenceContacts["empty"] = false;
    this.setState({ referenceContacts });
  };

  getBase64 = file => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  };

  checkEmpty = async () => {
    const {
      accrediations,
      ProfessionFormData,
      supportingDocuments,
      ServiceOrSkillTags,
      initialOtherInformationData,
      referenceContacts
    } = this.state;

    let update = false;

    if (accrediations.empty && !accrediations.empty) {
      update = true;
    }
    if (
      initialOtherInformationData.success &&
      !initialOtherInformationData.success
    ) {
      update = true;
    }

    if (referenceContacts.empty && !referenceContacts.empty) {
      update = true;
    }

    if (ServiceOrSkillTags.length !== 0) {
      update = true;
    }
  };

  savePersonaLandlord = async () => {
    const { PersonaQueries, contextData } = this.props;
    contextData.startLoading();
    this.setState({ isSubmitting: true });

    const {
      accrediations,
      ProfessionFormData,
      supportingDocuments,
      ServiceOrSkillTags,
      initialOtherInformationData,
      referenceContacts
    } = this.state;

    if (ProfessionFormData["profession"] === "Other") {
      ProfessionFormData["profession"] = ProfessionFormData["professionCustom"];
      delete ProfessionFormData["empty"];
      delete ProfessionFormData["professionCustom"];
    }

    delete ProfessionFormData["empty"];

    let uploadSupportingDocuments;
    let uploadableFiles = _.get(supportingDocuments, "supportingDocuments");

    if (!_.isEmpty(uploadableFiles)) {
      uploadSupportingDocuments = uploadableFiles.map(async (file, i) => {
        if (file.docRaw && !_.isEmpty(file.docRaw)) {
          showMessage("Uploading Documents...");

          let toUpload = file.docRaw[file.docRaw.length - 1];
          let getB64File = await this.getBase64(toUpload);

          let uploadedFile = await Axios.post(
            `${process.env.REACT_APP_SERVER}/api/v1/file-upload`,
            {
              file: getB64File,
              filename: toUpload.name
            }
          );

          if (_.get(uploadedFile, "data.success")) {
            let documentUrl = _.get(uploadedFile, "data.data");
            delete file["docRaw"];
            file["documentUrl"] = documentUrl;
          }
        } else {
          delete file["docRaw"];
          file["documentUrl"] = "";
        }
      });

      await Promise.all(uploadSupportingDocuments);
    }

    delete supportingDocuments["empty"];
    delete initialOtherInformationData["empty"];
    delete referenceContacts["empty"];
    showMessage("Saving Data...");

    let finalObj = {
      serviceOrSkillTags: ServiceOrSkillTags,
      accreditation: accrediations.accrediations,
      profession: ProfessionFormData,
      supportingDocuments: supportingDocuments.supportingDocuments,
      otherInformation: initialOtherInformationData.otherInformation,
      referenceContacts: referenceContacts.referenceContacts
    };

    const queryResponse = await this.props.client.mutate({
      mutation: PersonaQueries.ServiceProPersona,
      variables: finalObj
    });

    if (
      _.get(
        queryResponse,
        "data.updateServiceProviderPersonaInformation.success"
      )
    ) {
      showNotification(
        "success",
        "Persona Details Updated!",
        "Your Persona Details have been successfully updated!"
      );
      contextData.endLoading();
      this.setState({ isSubmitting: false });

      this.fetchServiceProPersona();
    } else {
      this.setState({ isSubmitting: false });

      contextData.endLoading();
      showNotification(
        "error",
        "An error occured",
        _.get(
          queryResponse,
          "data.updateServiceProviderPersonaInformation.message"
        )
      );
    }
  };

  updateSkillTags = tags => {
    this.setState({ ServiceOrSkillTags: tags });
    this.toggleCollapse("collapse3");
  };

  render() {
    const {
      openCollapse,
      ProfessionFormData,
      accrediations,
      supportingDocuments,
      ServiceOrSkillTags,
      initialOtherInformationData,
      referenceContacts,
      isSubmitting
    } = this.state;
    return (
      <>
        <Card style={{ marginBottom: "1rem" }}>
          <CardHeader
            onClick={() => this.toggleCollapse("collapse1")}
            data-type="collapseLayout"
            className="card__title card__middle"
          >
            Profession *
            <div className="details__icons">
              <i className="mdi mdi-chevron-down down__marg"></i>
            </div>
          </CardHeader>
          <Collapse isOpen={openCollapse["collapse1"]}>
            <CardBody>
              <ProfessionFormSection
                saveProfessionData={this.saveProfessionData}
                ProfessionFormData={ProfessionFormData}
                LandlordPersonaSchema={LandlordPersonaSchema}
              />
            </CardBody>
          </Collapse>
        </Card>

        <Card style={{ marginBottom: "1rem" }}>
          <CardHeader
            onClick={() => this.toggleCollapse("collapse2")}
            data-type="collapseLayout"
            className="card__title card__middle"
          >
            Tag Services or Skills *
            <div className="details__icons">
              <i className="mdi mdi-chevron-down down__marg"></i>
            </div>
          </CardHeader>
          <Collapse isOpen={openCollapse["collapse2"]}>
            <CardBody>
              <AddSkillsTags
                ServiceOrSkillTags={ServiceOrSkillTags}
                updateSkillTags={this.updateSkillTags}
              />
            </CardBody>
          </Collapse>
        </Card>

        <Card style={{ marginBottom: "1rem" }}>
          <CardHeader
            onClick={() => this.toggleCollapse("collapse3")}
            data-type="collapseLayout"
            className="card__title card__middle"
          >
            Accrediations *
            <div className="details__icons">
              <i className="mdi mdi-chevron-down down__marg"></i>
            </div>
          </CardHeader>
          <Collapse isOpen={openCollapse["collapse3"]}>
            <CardBody>
              <AccrediationsFieldSet
                setAccrediationData={this.setAccrediationData}
                accrediations={accrediations}
                LandlordPersonaSchema={LandlordPersonaSchema}
              />
            </CardBody>
          </Collapse>
        </Card>

        <Card style={{ marginBottom: "1rem" }}>
          <CardHeader
            onClick={() => this.toggleCollapse("collapse4")}
            data-type="collapseLayout"
            className="card__title card__middle"
          >
            Upload Documents *
            <div className="details__icons">
              <i className="mdi mdi-chevron-down down__marg"></i>
            </div>
          </CardHeader>
          <Collapse isOpen={openCollapse["collapse4"]}>
            <CardBody>
              <DynamicFieldSet
                setSupportingDocuments={this.setSupportingDocuments}
                supportingDocuments={supportingDocuments}
                LandlordPersonaSchema={LandlordPersonaSchema}
              />
            </CardBody>
          </Collapse>
        </Card>

        <Card style={{ marginBottom: "1rem" }}>
          <CardHeader
            onClick={() => this.toggleCollapse("collapse5")}
            data-type="collapseLayout"
            className="card__title card__middle"
          >
            Other Information *
            <div className="details__icons">
              <i className="mdi mdi-chevron-down down__marg"></i>
            </div>
          </CardHeader>
          <Collapse isOpen={openCollapse["collapse5"]}>
            <CardBody>
              <OtherInformation
                initialOtherInformationData={initialOtherInformationData}
                setOtherInformation={this.setOtherInformation}
              />
            </CardBody>
          </Collapse>
        </Card>

        <Card style={{ marginBottom: "1rem" }}>
          <CardHeader
            onClick={() => this.toggleCollapse("collapse6")}
            data-type="collapseLayout"
            className="card__title card__middle"
          >
            References Contact *
            <div className="details__icons">
              <i className="mdi mdi-chevron-down down__marg"></i>
            </div>
          </CardHeader>
          <Collapse isOpen={openCollapse["collapse6"]}>
            <CardBody>
              <ReferencesContacts
                referenceContacts={referenceContacts}
                setReferencesContacts={this.setReferencesContacts}
              />
            </CardBody>
          </Collapse>
        </Card>

        <div className="row">
          <div className="col-lg-6">
            <div className="form-group">
              <button
                disabled={isSubmitting}
                onClick={this.savePersonaLandlord}
                className="btn btns__blue--outline"
              >
                Save & Verify Later
              </button>
            </div>
          </div>

          <div className="col-lg-6">
            <div className="form-group">
              <button
                onClick={this.savePersonaLandlord}
                className="btn btns__blue"
              >
                Verify Now
              </button>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default ServiceProPersona;
