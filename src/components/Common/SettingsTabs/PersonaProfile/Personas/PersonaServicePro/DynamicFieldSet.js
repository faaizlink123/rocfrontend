import React from "react";
import { Formik, Form, Field, FieldArray } from "formik";
import FileUpload from "./Dropzone";

const dropzoneStyle = {
  width: "100%",
  height: "100px",
  borderWidth: 2,
  borderColor: "rgb(102, 102, 102)",
  borderStyle: "dashed",
  borderRadius: 5
};

let docsDropdown = [
  "Identity Card",
  "Passport",
  "Utility bill",
  "Bank / Card Statement",
  "Education Certificate",
  "Professional Certificate",
  "Accredited Membership",
  "Employment Document",
  "Business Financial Statement",
  "Other"
];

class DynamicFieldSet extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      supportingDocuments: {
        supportingDocuments: [
          { document: "", documentNumber: "", docRaw: [], documentUrl: "" }
        ]
      }
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.supportingDocuments) {
      return { supportingDocuments: nextProps.supportingDocuments };
    }
  }

  render() {
    const { supportingDocuments } = this.state;
    return (
      <div>
        <h1>Supporting Documents</h1>
        <Formik
          enableReinitialize
          initialValues={supportingDocuments}
          onSubmit={values => this.props.setSupportingDocuments(values)}
          render={({ values, setFieldValue }) => (
            <Form>
              <FieldArray
                name="supportingDocuments"
                render={arrayHelpers => (
                  <div>
                    {values.supportingDocuments.map((supportingDoc, index) => (
                      <div key={index}>
                        <Field
                          component="select"
                          name={`supportingDocuments[${index}].document`}
                          className="form-control"
                        >
                          <option disabled selected>
                            Select Document
                          </option>
                          {docsDropdown.map((docField, index) => {
                            return (
                              <option key={index} value={docField}>
                                {docField}
                              </option>
                            );
                          })}
                        </Field>

                        <Field
                          type="text"
                          className="form-control"
                          name={`supportingDocuments.${index}.documentNumber`}
                        />

                        <FileUpload
                          key={supportingDoc}
                          fieldVal={supportingDoc}
                          style={dropzoneStyle}
                          setFieldValue={setFieldValue}
                          setFieldValueData={{
                            main: "supportingDocuments",
                            index,
                            fileKey: "docRaw"
                          }}
                        />

                        {values.supportingDocuments.length > 1 && (
                          <button
                            type="button"
                            onClick={() => arrayHelpers.remove(index)}
                          >
                            -
                          </button>
                        )}
                      </div>
                    ))}
                    <button
                      type="button"
                      onClick={() =>
                        arrayHelpers.push({
                          document: "",
                          documentNumber: "",
                          docRaw: [],
                          documentUrl: ""
                        })
                      }
                    >
                      +
                    </button>
                  </div>
                )}
              />
              <div className="row">
                <div className="col-lg-6">
                  <div className="form-group">
                    <button type="submit" className="btn btns__blue">
                      Save
                    </button>
                  </div>
                </div>
              </div>
            </Form>
          )}
        />
      </div>
    );
  }
}

export default DynamicFieldSet;
