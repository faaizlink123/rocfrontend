import React, { useState, useEffect } from "react";
import { Formik, Form } from "formik";
import TagsInput from "react-tagsinput";
import "react-tagsinput/react-tagsinput.css";

const AddSkillsTags = props => {
  const updateTags = tags => {
    setTags(tags);
  };

  const saveUpdateTags = async () => {
    props.updateSkillTags(ServiceOrSkillTags);
  };

  const [ServiceOrSkillTags, setTags] = useState(props.ServiceOrSkillTags);

  useEffect(() => {
    setTags(props.ServiceOrSkillTags);
  }, [props.ServiceOrSkillTags]);

  return (
    <Formik
      enableReinitialize
      initialValues={{ ...props.ProfessionFormData }}
      onSubmit={(values, { validateForm, setSubmitting }) => {
        setSubmitting(true);
      }}
    >
      {({ isSubmitting, setFieldValue, values, errors }) => (
        <Form>
          <div className="tab__details">
          <label className="tab__deatils--label">Add Profession(s)</label>
            <div className="form-group">
              <TagsInput value={ServiceOrSkillTags} onChange={updateTags} className="tab__deatils--input" />
            </div>

            <div className="row">
              <div className="col-lg-6">
                <div className="form-group">
                  <button
                    onClick={saveUpdateTags}
                    type="submit"
                    className="btn btns__blue"
                  >
                    Next
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default AddSkillsTags;
