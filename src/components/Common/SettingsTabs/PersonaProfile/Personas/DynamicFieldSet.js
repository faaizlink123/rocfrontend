import React from "react";
import { Formik, Form, Field, FieldArray } from "formik";
import FileUpload from "./Dropzone";

const dropzoneStyle = {
  width: "100%",
  height: "100px",
  borderWidth: 2,
  borderColor: "rgb(102, 102, 102)",
  borderStyle: "dashed",
  borderRadius: 5
};

let docsDropdown = [
  "Identity Card",
  "Passport",
  "Utility bill",
  "Bank / Card Statement",
  "Education Certificate",
  "Professional Certificate",
  "Accredited Membership",
  "Employment Document",
  "Business Financial Statement",
  "Other"
];

class DynamicFieldSet extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      supportingDocuments: {
        supportingDocuments: [
          { document: "", documentNumber: "", docRaw: [], documentUrl: "" }
        ]
      }
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.supportingDocuments) {
      return { supportingDocuments: nextProps.supportingDocuments };
    }
  }

  render() {
    const { supportingDocuments } = this.state;

    console.log(supportingDocuments);
    return (
      <div>
        <Formik
          enableReinitialize
          initialValues={supportingDocuments}
          onSubmit={values => this.props.setSupportingDocuments(values)}
          render={({ values, setFieldValue }) => (
            <Form>
              <FieldArray
                name="supportingDocuments"
                render={arrayHelpers => (
                  <div>
                    {values.supportingDocuments.map((supportingDoc, index) => (
                      <div key={index}>

                      <div className="row">

                      <div className="col-md-4">
                        <Field
                          component="select"
                          name={`supportingDocuments[${index}].document`}
                          className="form-control tab__select"
                        >
                          <option disabled selected>
                           Select Document
                          </option>
                          {docsDropdown.map((docField, index) => {
                            return (
                              <option key={index} value={docField}>
                                {docField}
                              </option>
                            );
                          })}
                        </Field>
                        </div>

                        <div className="col-md-4">
                        <Field
                          type="text"
                          className="form-control tab__input"
                          placeholder="Document name"
                          name={`supportingDocuments.${index}.documentNumber`}
                        />
                        </div>

                        <div className="col-md-4 flex__browse">
                        <FileUpload
                          key={supportingDoc}
                          fieldVal={supportingDoc}
                          style={dropzoneStyle}
                          setFieldValue={setFieldValue}
                          setFieldValueData={{
                            main: "supportingDocuments",
                            index,
                            fileKey: "docRaw"
                          }}
                        />

                        <span className="img-supported-doc" />
                        </div>



                        </div>

                        {values.supportingDocuments.length > 1 && (
                          <div className="row">
                          <div className="col-lg-4">
                          <button
                            type="button" className="btn btns__addmore btn__gap"
                            onClick={() => arrayHelpers.remove(index)}
                          >
                            <i className="fas fa-minus"></i> Remove
                          </button>
                          </div>
                          </div>
                        )}
                      </div>
                    ))}
                    <div className="row">
                      <div className="col-lg-7"> </div>
                        <div className="col-lg-5">  
                    <button
                      type="button" className="btn btns__addmore"
                      onClick={() =>
                        arrayHelpers.push({
                          document: "",
                          documentNumber: "",
                          docRaw: [],
                          documentUrl: ""
                        })
                      }
                    >
                      <i className="fas fa-plus"></i> Add more
                    </button>
                    </div>
                    </div>
                  </div>
                )}
              />
              <div className="row">
                <div className="col-lg-6">
                  <div className="form-group">
                    <button type="submit" className="btn btns__blue">
                      Save
                    </button>
                  </div>
                </div>
              </div>

              
            </Form>
          )}
        />
      </div>
    );
  }
}

export default DynamicFieldSet;
