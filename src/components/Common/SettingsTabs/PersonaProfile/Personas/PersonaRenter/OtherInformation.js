import React, { useState, useEffect } from "react";
import { Datepicker } from "react-formik-ui";
import { Formik, Form } from "formik";
import MyNumberInput from "../../../../../../config/CustomNumberInput";

const ProfessionFormSection = props => {
  const [smoking, setSmokingStatus] = useState(props.otherInformation.smoking);
  const [disability, setDisabledStatus] = useState(
    props.otherInformation.disability
  );
  const [incomeSupport, setIncomeSupportStatus] = useState(
    props.otherInformation.incomeSupport
  );

  useEffect(() => {
    setSmokingStatus(props.otherInformation.smoking);
    setDisabledStatus(props.otherInformation.disability);
    setIncomeSupportStatus(props.otherInformation.incomeSupport);
  }, [props, props.accountPreferences]);

  return (
    <Formik
      enableReinitialize
      initialValues={{ ...props.otherInformation }}
      //   validationSchema={props.LandlordPersonaSchema.ProfessionSchema}
      onSubmit={(values, { validateForm, setSubmitting }) => {
        setSubmitting(true);
        values.smoking = smoking;
        values.disability = disability;
        values.incomeSupport = incomeSupport;
        props.saveOtherInformation(values);
      }}
    >
      {({ isSubmitting, setFieldValue, values, errors }) => (
        <Form>
          <div className="tab__details">
            <div className="row">
              <div className="col-lg-6">
                <div className="form-group">
                <div className="flex__div">
                <div className="input-group-prepend">
                      <div className="input-group-text input_icon border-radius__l">
                        <i className="mdi mdi-account-tie" />
                      </div>
                    </div>
                  <MyNumberInput
                    placeholder="No. of Adult"
                    className={
                      errors && errors["noOfAdult"]
                        ? "tab__deatils--input error__field_show"
                        : "tab__deatils--input"
                    }
                    mask="_"
                    value={values.noOfAdult}
                    onValueChange={val =>
                      setFieldValue("noOfAdult", val.floatValue)
                    }
                  />
                  </div>
                </div>
              </div>

              <div className="col-lg-6">
                <div className="form-group">
                <div className="flex__div">
                <div className="input-group-prepend">
                      <div className="input-group-text input_icon border-radius__l">
                        <i className="mdi mdi-baby" />
                      </div>
                    </div>
                  <MyNumberInput
                    placeholder="No of child"
                    className={
                      errors && errors["noOfChild"]
                        ? "tab__deatils--input error__field_show"
                        : "tab__deatils--input"
                    }
                    mask="_"
                    value={values.noOfChild}
                    onValueChange={val =>
                      setFieldValue("noOfChild", val.floatValue)
                    }
                  />
                  </div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-6">
                <div className="form-group">
                <div className="flex__div">
                <div className="input-group-prepend">
                      <div className="input-group-text input_icon border-radius__l">
                        <i className="mdi mdi-dog-side" />
                      </div>
                    </div>
                  <MyNumberInput
                    placeholder="No of pets"
                    className={
                      errors && errors["noOfPets"]
                        ? "tab__deatils--input error__field_show"
                        : "tab__deatils--input"
                    }
                    mask="_"
                    value={values.noOfPets}
                    onValueChange={val =>
                      setFieldValue("noOfPets", val.floatValue)
                    }
                  />
                </div>
              </div>
              </div>

              <div className="col-lg-6">
                <div className="form-group">
                <div className="flex__div">
                <div className="input-group-prepend">
                      <div className="input-group-text input_icon border-radius__l">
                        <i className="mdi mdi-car" />
                      </div>
                    </div>
                  <MyNumberInput
                    placeholder="No of cars"
                    className={
                      errors && errors["noOfCars"]
                        ? "tab__deatils--input error__field_show"
                        : "tab__deatils--input"
                    }
                    mask="_"
                    value={values.noOfCars}
                    onValueChange={val =>
                      setFieldValue("noOfCars", val.floatValue)
                    }
                  />
                </div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-6">
                <div className="form-group">
                <div className="flex__div">
                <div className="input-group-prepend">
                      <div className="input-group-text input_icon border-radius__l">
                        <i className="mdi mdi-calendar" />
                      </div>
                    </div>
                  <Datepicker
                    name="moveInDate"
                    placeholder="Move In Date"
                    todayButton="Today"
                    showYearDropdown
                    dateFormat="dd/MM/yyyy"
                    showMonthDropdown
                    dropdownMode={"select"}
                    style={{ width: '100%' }}
                    disabledKeyboardNavigation={false}
                    className={
                      errors && errors["moveInDate"]
                        ? "tab__deatils--input error__field_show"
                        : "tab__deatils--input"
                    }
                  />
                </div>
              </div>
              </div>

              <div className="col-lg-6">
                <div className="form-group">
                  <div className="form-group yes__no--toogle">
                  
                  <label className="tab__deatils--label toggle_icon"><i className="mdi mdi-smoking fs-22"></i> Smoking?</label>
                    <div class="btn-group">
                      <a
                        href
                        className={
                          smoking
                            ? "btn btn-success btn-md active activeShowCompany"
                            : "btn btn-success btn-md active notActiveShowCompany"
                        }
                        data-toggle="company"
                        data-title="Y"
                        onClick={() => setSmokingStatus(true)}
                      >
                        YES
                      </a>
                      <a
                        href
                        className={
                          !smoking
                            ? "btn btn-light btn-md no_activeShowCompany"
                            : "btn btn-light btn-md no_notActiveShowCompany"
                        }
                        data-toggle="company"
                        data-title="N"
                        onClick={() => setSmokingStatus(false)}
                      >
                        NO
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-6">
                <div className="form-group yes__no--toogle">
                  <label className="tab__deatils--label toggle_icon"><i className="mdi mdi-wheelchair-accessibility fs-22"></i> Disability?</label>
                  <div class="btn-group">
                    <a
                      href
                      className={
                        disability
                          ? "btn btn-success btn-md active activeShowCompany"
                          : "btn btn-success btn-md active notActiveShowCompany"
                      }
                      data-toggle="company"
                      data-title="Y"
                      onClick={() => setDisabledStatus(true)}
                    >
                      YES
                    </a>
                    <a
                      href
                      className={
                        !disability
                          ? "btn btn-light btn-md no_activeShowCompany"
                          : "btn btn-light btn-md no_notActiveShowCompany"
                      }
                      data-toggle="company"
                      data-title="N"
                      onClick={() => setDisabledStatus(false)}
                    >
                      NO
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-6">
                <div className="form-group yes__no--toogle">
                  <label className="tab__deatils--label toggle_icon"><i className="mdi mdi-account-supervisor fs-22"></i>
                    {" "}
                     Income Support ?
                  </label>
                  <div class="btn-group">
                    <a
                      href
                      className={
                        incomeSupport
                          ? "btn btn-success btn-md active activeShowCompany"
                          : "btn btn-success btn-md active notActiveShowCompany"
                      }
                      data-toggle="company"
                      data-title="Y"
                      onClick={() => setIncomeSupportStatus(true)}
                    >
                      YES
                    </a>
                    <a
                      href
                      className={
                        !incomeSupport
                          ? "btn btn-light btn-md no_activeShowCompany"
                          : "btn btn-light btn-md no_notActiveShowCompany"
                      }
                      data-toggle="company"
                      data-title="N"
                      onClick={() => setIncomeSupportStatus(false)}
                    >
                      NO
                    </a>
                  </div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-4">
                <div className="form-group">
                  <button type="submit" className="btn btns__blue">
                    Next
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default ProfessionFormSection;
