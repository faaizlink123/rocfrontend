import React, { useState } from "react";
import ValueTrue from "./RenterAddressTrue";
import LoqateAddress from "../../../../../../config/AddressAutoCompleteLoqate";
import LoqateAddressFull from "../../../../../../config/LoqateGetFullAddress";
import SuggestionDropdown from "../../../ProfileInfo/Downshift";

const ProfessionFormSection = props => {
  const [showCompany, setCompanyStatus] = useState(false);

  const findAddress = async address => {
    let fullAddress = await LoqateAddressFull(address.Id);
    let completeAddress = fullAddress.Items[0];

    setLoqateData({
      fullAddress: completeAddress.Label
    });
  };

  const saveData = data => {
    data["isSameAddress"] = showCompany;
    // data["fullAddress"] = loqateData.fullAddress;
    props.saveLandlordAgentReference(data);
  };

  const [loqateData, setLoqateData] = useState({});

  return (
    <>
      <div className="form-group yes__no--toogle">
        <label className="tab__deatils--label">
          Is your rented address is same as current Address ?
        </label>
        <div class="btn-group">
          <a
            href
            className={
              showCompany
                ? "btn btn-success btn-md active activeShowCompany"
                : "btn btn-success btn-md active notActiveShowCompany"
            }
            data-toggle="company"
            data-title="Y"
            onClick={() => setCompanyStatus(true)}
          >
            YES
          </a>
          <a
            href
            className={
              !showCompany
                ? "btn btn-light btn-md no_activeShowCompany"
                : "btn btn-light btn-md no_notActiveShowCompany"
            }
            data-toggle="company"
            data-title="N"
            onClick={() => setCompanyStatus(false)}
          >
            NO
          </a>
        </div>
      </div>

      {showCompany ? (
        <>
          <SuggestionDropdown
            LoqateAddress={LoqateAddress}
            findAddress={findAddress}
            loqateData={loqateData}
          />

          <input
            type="text"
            className="form-control"
            value={loqateData.fullAddress}
          />
        </>
      ) : (
        <></>
      )}

      <ValueTrue saveData={saveData} {...props} />
    </>
  );
};

export default ProfessionFormSection;
