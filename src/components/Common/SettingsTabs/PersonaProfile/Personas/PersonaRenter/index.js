import React from "react";
import ProfessionFormSection from "./IncomeFormSection";
import DynamicFieldSet from "../DynamicFieldSet";
import OtherInformation from "./OtherInformation";
import LandlordAgentReference from "./LandlordAgentReference";
import LandlordPersonaSchema from "../../../../../../config/FormSchemas/LandlordPersona";
import RightToRent from "./RightToRent";
import { Collapse, CardBody, Card, CardHeader } from "reactstrap";
import Axios from "axios";
import _ from "lodash";
import showMessage from "../../../../../../config/ShowLoadingMessage";
import showNotification from "../../../../../../config/Notification";

class RenterPersona extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      openCollapse: {
        collapse1: true,
        collapse2: false,
        collapse3: false,
        collapse4: false,
        collapse5: false,
        isIncomplete: false
      },
      landLordOrAgentReference: {
        isSameAddress: false,
        landlordName: "", // Landlord/Agent's Full Name
        landlordContactNumber: "", // Landlord/Agent's Contact Number
        landlordEmail: "", //Landlord/Agent's Email
        rentPerMonth: "",
        rentalStartDate: new Date(),
        durationInMonth: "1"
      },
      rightToRent: {
        swissPassport: false,
        swissNationalID: false,
        documentCertifyingPermanentResidence: false,
        permanentResidentCard: false,
        biometricResidencePermit: false,
        PassportOrTravelDocument: false,
        immigrationStatusDocument: false,
        registrationAsBritishCitizen: false,
        passportEndorsed: false,
        biometricImmigrationDocument: false,
        nationalResidentCard: false,
        endorsementFromHomeOffice: false
      },
      otherInformation: {
        moveInDate: new Date(),
        smoking: false,
        incomeSupport: false,
        disability: false
      },
      income: {
        startDate: new Date(),
        endDate: new Date(),
        salary: {
          amount: 0
        }
      },
      supportingDocuments: {
        supportingDocuments: [
          { document: "", documentNumber: "", docRaw: [], documentUrl: "" }
        ]
      }
    };
  }

  componentDidMount() {
    this.fetchLandlordPersona();
  }

  fetchLandlordPersona = async () => {
    const { PersonaQueries, contextData } = this.props;
    contextData.startLoading();
    const fetchLandlordPersonas = await this.props.client.query({
      query: PersonaQueries.fetchRenterPersona
    });

    if (
      !_.isEmpty(fetchLandlordPersonas.data.getRenterPersonaInformation) &&
      _.get(fetchLandlordPersonas, "data.getRenterPersonaInformation.success")
    ) {
      let personaData = _.get(
        fetchLandlordPersonas,
        "data.getRenterPersonaInformation.data"
      );

      let obj = { ...personaData };
      delete obj["supportingDocuments"];

      this.setState({
        ...obj,
        supportingDocuments: {
          supportingDocuments: personaData.supportingDocuments
        }
      });

      contextData.endLoading();
    }
  };

  toggleCollapse = key => {
    let openCollapseData = this.state.openCollapse;
    Object.keys(openCollapseData).map((colKey, i) => {
      if (colKey === key) {
        openCollapseData[colKey] = true;
      } else {
        openCollapseData[colKey] = false;
      }
    });

    this.setState({ openCollapse: openCollapseData });
    this.forceUpdate();
  };

  saveProfessionData = income => {
    this.setState({ income });
    this.toggleCollapse("collapse2");
  };

  updateRTAData = (event, id) => {
    let initialData = this.state.rightToRent;

    initialData[id] = !initialData[id];

    this.setState({ rightToRent: initialData });
  };

  saveLandlordAgentReference = landLordOrAgentReference => {
    this.setState({ landLordOrAgentReference });
    this.toggleCollapse("collapse3");
  };

  saveOtherInformation = otherInformation => {
    this.setState({ otherInformation });
    this.toggleCollapse("collapse4");
  };

  setSupportingDocuments = supportingDocuments => {
    this.setState({ supportingDocuments });
  };

  getBase64 = file => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  };

  savePersonaLandlord = async () => {
    const {
      otherInformation,
      income,
      supportingDocuments,
      rightToRent,
      landLordOrAgentReference
    } = this.state;

    const { PersonaQueries, contextData } = this.props;
    contextData.startLoading();
    this.setState({ isSubmitting: true });
    let uploadSupportingDocuments;
    let uploadableFiles = _.get(supportingDocuments, "supportingDocuments");

    if (!_.isEmpty(uploadableFiles)) {
      uploadSupportingDocuments = uploadableFiles.map(async (file, i) => {
        if (file.docRaw && !_.isEmpty(file.docRaw)) {
          showMessage("Uploading Documents...");

          let toUpload = file.docRaw[file.docRaw.length - 1];
          let getB64File = await this.getBase64(toUpload);

          let uploadedFile = await Axios.post(
            `${process.env.REACT_APP_SERVER}/api/v1/file-upload`,
            {
              file: getB64File,
              filename: toUpload.name
            }
          );

          if (_.get(uploadedFile, "data.success")) {
            let documentUrl = _.get(uploadedFile, "data.data");
            delete file["docRaw"];
            file["documentUrl"] = documentUrl;
          }
        } else {
          delete file["docRaw"];
          file["documentUrl"] = "";
        }
      });

      await Promise.all(uploadSupportingDocuments);
    }

    delete supportingDocuments["empty"];

    showMessage("Saving Data...");

    const queryResponse = await this.props.client.mutate({
      mutation: PersonaQueries.updateRenterPersona,
      variables: {
        otherInformation,
        income,
        rightToRent,
        landLordOrAgentReference,
        supportingDocuments: supportingDocuments.supportingDocuments
      }
    });

    if (_.get(queryResponse, "data.updateRenterPersonaInformation.success")) {
      showNotification(
        "success",
        "Persona Details Updated!",
        "Your Persona Details have been successfully updated!"
      );
      contextData.endLoading();
      this.setState({ isSubmitting: false });
      this.fetchLandlordPersona();
    } else {
      contextData.endLoading();
      this.setState({ isSubmitting: false });

      showNotification(
        "error",
        "An error occured",
        _.get(queryResponse, "data.updateRenterPersonaInformation.message")
      );
    }
  };

  render() {
    const {
      openCollapse,
      income,
      supportingDocuments,
      rightToRent,
      otherInformation,
      landLordOrAgentReference,
      isSubmitting
    } = this.state;
    return (
      <>
        <Card style={{ marginBottom: "1rem" }}>
          <CardHeader
            onClick={() => this.toggleCollapse("collapse1")}
            data-type="collapseLayout"
            className="card__title card__middle"
          >
            Income *
            <div className="details__icons">
              <i className="mdi mdi-chevron-down down__marg"></i>
              </div>
          </CardHeader>
          <Collapse isOpen={openCollapse["collapse1"]}>
            <CardBody>
              <ProfessionFormSection
                saveProfessionData={this.saveProfessionData}
                income={income}
                LandlordPersonaSchema={LandlordPersonaSchema}
              />
            </CardBody>
          </Collapse>
        </Card>

        <Card style={{ marginBottom: "1rem" }}>
          <CardHeader
            onClick={() => this.toggleCollapse("collapse2")}
            data-type="collapseLayout"
            className="card__title card__middle"
          >
            Landlord and Agent Reference *
            <div className="details__icons">
              <i className="mdi mdi-chevron-down down__marg"></i>
              </div>
          </CardHeader>
          <Collapse isOpen={openCollapse["collapse2"]}>
            <CardBody>
              <LandlordAgentReference
                landLordOrAgentReference={landLordOrAgentReference}
                LandlordPersonaSchema={LandlordPersonaSchema}
                saveLandlordAgentReference={this.saveLandlordAgentReference}
              />
            </CardBody>
          </Collapse>
        </Card>

        <Card style={{ marginBottom: "1rem" }}>
          <CardHeader
            onClick={() => this.toggleCollapse("collapse3")}
            data-type="collapseLayout"
            className="card__title card__middle"
          >
            Other Information *
            <div className="details__icons">
              <i className="mdi mdi-chevron-down down__marg"></i>
              </div>
          </CardHeader>
          <Collapse isOpen={openCollapse["collapse3"]}>
            <CardBody>
              <OtherInformation
                saveOtherInformation={this.saveOtherInformation}
                otherInformation={otherInformation}
                LandlordPersonaSchema={LandlordPersonaSchema}
              />
            </CardBody>
          </Collapse>
        </Card>

        <Card style={{ marginBottom: "1rem" }}>
          <CardHeader
            onClick={() => this.toggleCollapse("collapse4")}
            data-type="collapseLayout"
            className="card__title card__middle"
          >
            Right to Rent (Prelimenary) *
            <div className="details__icons">
              <i className="mdi mdi-chevron-down down__marg"></i>
              </div>
          </CardHeader>
          <Collapse isOpen={openCollapse["collapse4"]}>
            <CardBody className="cardbody__Padd">
              <RightToRent
                rightToRent={rightToRent}
                updateRTAData={this.updateRTAData}
              />
            </CardBody>
          </Collapse>
        </Card>

        <Card style={{ marginBottom: "1rem" }}>
          <CardHeader
            onClick={() => this.toggleCollapse("collapse5")}
            data-type="collapseLayout"
            className="card__title card__middle"
          >
            Upload Documents *
            <div className="details__icons">
              <i className="mdi mdi-chevron-down down__marg"></i>
              </div>
          </CardHeader>
          <Collapse isOpen={openCollapse["collapse5"]}>
            <CardBody>
              <DynamicFieldSet
                setSupportingDocuments={this.setSupportingDocuments}
                supportingDocuments={supportingDocuments}
                LandlordPersonaSchema={LandlordPersonaSchema}
              />
            </CardBody>
          </Collapse>
        </Card>
        <div className="row">
          <div className="col-lg-6">
            <div className="form-group">
              <button
                 disabled={isSubmitting}
                 onClick={this.savePersonaLandlord}
                className="btn btns__blue--outline"
              >
                Save & Verify Later
              </button>
            </div>
          </div>

          <div className="col-lg-6">
            <div className="form-group">
              <button
                disabled={isSubmitting}
                onClick={this.savePersonaLandlord}
                className="btn btns__blue"
              >
                Verify Now
              </button>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default RenterPersona;
