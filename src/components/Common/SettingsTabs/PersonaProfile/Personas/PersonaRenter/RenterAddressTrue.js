import React from "react";
import { Datepicker } from "react-formik-ui";
import { Formik, Form, Field } from "formik";
import CustomInput from "../../../../../../config/FormikInput";
import MyNumberInput from "../../../../../../config/CustomNumberInput";

const residencyStatuses = [
  "Renting",
  "Owned / Mortgaged",
  "Staying with parents/ family"
];

const ProfessionFormSection = props => {
  return (
    <Formik
      enableReinitialize
      initialValues={{ ...props.landLordOrAgentReference }}
      //   validationSchema={props.LandlordPersonaSchema.ProfessionSchema}
      onSubmit={(values, { validateForm, setSubmitting }) => {
        setSubmitting(true);
        values.durationInMonth = String(values.durationInMonth);
        props.saveData(values);
      }}
    >
      {({ isSubmitting, setFieldValue, values, errors }) => (
        <Form>
          <div className="tab__details">
            <div className="row">
              <div className="col-lg-6">
                <div className="form-group">
                  <label className="tab__deatils--label">
                    Current Residency Status
                  </label>
                  <Field
                    component="select"
                    name={`currentResidencyStatus`}
                    className={
                      errors && errors["currentResidencyStatus"]
                        ? "form-control error__field_show"
                        : "form-control tab__deatils--select"
                    }
                  >
                    <option selected disabled>
                      Select Status
                    </option>
                    {residencyStatuses.map((docField, index) => {
                      return (
                        <option key={index} value={docField}>
                          {docField}
                        </option>
                      );
                    })}
                  </Field>
                </div>
              </div>
            
              <div className="col-lg-6">
                <div className="form-group">
                  <label className="tab__deatils--label">
                    Landlord/Agent's Full Name <span>*</span>
                  </label>
                  <CustomInput
                    placeholder="Enter Landlord/Agent's Full Name"
                    type="text"
                    name="landlordName"
                    errors={errors}
                  />
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-6">
                <div className="form-group">
                  <label className="tab__deatils--label">
                    Rent Per Month <span>*</span>
                  </label>
                  <MyNumberInput
                    placeholder="Rent Per Month"
                    className={
                      errors && errors["rentPerMonth"]
                        ? "tab__deatils--input error__field_show"
                        : "tab__deatils--input"
                    }
                    mask="_"
                    value={values.rentPerMonth}
                    onValueChange={val =>
                      setFieldValue("rentPerMonth", String(val.floatValue))
                    }
                  />
                </div>
                </div>

                <div className="col-lg-6">
                <div className="form-group">
                  <label className="tab__deatils--label">
                    Duration in Month <span>*</span>
                  </label>
                  <CustomInput
                    placeholder="Duration in month"
                    type="number"
                    min="1"
                    max="12"
                    name="durationInMonth"
                    errors={errors}
                  />
                </div>
                </div>
              </div>

              <div className="row">
                <div className="col-lg-6">
                  <div className="form-group">
                    <label className="tab__deatils--label">
                      Landlord/Agent's Contact Number <span>*</span>
                    </label>
                    <MyNumberInput
                      placeholder="Landlord/Agent's Contact Number"
                      className={
                        errors && errors["landlordContactNumber"]
                          ? "tab__deatils--input error__field_show"
                          : "tab__deatils--input"
                      }
                      format="(###) ###-####"
                      mask="_"
                      value={values.landlordContactNumber}
                      onValueChange={val =>
                        setFieldValue(
                          "landlordContactNumber",
                          String(val.floatValue)
                        )
                      }
                    />
                  </div>
</div> 
<div className="col-lg-6">
                  <div className="form-group">
                    <label className="tab__deatils--label">
                      Landlord/Agent's Email <span>*</span>
                    </label>

                    <CustomInput
                      placeholder="Enter Agent's email"
                      type="email"
                      name="landlordEmail"
                      errors={errors}
                    />
                  </div>
                </div>
              </div>
       

            <div className="row">
              <div className="col-lg-6">
                <div className="form-group">
                  <label className="tab__deatils--label">
                    Rental Start Date <span>*</span>
                  </label>
                  <div className="date__flex">
                    <div className="input-group-prepend">
                      <div className="input-group-text">
                        <i className="fa fa-calendar" />
                      </div>
                    </div>
                    <Datepicker
                      name="rentalStartDate"
                      todayButton="Today"
                      showYearDropdown
                      dateFormat="dd/MM/yyyy"
                      showMonthDropdown
                      dropdownMode={"select"}
                      disabledKeyboardNavigation={false}
                      className={
                        errors && errors["rentalStartDate"]
                          ? "tab__deatils--input error__field_show"
                          : "tab__deatils--input"
                      }
                    />
                  </div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-4">
                <div className="form-group">
                  <button type="submit" className="btn btns__blue">
                    Next
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default ProfessionFormSection;
