import PersonaLandlord from "./PersonaLandlord";
import PersonaRenter from "./PersonaRenter";
import PersonaServicePro from "./PersonaServicePro";

let obj = {
  PersonaLandlord,
  PersonaRenter,
  PersonaServicePro
};

export default obj;
