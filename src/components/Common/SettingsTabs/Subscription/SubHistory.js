import React from "react";

import { Table } from "antd";

class BusinessExpenses extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }
  render() {
    const columns = [
      {
        title: "Name",
        dataIndex: "accountName",
        align: "center"
      },
      {
        title: "Account Type",
        dataIndex: "accountType",
        align: "center"
      },
      {
        title: "Category",
        dataIndex: "category",
        align: "center"
      },
      // {
      //   title: "Action",
      //   align: "center",
      //   render: text => (
      //     <>
      //       {text.isCreatedByAdmin ? (
      //         <i class="fas fa-lock"></i>
      //       ) : (
      //         <i
      //           onClick={() => this.props.openDrawer("update", text)}
      //           class="fas fa-edit"
      //         ></i>
      //       )}
      //     </>
      //   )
      // }
    ];
    return (
      <>
        <Table
          ref={this.props.testRef}
          locale={{ emptyText: "No Data!" }}
          columns={columns}
          dataSource={[]}
          bordered
        />
      </>
    );
  }
}

export default BusinessExpenses;
