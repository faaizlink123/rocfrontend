import React, { useState, useEffect } from "react";
import _ from "lodash";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import SubHistory from "./SubHistory";
import Switch from "react-switch";
import { Modal, message } from "antd";

import "./style.scss";
const { confirm } = Modal;

const SubscriptionSetting = props => {
  const [yearlyPlans, setYearlyPlans] = useState(props.yearlyPlans);
  const [monthlyPlans, setMonthlyPlans] = useState(props.monthlyPlans);

  let monthlyPremium = _.filter(monthlyPlans, {
    displayName: "Premium Monthly"
  });
  let monthlyStandard = _.filter(monthlyPlans, {
    displayName: "Standard Monthly"
  });

  let yearlyPremium = _.filter(yearlyPlans, { displayName: "Premium Yearly" });
  let yearlyStandard = _.filter(yearlyPlans, {
    displayName: "Standard Yearly"
  });

  const [viewPlan, setViewPlan] = useState("monthly");

  useEffect(() => {
    setYearlyPlans(props.yearlyPlans);
    setMonthlyPlans(props.monthlyPlans);
  }, [props.monthlyPlans, props.yearlyPlans]);

  let selectedPlan = _.get(
    props.contextData,
    "userData.authentication.data.selectedPlan"
  );

  console.log("selectedPlanselectedPlanselectedPlanselectedPlan", selectedPlan);

  const showConfirm = plan => {
    confirm({
      title: "Are you sure you want to proceed with buying this plan?",
      content: (
        <p>
          You have selected the{" "}
          <b>
            {plan[0].displayName}, which costs {`£${plan[0].amount}`}
          </b>
          , do you want to proceed?
        </p>
      ),
      onOk() {
        return new Promise(async (resolve, reject) => {
          props.client
            .mutate({
              mutation: props.AccountQueries.buySubscription,
              variables: { planId: plan[0].planId }
            })
            .then(res => {
              if (_.get(res, "data.createUserSubscription.success")) {
                message.success("Plan Selected!");
                resolve();
              } else {
                message.error(
                  `There was an error selecting the plan - ${_.get(
                    res,
                    "data.createUserSubscription.message"
                  )}`
                );
                resolve();
              }
            });
          // setTimeout(Math.random() > 0.5 ? resolve : reject, 5000);
        }).catch(() => console.log("Oops errors!"));
      },
      onCancel() {}
    });
  };

  return (
    <>
      <Tabs className="subs__tabs">
        <TabList>
          <Tab>Subscription</Tab>
          <Tab>Billing</Tab>
        </TabList>

        <TabPanel>
          <div className="subscribe_landlord">
            <div className="col-md-12 text-center">
              <div className="monthly__annually--toggle">
                <span className="toggle__btn--space">Monthly</span>
                <Switch
                  onChange={() =>
                    setViewPlan(viewPlan === "monthly" ? "yearly" : "monthly")
                  }
                  checked={viewPlan === "monthly" ? false : true}
                />
                <span className="toggle__btn--space">Annually</span>
              </div>
            </div>

            {/* Monthly Plan */}
            {viewPlan === "monthly" && (
              <div className="">
                <div className="row">
                  <div className="col-xl-4">
                    <div className="main_wrapper">
                      <div className="header">
                        <div className="plan-name">STANDARD</div>
                        <div className="plan-price">
                          {_.get(monthlyStandard, "[0].amount")}
                          <sup className="sup__tag">£</sup>
                        </div>
                        <div className="plan-frequency">per month / unit</div>
                      </div>
                      <div className="menu">
                        <ul>
                          <li>
                            <i className="fa fa-check"></i>{" "}
                            <strong>Include all features of Basic plan</strong>
                          </li>
                          <li className="text-center">
                            <i className="fa fa-plus"></i>
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Market property on
                            partner sites{" "}
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Send Rental
                            Agreement for eSign
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Storage 5 GB
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Collect rent via
                            Direct Debit
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Deposit protection
                          </li>
                        </ul>
                        <div className="col-12 text-center my-4">
                          <a
                            href
                            onClick={() => showConfirm(monthlyStandard)}
                            className="btn btn__plan"
                          >
                            Get Plan
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-4">
                    <div className="main_wrapperTwo">
                      <div className="head_wrapper">
                        {selectedPlan.displayName === "Free" && (
                          <div className="current">
                            <img
                              src={"/assets/images/current-plan.png"}
                              alt="img"
                            />
                          </div>
                        )}
                        <div className="header">
                          <div className="plan-name">BASIC</div>
                          <div className="plan-price">
                            0<sup className="sup__tag">£</sup>
                          </div>
                          <div className="plan-frequency">per month </div>
                        </div>
                      </div>
                      <div className="menuTwo">
                        <ul>
                          <li>
                            <i className="fa fa-check"></i>List &amp; manage
                            Rentals up to 4 units
                          </li>
                          <li>
                            <i className="fa fa-check"></i>Viewing and enquiry
                            organiser
                          </li>
                          <li>
                            <i className="fa fa-check"></i>Online Offer &amp;
                            applications
                          </li>
                          <li className="text-center">
                            <i className="fa fa-plus"></i>
                          </li>
                          <li>Bolt-on zyPass™ Landlord screening </li>
                          <li>
                            <i className="fa fa-plus"></i>Bolt-on zyPass™ Tenant
                            screening{" "}
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Draft Rental
                            Agreement
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Storage up to 1 GB
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Automated rental
                            demand invoice
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Raise maintenance
                            tasks &amp; pay online
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Manage accounting
                            &amp; finance.
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Generate Tax return
                            report
                          </li>
                        </ul>
                        <div className="col-12 text-center my-4">
                          <a href className="btn btn-grey btn__plan">
                            Start Free
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-4">
                    <div className="main_wrapperThree">
                      <div className="header">
                        <div className="plan-name">PREMIUM</div>
                        <div className="plan-price">
                          {_.get(monthlyPremium, "[0].amount")}
                          <sup className="sup__tag">£</sup>
                        </div>
                        <div className="plan-frequency">per month / unit</div>
                      </div>
                      <div className="menuThree">
                        <ul>
                          <li>
                            <i className="fa fa-check"></i>{" "}
                            <strong>
                              Include all features of Standard plan
                            </strong>
                          </li>
                          <li className="text-center">
                            <i className="fa fa-plus"></i>
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Expert Accountant to
                            prepare & submit Tax return
                          </li>
                          <li>
                            <i className="fa fa-check"></i> zyPass™ Landlord
                            screening and Tenant Referencing
                          </li>
                        </ul>
                        <div className="col-12 text-center my-4">
                          <a
                            href
                            onClick={() => showConfirm(monthlyPremium)}
                            className="btn btn__plan"
                          >
                            Get Plan
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}

            {/* Yearly Plan */}
            {viewPlan === "yearly" && (
              <div className="">
                <div className="row">
                  <div className="col-xl-4">
                    <div className="main_wrapper">
                      <div className="header">
                        <div className="plan-name">STANDARD</div>
                        <div className="plan-price">
                          {_.get(yearlyStandard, "[0].amount")}
                          <sup className="sup__tag">£</sup>
                        </div>
                        <div className="plan-frequency">per year / unit</div>
                      </div>
                      <div className="menu">
                        <ul>
                          <li>
                            <i className="fa fa-check"></i>{" "}
                            <strong>Include all features of Basic plan</strong>
                          </li>
                          <li className="text-center">
                            <i className="fa fa-plus"></i>
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Market property on
                            partner sites{" "}
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Send Rental
                            Agreement for eSign
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Storage 5 GB
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Collect rent via
                            Direct Debit
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Deposit protection
                          </li>
                        </ul>
                        <div className="col-12 text-center my-4">
                          <a
                            href
                            onClick={() => showConfirm(yearlyStandard)}
                            className="btn btn__plan"
                          >
                            Get Plan
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-4">
                    <div className="main_wrapperTwo">
                      <div className="head_wrapper">
                        <div className="current">
                          {selectedPlan.displayName === "Free" && (
                            <div className="current">
                              <img
                                src={"/assets/images/current-plan.png"}
                                alt="img"
                              />
                            </div>
                          )}
                        </div>
                        <div className="header">
                          <div className="plan-name">BASIC</div>
                          <div className="plan-price">
                            0<sup className="sup__tag">£</sup>
                          </div>
                          <div className="plan-frequency">per year </div>
                        </div>
                      </div>
                      <div className="menuTwo">
                        <ul>
                          <li>
                            <i className="fa fa-check"></i>List &amp; manage
                            Rentals up to 4 units
                          </li>
                          <li>
                            <i className="fa fa-check"></i>Viewing and enquiry
                            organiser
                          </li>
                          <li>
                            <i className="fa fa-check"></i>Online Offer &amp;
                            applications
                          </li>
                          <li className="text-center">
                            <i className="fa fa-plus"></i>
                          </li>
                          <li>Bolt-on zyPass™ Landlord screening </li>
                          <li>
                            <i className="fa fa-plus"></i>Bolt-on zyPass™ Tenant
                            screening{" "}
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Draft Rental
                            Agreement
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Storage up to 1 GB
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Automated rental
                            demand invoice
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Raise maintenance
                            tasks &amp; pay online
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Manage accounting
                            &amp; finance.
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Generate Tax return
                            report
                          </li>
                        </ul>
                        <div className="col-12 text-center my-4">
                          <a href className="btn btn-grey btn__plan">
                            Start Free
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-4">
                    <div className="main_wrapperThree">
                      <div className="header">
                        <div className="plan-name">PREMIUM</div>
                        <div className="plan-price">
                          {_.get(yearlyPremium, "[0].amount")}
                          <sup className="sup__tag">£</sup>
                        </div>
                        <div className="plan-frequency">per year / unit</div>
                      </div>
                      <div className="menuThree">
                        <ul>
                          <li>
                            <i className="fa fa-check"></i>{" "}
                            <strong>
                              Include all features of Standard plan
                            </strong>
                          </li>
                          <li className="text-center">
                            <i className="fa fa-plus"></i>
                          </li>
                          <li>
                            <i className="fa fa-check"></i> Expert Accountant to
                            prepare & submit Tax return
                          </li>
                          <li>
                            <i className="fa fa-check"></i> zyPass™ Landlord
                            screening and Tenant Referencing
                          </li>
                        </ul>
                        <div className="col-12 text-center my-4">
                          <a
                            href
                            onClick={() => showConfirm(yearlyPremium)}
                            className="btn btn__plan"
                          >
                            Get Plan
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </TabPanel>
        <TabPanel>
          <SubHistory />
        </TabPanel>
      </Tabs>
    </>
  );
};

export default SubscriptionSetting;
