import React, { useEffect, useState } from "react";
import _ from "lodash";
import "./style.scss";

const NotificationSettings = props => {
  props.history.listen((location, action) => {
    if (props.updateNotifications) {
      props.setNotificationPreferences("save");
    }
  });

  let [preferences, setPreferences] = useState(props.notificationPreferences);

  let [appointment, setAppointment] = useState(
    _.find(preferences, "appointment").appointment
  );
  let [screeningReport, setscreeningReport] = useState(
    _.find(preferences, "screeningReport").screeningReport
  );
  let [newsLetter, setnewsLetter] = useState(
    _.find(preferences, "newsLetter").newsLetter
  );
  let [taskReminder, settaskReminder] = useState(
    _.find(preferences, "taskReminder").taskReminder
  );

  useEffect(() => {
    let preferences = props.notificationPreferences;
    setAppointment(_.find(preferences, "appointment").appointment);
    setscreeningReport(_.find(preferences, "screeningReport").screeningReport);
    setnewsLetter(_.find(preferences, "newsLetter").newsLetter);
    settaskReminder(_.find(preferences, "taskReminder").taskReminder);
    setPreferences(props.notificationPreferences);
  }, [props]);

  console.log("ADASDASDASDASDASDASDASDASD", appointment);

  return (
    <div className="notify">
      <div className="">
        <div className="row">
          <div className="col-xl-6">
            <div className="notify_settings d-flex">
              <img src={"/assets/images/icon-notification1.png"} alt="img" />
              <div className="details">
                <h4>New Enquiry, Application /Order &amp; Appointment</h4>
                <div className="custom-control custom-switch custom-control-inline">
                  <input
                    type="checkbox"
                    checked={appointment["email"]}
                    onChange={event =>
                      props.setNotificationPreferences("set", {
                        event,
                        name: "email",
                        type: "appointment"
                      })
                    }
                    className="custom-control-input"
                    id="switch1"
                  />
                  <label className="custom-control-label" for="switch1">
                    {" "}
                    Email
                  </label>
                </div>
                <div className="custom-control custom-switch custom-control-inline">
                  <input
                    type="checkbox"
                    checked={appointment["sms"]}
                    onChange={event =>
                      props.setNotificationPreferences("set", {
                        event,
                        name: "sms",
                        type: "appointment"
                      })
                    }
                    className="custom-control-input"
                    id="switch2"
                  />
                  <label className="custom-control-label" for="switch2">
                    {" "}
                    SMS
                  </label>
                </div>
                <div className="custom-control custom-switch custom-control-inline">
                  <input
                    type="checkbox"
                    checked={appointment["whatsApp"]}
                    onChange={event =>
                      props.setNotificationPreferences("set", {
                        event,
                        name: "whatsApp",
                        type: "appointment"
                      })
                    }
                    className="custom-control-input"
                    id="switch3"
                  />
                  <label className="custom-control-label" for="switch3">
                    {" "}
                    WhatsApp
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-6">
            <div className="notify_settings d-flex">
              <img src={"/assets/images/icon-notification2.png"} alt="img" />
              <div className="details">
                <h4>Background check & screening report</h4>
                <div className="custom-control custom-switch custom-control-inline">
                  <input
                    type="checkbox"
                    checked={screeningReport["email"]}
                    onChange={event =>
                      props.setNotificationPreferences("set", {
                        event,
                        name: "email",
                        type: "screeningReport"
                      })
                    }
                    className="custom-control-input"
                    id="switch4"
                  />
                  <label className="custom-control-label" for="switch4">
                    {" "}
                    Email
                  </label>
                </div>
                <div className="custom-control custom-switch custom-control-inline">
                  <input
                    type="checkbox"
                    checked={screeningReport["sms"]}
                    onChange={event =>
                      props.setNotificationPreferences("set", {
                        event,
                        name: "sms",
                        type: "screeningReport"
                      })
                    }
                    className="custom-control-input"
                    id="switch5"
                  />
                  <label className="custom-control-label" for="switch5">
                    {" "}
                    SMS
                  </label>
                </div>
                <div className="custom-control custom-switch custom-control-inline">
                  <input
                    name="whatsApp"
                    type="checkbox"
                    checked={screeningReport["whatsApp"]}
                    onChange={event =>
                      props.setNotificationPreferences("set", {
                        event,
                        name: "whatsApp",
                        type: "screeningReport"
                      })
                    }
                    className="custom-control-input"
                    id="switch6"
                  />
                  <label className="custom-control-label" for="switch6">
                    {" "}
                    WhatsApp
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-6">
            <div className="notify_settings d-flex">
              <img src={"/assets/images/icon-notification3.png"} alt="img" />
              <div className="details">
                <h4>New offers and Newsletter</h4>
                <div className="custom-control custom-switch custom-control-inline">
                  <input
                    type="checkbox"
                    checked={newsLetter["email"]}
                    onChange={event =>
                      props.setNotificationPreferences("set", {
                        event,
                        name: "email",
                        type: "newsLetter"
                      })
                    }
                    className="custom-control-input"
                    id="switch7"
                  />
                  <label className="custom-control-label" for="switch7">
                    {" "}
                    Email
                  </label>
                </div>
                <div className="custom-control custom-switch custom-control-inline">
                  <input
                    type="checkbox"
                    checked={newsLetter["sms"]}
                    onChange={event =>
                      props.setNotificationPreferences("set", {
                        event,
                        name: "sms",
                        type: "newsLetter"
                      })
                    }
                    className="custom-control-input"
                    id="switch8"
                  />
                  <label className="custom-control-label" for="switch8">
                    {" "}
                    SMS
                  </label>
                </div>
                <div className="custom-control custom-switch custom-control-inline">
                  <input
                    type="checkbox"
                    checked={newsLetter["whatsApp"]}
                    onChange={event =>
                      props.setNotificationPreferences("set", {
                        event,
                        name: "whatsApp",
                        type: "newsLetter"
                      })
                    }
                    className="custom-control-input"
                    id="switch9"
                  />
                  <label className="custom-control-label" for="switch9">
                    {" "}
                    WhatsApp
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-6">
            <div className="notify_settings d-flex">
              <img src={"/assets/images/icon-notification4.png"} alt="img" />
              <div className="details">
                <h4>Task reminder, Appointments & payments</h4>
                <div className="custom-control custom-switch custom-control-inline">
                  <input
                    type="checkbox"
                    checked={taskReminder["email"]}
                    onChange={event =>
                      props.setNotificationPreferences("set", {
                        event,
                        name: "email",
                        type: "taskReminder"
                      })
                    }
                    className="custom-control-input"
                    id="switch10"
                  />
                  <label className="custom-control-label" for="switch10">
                    {" "}
                    Email
                  </label>
                </div>
                <div className="custom-control custom-switch custom-control-inline">
                  <input
                    type="checkbox"
                    checked={taskReminder["sms"]}
                    onChange={event =>
                      props.setNotificationPreferences("set", {
                        event,
                        name: "sms",
                        type: "taskReminder"
                      })
                    }
                    className="custom-control-input"
                    id="switch11"
                  />
                  <label className="custom-control-label" for="switch11">
                    {" "}
                    SMS
                  </label>
                </div>
                <div className="custom-control custom-switch custom-control-inline">
                  <input
                    type="checkbox"
                    checked={taskReminder["whatsApp"]}
                    onChange={event =>
                      props.setNotificationPreferences("set", {
                        event,
                        name: "whatsApp",
                        type: "taskReminder"
                      })
                    }
                    className="custom-control-input"
                    id="switch12"
                  />
                  <label className="custom-control-label" for="switch12">
                    {" "}
                    WhatsApp
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NotificationSettings;
