import React from "react";
import _ from "lodash";
import moment from "moment";
import "./Card.scss";

const Card = props => {
  let userData = props.currentUserData;
  let gender =
    _.get(userData, "gender") &&
    _.get(userData, "gender")
      .charAt(0)
      .toUpperCase();

  let ProfileCompletenessData = props.ProfileCompletenessData;
  let profileChecks = ProfileCompletenessData.profile;
  let selfDeclaration = ProfileCompletenessData.selfDeclaration;

  console.log("-------------------------------", ProfileCompletenessData);

  return (
    <>
      <div className="active-connect-tab">
        <div className="top_panel">
          <div className="card-body text-center">
            <div className="profile_avator">
              <img src={userData.avatar} alt="img" />
              <div className="status">
                <i className="fas fa-check"></i>
              </div>
            </div>
            <div className="name">
              {userData.socialLinks && (
                <>
                  <a href>
                    <i className="fab fa-facebook-f"></i>
                  </a>
                  <a href>
                    <i className="fab fa-twitter"></i>
                  </a>
                  <a href>
                    <i className="fab fa-linkedin-in"></i>
                  </a>
                </>
              )}
            </div>
            <div className="address"></div>
            {userData.firstName && (
              <div className="address">
                {userData.firstName}, {gender}{" "}
                {moment().diff(userData.dob, "year")}, {userData.nationality}
              </div>
            )}

            <div className="reference mb-4">
              <sup>
                <i className="fa fa-quote-left"></i>
              </sup>{" "}
              References <span>5</span>
            </div>
            <div onClick={() => props.showProfileCompletenessModal()}>
              <div className="progress mb-3">
                <div
                  className="progress-bar bg-success"
                  style={{
                    width: `${ProfileCompletenessData.profileCompleteness}%`
                  }}
                >
                  {" "}
                  {ProfileCompletenessData.profileCompleteness}%{" "}
                </div>
              </div>
            </div>
          </div>
        </div>
        {!_.isEmpty(ProfileCompletenessData) && (
          <div className="bottom_panel">
            <div className="short_card custom">
              <h4 className="text-center text-danger mb-4">Not Verified </h4>
              <ul className="list-two-column">
                <li
                  className={
                    profileChecks && profileChecks.identity && "verified"
                  }
                >
                  <span className={"mdi mdi-account-card-details"}></span>{" "}
                  Identity
                </li>
                <li
                  className={
                    profileChecks && profileChecks.creditCheck && "verified"
                  }
                >
                  <span className={"mdi mdi-credit-card-plus"}></span>
                  Credit Check
                </li>
                <li
                  className={profileChecks && profileChecks.phone && "verified"}
                >
                  <span className={"mdi mdi-phone-in-talk"}></span> Phone
                </li>
                <li
                  className={
                    profileChecks && profileChecks.income && "verified"
                  }
                >
                  <span className={"mdi mdi-coin"}></span> Income
                </li>
                <li
                  className={profileChecks && profileChecks.bank && "verified"}
                >
                  <span className={"mdi mdi-bank"}></span> Bank
                </li>
                <li
                  className={
                    profileChecks && profileChecks.rental && "verified"
                  }
                >
                  <span className={"mdi mdi-cash-100"}></span> Past Rental
                </li>
                <li
                  className={
                    profileChecks && profileChecks.rightToRent && "verified"
                  }
                >
                  <span className={"mdi mdi-account-arrow-right"}></span> Right
                  to Rent
                </li>
                <li
                  className={
                    profileChecks && profileChecks.affordability && "verified"
                  }
                >
                  <span className={"mdi mdi-currency-gbp"}></span> Affordability
                </li>
              </ul>
            </div>
            <div className="short_card">
              <h4 className="text-center mb-4">Self Declaration</h4>
              <ul className="list-two-column">
                <li
                  className={
                    selfDeclaration && selfDeclaration.pets && "verified"
                  }
                >
                  <span className="mdi mdi-dog-side"></span> Pets
                </li>
                <li
                  className={
                    selfDeclaration && selfDeclaration.smoking && "verified"
                  }
                >
                  <span className="mdi mdi-smoking"></span>Smoking
                </li>
                <li
                  className={
                    selfDeclaration && selfDeclaration.vehicles && "verified"
                  }
                >
                  <span className="mdi mdi-car"></span> Vehicles
                </li>
                <li
                  className={
                    selfDeclaration && selfDeclaration.disable && "verified"
                  }
                >
                  <span className="mdi mdi-wheelchair-accessibility"></span>{" "}
                  Disable
                </li>
                <li
                  className={
                    selfDeclaration &&
                    selfDeclaration.incomeSupport &&
                    "verified"
                  }
                >
                  <span className="mdi mdi-account-supervisor"></span> Income
                  Support
                </li>
              </ul>
            </div>
            <div className="short_foot">
              Last screen date 09-Mar-2019, Powered by{" "}
              <img src={"/assets/images/zypass.png"} alt="img" />
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default Card;
