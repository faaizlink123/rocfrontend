import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { message } from "antd";
import { Formik, Form, Field, ErrorMessage } from "formik";
import ForgotPassQueries from "../../../config/queries/login";
import _ from "lodash";
import { withApollo } from "react-apollo";

import "./register.scss";

const ResetPasswordInvite = props => {
  const signUp = async formData => {
    const checkUserQuery = await props.client.mutate({
      mutation: ForgotPassQueries.forgotPassword,
      variables: formData
    });

    if (_.get(checkUserQuery, "data.forgotPassword.success")) {
      message.success("We have sent an email to your registered email!");
    } else {
      message.error(_.get(checkUserQuery, "data.forgotPassword.message"));
    }
  };

  return (
    <div className="login__bg">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="login-wrapper">
              <Formik
                onSubmit={(values, { setSubmitting }) => {
                  setSubmitting(true);
                  signUp(values);
                }}
              >
                {({ isSubmitting, errors }) => (
                  <>
                    <img src={"/roc.png"} alt="" className="login-logo" />
                    <div className="login-box">
                      <Form>
                        <h4>Forgot your Password?</h4>
                        <div className="form-group">
                          <i className="fa fa-envelope i__dark"></i>
                          <Field
                            placeholder="Enter your registered email address"
                            type="email"
                            name="email"
                            required
                            className={
                              errors.email
                                ? "full__input error__field_show"
                                : "full__input"
                            }
                          />
                        </div>

                        <div className="form-group mb-0">
                          <button
                            type="submit"
                            className="btn btn-primary  btn-block"
                          >
                            Send me instruction
                          </button>
                        </div>
                      </Form>
                    </div>
                  </>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withApollo(withRouter(ResetPasswordInvite));
