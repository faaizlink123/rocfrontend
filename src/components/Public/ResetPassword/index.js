import React from "react";
import { withRouter } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";
import ForgotPassQueries from "../../../config/queries/login";
import showNotification from "../../../config/Notification";
import { useParams } from "react-router-dom";
import RegistrationFormSchema from "../../../config/FormSchemas/register";
import _ from "lodash";
import { withApollo } from "react-apollo";
import { useQuery } from "@apollo/react-hooks";
import PacmanLoader from "react-spinners/PropagateLoader";

import "./register.scss";

const Register = props => {
  let { token } = useParams();

  const { data } = useQuery(ForgotPassQueries.getUserInformation, {
    variables: { token }
  });

  const resetPassword = async data => {
    const resetPasswordQuery = await props.client.mutate({
      mutation: ForgotPassQueries.resetPassword,
      variables: { token, password: data.password }
    });

    if (_.get(resetPasswordQuery, "data.resetPassword.success")) {
      showNotification("success", "Password was reset successfully!", "");
      window.location.replace("/login");
    } else {
      showNotification(
        "error",
        "An error occured",
        _.get(resetPasswordQuery, "data.resetPassword.message")
      );
    }
  };

  if (_.get(data, "getUserInformation.message") === "Token expire.") {
    props.history.push("/login");
    return <></>;
  }

  if (!_.get(data, "getUserInformation.success")) {
    return (
      <>
        <PacmanLoader
          sizeUnit={"px"}
          size={18}
          color={"#123abc"}
          loading={true}
        />
      </>
    );
  }

  if (_.get(data, "getUserInformation.success")) {
    let userData = _.get(data, "getUserInformation.data");
    return (
      <div className="login__bg">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="login-wrapper">
                <Formik
                  initialValues={{ email: userData ? userData.email : "" }}
                  validationSchema={RegistrationFormSchema}
                  onSubmit={(values, { setSubmitting }) => {
                    setSubmitting(true);
                    resetPassword(values);
                  }}
                >
                  {({ isSubmitting, errors }) => (
                    <>
                      <img src={"/roc.png"} alt="" className="login-logo" />
                      <div className="login-box">
                        <Form>
                          <h4>Welcome Back, {userData && userData.name}!</h4>
                          <div className="form-group">
                            <i className="fa fa-envelope i__dark"></i>
                            <Field
                              placeholder="Please enter your email!"
                              type="email"
                              name="email"
                              className={
                                errors.email
                                  ? "full__input error__field_show"
                                  : "full__input"
                              }
                              disabled
                            />
                          </div>
                          <div className="form-group">
                            <i className="fa fa-lock i__dark"></i>
                            <Field
                              placeholder="Enter your password"
                              type="password"
                              name="password"
                              className={
                                errors.password
                                  ? "full__input error__field_show"
                                  : "full__input"
                              }
                            />
                            <ErrorMessage
                              name="password"
                              component="div"
                              className="all__errors"
                            />
                          </div>
                          <div className="form-group">
                            <i className="fa fa-lock i__dark"></i>
                            <Field
                              placeholder="Confirm your password"
                              type="password"
                              name="comfirmPassword"
                              className={
                                errors.confirmPassword
                                  ? "full__input error__field_show"
                                  : "full__input"
                              }
                            />
                            <ErrorMessage
                              name="comfirmPassword"
                              component="div"
                              className="all__errors"
                            />
                          </div>

                          <div className="form-group mb-0">
                            <button
                              type="submit"
                              className="btn btn-primary  btn-block"
                            >
                              Reset Password
                            </button>
                          </div>
                        </Form>
                      </div>
                    </>
                  )}
                </Formik>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default withApollo(withRouter(Register));
