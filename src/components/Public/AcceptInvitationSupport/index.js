import React from "react";
import { withRouter } from "react-router-dom";
import UserRoleQuery from "../../../config/queries/userRole";
import { message } from "antd";
import { useParams } from "react-router-dom";
import _ from "lodash";
import { withApollo } from "react-apollo";
import { useQuery } from "@apollo/react-hooks";
import PacmanLoader from "react-spinners/PropagateLoader";

const Register = props => {
  let { token } = useParams();

  const { loading, data } = useQuery(UserRoleQuery.acceptInvitation, {
    variables: { token }
  });

  if (_.get(data, "acceptInvitation.message") === "Invitation token expire.") {
    message.error("Invitation Accepted! Please log in to continue");

    props.history.push("/login");
    return (
      <>
        <p>Loading...</p>
      </>
    );
  }

  if (!_.get(data, "acceptInvitation.success")) {
    return (
      <>
        <PacmanLoader
          sizeUnit={"px"}
          size={18}
          color={"#123abc"}
          loading={true}
        />
      </>
    );
  }

  if (_.get(data, "getUserInformation.success")) {
    message.success("Invitation Accepted! Please log in to continue");
    props.history.push("/login");
    return <></>;
  }
};

export default withApollo(withRouter(Register));
