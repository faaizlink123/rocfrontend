import React, { useState, useEffect, useRef } from "react";
import { withApollo } from "react-apollo";
import { withRouter } from "react-router-dom";
import { Button } from "antd";
import cookie from "react-cookies";

import _ from "lodash";

import Queries from "../../../config/queries/login";
import "./style.scss";

const LockScreen = props => {
  const formRef = useRef(null);

  const [loading, setLoader] = useState(false);

  const checkPassword = async e => {
    e.preventDefault();
    setLoader(true);
    const checkPasswordQuery = await props.client.query({
      query: Queries.checkPassword,
      variables: {
        password: e.target[0].value
      }
    });

    if (
      !_.isEmpty(checkPasswordQuery.data.checkPassword) &&
      _.get(checkPasswordQuery, "data.checkPassword.success")
    ) {
      setPwdCheckStatus("form-control");
      formRef.current.reset();
      localStorage.setItem("screenLocked", false);
      setLoader(false);
      setLockScreenState(false);
    } else {
      setLoader(false);
      setPwdCheckStatus("form-control password_check__error");
    }
  };

  const [signoutTime] = useState(process.env.REACT_APP_LOCKSCREEN_LOGOUT);
  const [warningTime] = useState(process.env.REACT_APP_LOCKSCREEN_TIMEOUT);

  let screenLocked = localStorage.getItem("screenLocked");

  const [isLocked, setLockScreenState] = useState(
    screenLocked ? (screenLocked === "true" ? true : false) : false
  );

  const [pwdCheckError, setPwdCheckStatus] = useState("form-control");

  let warnTimeout;
  let logoutTimeout;

  const warn = () => {
    setLockScreenState(true);
    localStorage.setItem("screenLocked", true);
  };
  const logout = () => {
    cookie.remove("token", { path: "/" });

    window.location.reload();
  };

  const setTimeouts = () => {
    warnTimeout = setTimeout(warn, warningTime);
    logoutTimeout = setTimeout(logout, signoutTime);
  };

  const clearTimeouts = () => {
    if (warnTimeout) clearTimeout(warnTimeout);
    if (logoutTimeout) clearTimeout(logoutTimeout);
  };

  useEffect(() => {
    const events = [
      "load",
      "mousemove",
      "mousedown",
      "click",
      "scroll",
      "keypress"
    ];

    const resetTimeout = () => {
      clearTimeouts();
      setTimeouts();
    };

    for (let i in events) {
      window.addEventListener(events[i], resetTimeout);
    }

    setTimeouts();
    return () => {
      for (let i in events) {
        window.removeEventListener(events[i], resetTimeout);
        clearTimeouts();
      }
    };
  });

  return (
    <div
      className={
        isLocked
          ? "lock__unlock--screen"
          : "lock__unlock--screen unlock__screen"
      }
    >
      <div className="lock_bg">
        <div className="container">
          <div className="text-center">
            <div className="user_img mb-3">
              <img
                src={"/user-avatar.png"}
                className="rounded-circle"
                alt="img"
              />
            </div>
            <h4 className="mb-0">Hello!,</h4>
            <p className="mb-4">Your session is about to expire!</p>
            <form onSubmit={checkPassword} ref={formRef}>
              <div className="form-group">
                <input
                  type="password"
                  autoFocus
                  className={pwdCheckError}
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                  placeholder="Enter Password"
                />
                <small
                  id="emailHelp"
                  className="form-text text-muted mb-4 text-left"
                >
                  Enter Password to unlock your screen.
                </small>
                <Button
                  loading={loading}
                  color="primary"
                  type="submit"
                  className="btn btn-primary"
                >
                  Unlock Screen
                </Button>
                <small className="form-text text-muted">
                  <a
                    href
                    onClick={() => {
                      props.history.push("/");
                      localStorage.setItem("screenLocked", false);
                    }}
                  >
                    Take me to home page.
                  </a>
                </small>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(withApollo(LockScreen));
