import React from "react";
import { withRouter, Link } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import { LinkedIn } from "react-linkedin-login-oauth2";

const LoginFormEmail = props => {
  const handleFailure = failure => {
    console.log(failure);
  };

  const handleSuccess = success => {
    console.log(success);
  };

  return (
    <div className="login-wrapper">
      <img src={"/roc.png"} alt="" className="login-logo" />
      <div className="login-box">
        <Formik
          initialValues={{ email: "" }}
          validationSchema={props.FormValidationSchema.EmailCheckSchema}
          onSubmit={(values, { setSubmitting }) => {
            setSubmitting(true);
            props.onSubmit(values);
          }}
        >
          {({ isSubmitting, errors }) => (
            <Form>
              <h4>Login | Get Started</h4>
              <div className="form-group">
                {" "}
                <i className="fa fa-envelope i__dark"></i>
                <Field
                  placeholder="Please enter your email!"
                  type="email"
                  name="email"
                  className={
                    errors.email
                      ? "error__field_show full__input"
                      : "full__input"
                  }
                />
                <ErrorMessage
                  name="email"
                  component="div"
                  className="all__errors"
                />
                <div className="validation-message">
                  We will send a verification code to this email
                </div>
              </div>

              <div className="form-group">
                <button type="submit" className="btn btn-primary btn-block">
                  Login | Get Started
                </button>
              </div>
              <div className="or-seperator">
                <span>OR</span>
              </div>
              <div className="form-group">
                <FacebookLogin
                  appId={process.env.REACT_APP_FACEBOOK_APPID}
                  autoLoad={false}
                  fields="name,email,picture"
                  callback={props.responseFacebook}
                  render={renderProps => (
                    <div
                      onClick={renderProps.onClick}
                      className="btn btn-primary btn-facebook"
                    >
                      <i className="fab fa-facebook-f social__position"></i>
                      &nbsp; Login with Facebook
                    </div>
                  )}
                />
              </div>
              <div className="form-group">
                {" "}
                <LinkedIn
                  clientId={process.env.REACT_APP_LINKEDIN_ID}
                  onFailure={handleFailure}
                  onSuccess={handleSuccess}
                  redirectUri={`${window.location.origin}/linkedin`}
                  scope="w_member_social"
                  renderElement={({ onClick, disabled }) => (
                    <div
                      onClick={onClick}
                      disabled={disabled}
                      className="btn btn-primary btn-linkedin"
                    >
                      <i className="fab fa-linkedin-in social__position"></i>
                      &nbsp; Login with LinkedIn
                    </div>
                  )}
                >
                  Login with linkedin
                </LinkedIn>
              </div>
            </Form>
          )}
        </Formik>
        <p className="terms__text">
          By clicking login, you agree to our{" "}
          <a href="https://develop.rentoncloud.co/terms-of-use">Terms</a> &{" "}
          <a href="https://develop.rentoncloud.co/privacy-policy">
            Privacy Policy
          </a>
        </p>
      </div>
    </div>
  );
};

export default withRouter(LoginFormEmail);
