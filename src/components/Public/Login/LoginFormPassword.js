import React, { useState } from "react";
import { withRouter, Link } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";

const LoginFormPassword = props => {
  let userData = props.ifEmailExists;
  console.log(userData);

  return (
    <div className="login-wrapper">
      <img src={"/roc.png"} alt="" className="login-logo" />
      <div className="login-box">
        <Formik
          initialValues={userData}
          validationSchema={props.FormValidationSchema.PasswordSchema}
          onSubmit={(values, { setSubmitting }) => {
            setSubmitting(true);
            let obj = { email: values.email, password: values.password };
            props.onSubmit(obj);
          }}
        >
          {({ isSubmitting, errors }) => (
            <Form>
              <h4>Welcome back {userData && userData.firstName}!</h4>
              <div className="form-group">
                <i className="fa fa-envelope i__dark"></i>
                <Field
                  placeholder="Please enter your email!"
                  type="email"
                  name="email"
                  className="full__input"
                  disabled
                />
              </div>
              <div className="form-group">
                <i className="fa fa-lock i__dark"></i>
                <Field
                  placeholder="Enter your password"
                  type="password"
                  name="password"
                  className={
                    errors.password
                      ? "full__input error__field_show"
                      : "full__input"
                  }
                />
                <ErrorMessage
                  name="password"
                  component="div"
                  className="all__errors"
                />
              </div>

              <div className="form-group mb-30">
                <div className="row">
                  <div className="col-7 text-left">
                    <label className="pull-left checkbox-inline">
                      <input type="checkbox" /> Remember me
                    </label>
                  </div>

                  <div className="col-5 text-right">
                    <a
                      href
                      onClick={() => props.history.push("/forgotpassword")}
                    >
                      Forgot Password?
                    </a>
                  </div>
                </div>
              </div>

              <div className="form-group">
                <button type="submit" className="btn btn-primary  btn-block">
                  Login
                </button>
              </div>
            </Form>
          )}
        </Formik>
        <p className="terms__text">
          By clicking login, you agree to our{" "}
          <a href="https://develop.rentoncloud.co/terms-of-use">Terms</a> &{" "}
          <a href="https://develop.rentoncloud.co/privacy-policy">
            Privacy Policy
          </a>
        </p>
      </div>
    </div>
  );
};

export default withRouter(LoginFormPassword);
