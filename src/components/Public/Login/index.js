import React from "react";
import LoginFormEmail from "./LoginFormEmail";
import LoginFormPassword from "./LoginFormPassword";
import OtpVerification from "./ValidateOtp";
import LoginQuery from "../../../config/queries/login";
import { withApollo } from "react-apollo";
import { withRouter } from "react-router-dom";
import FormValidationSchema from "../../../config/FormSchemas/login";
import cookie from "react-cookies";
import { notification } from "antd";

import _ from "lodash";

import "./login.scss";

class LoginMain extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loginStep: 1,
      ifEmailExists: {
        email: ""
      }
    };
  }

  componentDidMount() {
    this.checkUser();
  }

  responseFacebook = async res => {
    // const socialLoginQueryResponse = await this.props.client.mutate({
    //   mutation: LoginQuery.socialLogin,
    //   variables: {
    //     firstName: res.name,
    //     lastName: "",
    //     email: res.email,
    //     socialId: String(res.userId)
    //   }
    // });

    this.props.startLoading(true);
    const queryResponse = await this.props.client.query({
      query: LoginQuery.checkEmail,
      variables: { email: res.email }
    });

    if (
      !_.isEmpty(queryResponse.data.checkEmail) &&
      _.get(queryResponse, "data.checkEmail.success")
    ) {
      let ifEmailExists = queryResponse.data.checkEmail;
      let loginStep = ifEmailExists.isMFA ? 2 : 3;

      this.setState({ loginStep, ifEmailExists });
      this.props.endLoading(false);
    } else {
      this.props.endLoading(false);

      this.props.history.push("/register", { ...res });
    }
  };

  onSubmitEmail = async formData => {
    this.props.startLoading(true);
    const queryResponse = await this.props.client.query({
      query: LoginQuery.checkEmail,
      variables: { email: formData.email }
    });

    if (
      !_.isEmpty(queryResponse.data.checkEmail) &&
      _.get(queryResponse, "data.checkEmail.success")
    ) {
      let ifEmailExists = queryResponse.data.checkEmail;
      let loginStep = ifEmailExists.isMFA ? 2 : 3;

      this.props.endLoading();

      this.setState({ loginStep, ifEmailExists });
    } else {
      this.props.endLoading();

      this.props.history.push("/register", { email: formData.email });
    }
  };

  OTPFilled = async OTP => {
    this.props.startLoading(true);

    const checkOtpQueryRes = await this.props.client.query({
      query: LoginQuery.checkOtp,
      variables: { email: this.state.ifEmailExists.email, otp: OTP }
    });

    if (
      !_.isEmpty(checkOtpQueryRes.data.checkOTP) &&
      _.get(checkOtpQueryRes, "data.checkOTP.success")
    ) {
      this.setState({ loginStep: 3 });
      this.props.endLoading(false);
    } else {
      this.props.endLoading(false);
      notification.error({ message: "OTP is incorrect!" });
    }
  };

  onSubmitLogin = async formDataLogin => {
    this.props.startLoading(true);
    const loginQueryResponse = await this.props.client.query({
      query: LoginQuery.loginUser,
      variables: { ...formDataLogin }
    });

    if (
      !_.isEmpty(loginQueryResponse.data.login) &&
      _.get(loginQueryResponse, "data.login.success")
    ) {
      let loginData = loginQueryResponse.data.login;

      cookie.save("token", loginData.token, { path: "/" });
      window.location.reload();
    } else {
      notification.error({ message: "Wrong Password! Please try again..." });
    }
  };

  checkUser = async () => {
    const checkUserQuery = await this.props.client.query({
      query: LoginQuery.checkAuth,
      context: {
        headers: {
          token: cookie.load("token")
        }
      }
    });

    if (
      !_.isEmpty(checkUserQuery.data.authentication) &&
      _.get(checkUserQuery, "data.authentication.success")
    ) {
      let userData = _.get(checkUserQuery, "data.authentication.data");

      this.props.history.push(`/${userData.role}`);
    }
  };

  render() {
    const { loginStep, ifEmailExists, OtpCheckStatus } = this.state;

    return (
      <div className="login__bg">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              {(loginStep === 1 && (
                <LoginFormEmail
                  FormValidationSchema={FormValidationSchema}
                  onSubmit={this.onSubmitEmail}
                  responseFacebook={this.responseFacebook}
                />
              )) ||
                (loginStep === 2 && (
                  <OtpVerification
                    ifEmailExists={ifEmailExists}
                    OTPFilled={this.OTPFilled}
                    OtpCheckStatus={OtpCheckStatus}
                    goBack={() => this.setState({ loginStep: 1 })}
                  />
                )) ||
                (loginStep === 3 && (
                  <LoginFormPassword
                    FormValidationSchema={FormValidationSchema}
                    onSubmit={this.onSubmitLogin}
                    ifEmailExists={ifEmailExists}
                    {...this.props}
                  />
                ))}
            </div>
          </div>
          {/* <button onClick={() => this.props.history.push("/address")}>
            {" "}
            go
          </button> */}
        </div>
      </div>
    );
  }
}

export default withRouter(withApollo(LoginMain));
