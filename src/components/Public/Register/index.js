import React, { useState, useEffect } from "react";
import { withRouter, Link } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";
import SignUpUser from "../../../config/queries/register";
import showNotification from "../../../config/Notification";
import ValidateEmail from "../../../config/ValidateEmail";
import RegistrationFormSchema from "../../../config/FormSchemas/register";
import _ from "lodash";
import { withApollo } from "react-apollo";

import "./register.scss";

const Register = props => {
  const [selectedCategory, setSelectedCategory] = useState("landlord");

  const setCategory = category => {
    setSelectedCategory(category);
  };

  const signUp = async formData => {
    let isValidEmail = await ValidateEmail(formData);
    if (!isValidEmail) {
      showNotification(
        "error",
        "Invalid Email",
        "The email you entered is invalid."
      );
    }

    if (isValidEmail) {
      const queryResponse = await props.client.mutate({
        mutation: SignUpUser,
        variables: { ...formData, role: selectedCategory }
      });
      if (
        !_.isEmpty(queryResponse.data.registration) &&
        _.get(queryResponse, "data.registration.success")
      ) {
        showNotification(
          "success",
          "Welcome!",
          "You have been registered successfully!"
        );

        props.history.push("/login");
      } else {
        showNotification(
          "error",
          "An error occured",
          _.get(queryResponse, "data.registration.message")
        );
      }
    }
  };

  let [email, setEmail] = useState(_.get(props.location.state, "email"));

  useEffect(() => {
    setEmail(_.get(props.location.state, "email"));
  }, [props]);

  return (
    <div className="login__bg">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="login-wrapper">
              <Formik
                initialValues={{ email: email ? email : "", password: "" }}
                validationSchema={RegistrationFormSchema}
                onSubmit={(values, { setSubmitting }) => {
                  setSubmitting(true);
                  signUp(values);
                }}
              >
                {({ isSubmitting, errors }) => (
                  <>
                    <img src={"/roc.png"} alt="" className="login-logo" />
                    <div className="login-box">
                      <Form>
                        <h4>Select Role & Get Started</h4>
                        <div className="user-type d-block clearfix mb-4">
                          <ul>
                            <li
                              className={
                                selectedCategory === "landlord" && "active"
                              }
                              onClick={() => setCategory("landlord")}
                            >
                              <span>
                                <i className="mdi mdi-home-account"></i>
                                <p>I am a Landlord</p>
                              </span>
                            </li>
                            <li
                              className={
                                selectedCategory === "renter" && "active"
                              }
                              onClick={() => setCategory("renter")}
                            >
                              <span>
                                <i className="mdi mdi-account-key"></i>
                                <p>I am a renter</p>
                              </span>
                            </li>
                            <li
                              className={
                                selectedCategory === "servicepro" && "active"
                              }
                              onClick={() => setCategory("servicepro")}
                            >
                              <span>
                                <i className="mdi mdi-account-clock"></i>
                                <p>I am a ServicePro</p>
                              </span>
                            </li>
                          </ul>
                        </div>
                        <div className="form-group">
                          <i className="fa fa-envelope i__dark"></i>
                          <Field
                            placeholder="Please enter your email!"
                            type="email"
                            name="email"
                            className={
                              errors.email
                                ? "full__input error__field_show"
                                : "full__input"
                            }
                            disabled={email && true}
                          />
                          <ErrorMessage
                            name="email"
                            component="div"
                            className="all__errors"
                          />
                        </div>
                        <div className="form-group">
                          <i className="fa fa-lock i__dark"></i>
                          <Field
                            placeholder="Enter your password"
                            type="password"
                            name="password"
                            className={
                              errors.password
                                ? "full__input error__field_show"
                                : "full__input"
                            }
                          />
                          <ErrorMessage
                            name="password"
                            component="div"
                            className="all__errors"
                          />
                        </div>
                        <div className="form-group">
                          <i className="fa fa-lock i__dark"></i>
                          <Field
                            placeholder="Confirm your password"
                            type="password"
                            name="comfirmPassword"
                            className={
                              errors.confirmPassword
                                ? "full__input error__field_show"
                                : "full__input"
                            }
                          />
                          <ErrorMessage
                            name="comfirmPassword"
                            component="div"
                            className="all__errors"
                          />
                        </div>

                        <div className="form-group small">
                          <label className="checkbox-inline terms__text">
                            <input required type="checkbox" /> I have read and
                            agree with{" "}
                            <a href="https://develop.rentoncloud.co/terms-of-use">
                              Terms
                            </a>{" "}
                            &{" "}
                            <a href="https://develop.rentoncloud.co/privacy-policy">
                              Privacy Policy
                            </a>
                          </label>
                        </div>

                        <div className="form-group mb-0">
                          <button
                            type="submit"
                            className="btn btn-primary  btn-block"
                          >
                            Signup
                          </button>
                        </div>
                      </Form>
                    </div>
                  </>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withApollo(withRouter(Register));
