import React from "react";
import LockScreen from "../../Public/LockScreen/";
import ShowLoadingMessage from "../../../config/ShowLoadingMessage";
import { Select } from "antd";

import Dashboard from "./Dashboard";
import SettingsTab from "../../Common/SettingsTabs";
import ImpersonatingUserExist from "../../Common/Impersonator";

import { Link, Switch, Route, Redirect } from "react-router-dom";
import { Layout, Menu, Button, Popover, Dropdown } from "antd";
import "../style.scss";
import UserContext from "../../../config/UserContext";
import AccountQueries from "../../../config/queries/account";
import cookie from "react-cookies";
import _ from "lodash";
import { withApollo } from "react-apollo";
const { SubMenu } = Menu;
const { Header, Sider, Content } = Layout;
const { Option } = Select;

const SettingTabsNames = [
  {
    key: "info",
    title: "Profile"
  },
  {
    key: "persona",
    title: "Persona Profile"
  },
  {
    key: "accountsetting",
    title: "Account Setting"
  },
  {
    key: "privacy",
    title: "Privacy"
  },
  {
    key: "security",
    title: "Security"
  },
  {
    key: "subscriptions",
    title: "Subscriptions"
  },
  {
    key: "notifications",
    title: "Notifications"
  },
  {
    key: "userRole",
    title: "User Role"
  },
  {
    key: "chartOfAccount",
    title: "Chart Of Account"
  }
];
class RenterMain extends React.PureComponent {
  static contextType = UserContext;

  constructor(props) {
    super(props);

    this.state = {
      collapsed: true
    };
  }

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  changeRole = async role => {
    this.context.startLoading();
    let roleToSet = role;
    const checkUserQuery = await this.props.client.query({
      query: AccountQueries.changeRole,
      variables: { role: roleToSet }
    });

    if (
      !_.isEmpty(checkUserQuery.data.changeRole) &&
      _.get(checkUserQuery, "data.changeRole.success")
    ) {
      cookie.remove("token", { path: "/" });

      cookie.save("token", _.get(checkUserQuery, "data.changeRole.token"), {
        path: "/"
      });
      localStorage.setItem("currentRole", roleToSet);

      window.location.reload();
    }
  };

  logout = () => {
    cookie.remove("token", { path: "/" });
    window.location.reload();
  };

  render() {
    let userData = _.get(this.context, "userData.authentication.data");
    let userRole = _.get(this.context, "userData.authentication.data.role");
    const { match } = this.props;
    let { collapsed } = this.state;
    let pathname = window.location.pathname.split(`/${userRole}/settings/`)[1];
    let activeTabTitle = _.find(SettingTabsNames, { key: pathname });
    let userDataMain = _.get(this.context, "userData.authentication");

    const menu = (
      <Menu>
        <Menu.Item key="0">
          <Link to={`/${userRole}/settings/info`}>
            <i className="fa fa-user mr-2" /> Profile
          </Link>
        </Menu.Item>
        <Menu.Item key="1">
          <Link to={`/${userRole}/settings`}>
            <i className="fa fa-cog mr-2" /> Settings
          </Link>
        </Menu.Item>
        <Menu.Item key="2">
          <span onClick={this.logout}>
            <i className="fa fa-unlock mr-2" /> Logout
          </span>
        </Menu.Item>
      </Menu>
    );

    const notifications = (
      <div className="notify_home">
        <ul>
          <li>
            {" "}
            Lorem Ipsum dolor sit{" "}
            <span>
              <a href>
                <i className="far fa-eye" />
              </a>{" "}
              <a href>
                <i className="far red fa-trash-alt" />
              </a>
            </span>
          </li>
          <li>
            {" "}
            Lorem Ipsum dolor sit{" "}
            <span>
              <a href>
                <i className="far fa-eye" />
              </a>{" "}
              <a href>
                <i className="far red fa-trash-alt" />
              </a>
            </span>
          </li>
          <li>
            {" "}
            Lorem Ipsum dolor sit{" "}
            <span>
              <a href>
                <i className="far fa-eye" />
              </a>{" "}
              <a href>
                <i className="far red fa-trash-alt" />
              </a>
            </span>
          </li>
        </ul>
        <a href className="d-block view text-center mt-2">
          View All
        </a>
      </div>
    );

    const searchBar = (
      <>
        <div className="search__listing">
          <ul>
            <li className="active__tab">Rent</li>
            <li>Task</li>
            <li>Landlord</li>
            <li>ServicePro</li>
          </ul>
          <span className="search__listing--closer">
            <i className="fa fa-times"></i>
          </span>
        </div>
      </>
    );

    return (
      <>
        {/* <LockScreen {...this.props} /> */}
        <div>
          <Layout>
            <Header className="main__topbar">
              <Button
                type="primary"
                onClick={this.toggleCollapsed}
                className="menuIcon"
              >
                {/* <Icon type={this.state.collapsed ? "menu-unfold" : "menu-fold"} /> */}
                <i className="fa fa-bars"></i>
              </Button>

              <img
                src={"/assets/images/logo-300.png"}
                alt=""
                className="header__logo"
              />

              <form className="search__wrapper">
                <Popover
                  placement="bottomLeft"
                  title={null}
                  content={searchBar}
                  trigger="click"
                >
                  <input
                    className="form-control"
                    type="search"
                    placeholder="Search"
                    aria-label="Search"
                  />
                </Popover>
                <button className="btn" type="submit">
                  <i className="mdi mdi-magnify" />
                </button>
              </form>

              {/* <p>Logged in as - {_.get(userData, "role")}</p> */}

              <ul className="navbar-nav ml-auto">
                {userDataMain.isImpersonate && (
                  <>
                    <li>
                      <ImpersonatingUserExist>
                        You are impersonating as {userData.firstName}
                      </ImpersonatingUserExist>
                    </li>
                  </>
                )}

                {!userDataMain.isImpersonate && (
                  <li>
                    <Select
                      defaultValue="renter"
                      style={{ width: 120 }}
                      onChange={this.changeRole}
                    >
                      <Option value="servicepro">Service Pro</Option>
                      <Option value="renter" disabled>
                        Renter
                      </Option>
                      <Option value="landlord">Landlord</Option>
                    </Select>
                  </li>
                )}
                <li className="nav-item">
                  <a href className="nav-a">
                    <span className="fa fa-calendar" />
                  </a>
                </li>
                <li className="nav-item">
                  <a href className="nav-a">
                    <span className="fa fa-comments" />
                  </a>
                </li>
                <li className="nav-item dropdown notification-menu">
                  <Popover
                    placement="bottom"
                    title={null}
                    content={notifications}
                    trigger="click"
                  >
                    <i className="fa fa-bell" />
                    <span className="badge badge-warning navbar-badge">15</span>
                  </Popover>
                </li>
                <li className="nav-item">
                  <Link to={`/${userRole}/settings/info`}>
                    <span className="fa fa-cog" />
                  </Link>
                </li>
                <li className="nav-item dropdown user-menu">
                  <Dropdown overlay={menu} trigger={["click"]}>
                    <a href className="nav-a user-menu-a">
                      <span className="profile-text">
                        Hi,{" "}
                        <span className="profile__name">
                          {userData.firstName}!
                        </span>
                      </span>
                      <img
                        alt={`${userData.firstName}'s profile pic`}
                        className="profile__thumb"
                        src={
                          userData.avatar
                            ? userData.avatar
                            : "/assets/images/avatar.jpg"
                        }
                        style={{ width: "32px", marginLeft: "7px" }}
                      />
                    </a>
                  </Dropdown>
                </li>
              </ul>
            </Header>

            <Layout>
              <Sider
                className="menu__sidebar menu__sidebar--renter"
                width={250}
                collapsed={this.state.collapsed}
              >
                <div>
                  <Menu defaultOpenKeys={["sub1"]} mode="inline" theme="light">
                    <Menu.Item key="1">
                      <Link to={`/${userRole}/dashboard`}>
                        <i className="mdi mdi-home" />
                        <span className="padd__left">Dashboard</span>
                      </Link>
                    </Menu.Item>

                    <Menu.Item key="2">
                      <i className="mdi mdi-currency-gbp" />
                      <span className="padd__left">Manage Rent</span>
                    </Menu.Item>

                    <Menu.Item key="3">
                      <i className="mdi mdi-home-account"></i>
                      <span className="padd__left">My Rental</span>
                    </Menu.Item>

                    <SubMenu
                      key="sub1"
                      title={
                        <span>
                          <i className="mdi mdi-hand-pointing-right"></i>
                          <span className="padd__left">FixIt</span>
                        </span>
                      }
                    >
                      <Menu.Item key="5">Task</Menu.Item>
                      <Menu.Item key="6">Reminders</Menu.Item>
                    </SubMenu>

                    <Menu.Item key="7">
                      <i className="mdi mdi-account-box-outline"></i>
                      <span className="padd__left">Applications</span>
                    </Menu.Item>

                    <Menu.Item key="8">
                      <i className="mdi mdi-file-search-outline"></i>
                      <span className="padd__left">Screening</span>
                    </Menu.Item>

                    <Menu.Item key="9">
                      <i className="mdi mdi-account-heart-outline"></i>
                      <span className="padd__left">Rental Wishlist</span>
                    </Menu.Item>

                    <Menu.Item key="10" className="fixed__btm--settings">
                      <Link to={`/${userRole}/settings`}>
                        <i className="mdi mdi-settings"></i>
                        <span className="padd__left">Settings</span>
                      </Link>
                    </Menu.Item>

                    <Menu.Item key="11" className="fixed__btm--support">
                      <i className="mdi mdi-headphones"></i>
                      <span className="padd__left">Support</span>
                    </Menu.Item>

                    <div className="used_space text-center fixed__btm--progressbar">
                      <span>
                        <a>Used 2.3/5 GB</a>
                      </span>
                      <div className="progress">
                        <div
                          className="progress-bar"
                          role="progressbar"
                          aria-valuenow="25"
                          aria-valuemin="0"
                          aria-valuemax="100"
                          style={{ width: "25%" }}
                        ></div>
                      </div>
                    </div>
                  </Menu>
                </div>
              </Sider>

              <Content
                className={
                  !collapsed
                    ? "right__container--part right__container--padding"
                    : "right__container--part"
                }
              >
                {/* BreadCrumb Data */}
                <div className="dashboard__header">
                  <div className="row">
                    <div className="col-lg-6">
                      <h2>{activeTabTitle && activeTabTitle.title}</h2>
                    </div>
                    <div className="col-lg-6">
                      <ol className="breadcrumb">
                        <li className="breadcrumb-item">
                          <a href>Home</a>
                        </li>
                        <li className="breadcrumb-item">
                          <a href>Settings</a>
                        </li>
                        <li className="breadcrumb-item active">
                          {activeTabTitle && activeTabTitle.title}
                        </li>
                      </ol>
                    </div>
                  </div>
                </div>
                <Switch>
                  <Route exact path={`${match.url}`}>
                    <Redirect to={`${match.url}/dashboard`} />
                  </Route>
                  <Route path={`${match.url}/dashboard`}>
                    <Dashboard userData={userData} {...this.props} />
                  </Route>
                  <Route path={`${match.url}/settings`}>
                    <SettingsTab {...this.props} />
                  </Route>
                </Switch>
              </Content>
            </Layout>
          </Layout>
        </div>
      </>
    );
  }
}

export default withApollo(RenterMain);
