let Landlord = [
  {
    name: "dashboard",
    title: "Dashboard"
  },
  {
    name: "properties",
    title: "Properties"
  },
  {
    name: "listings",
    title: "Listings"
  },
  {
    name: "applications",
    title: "Applications"
  },
  {
    name: "screening",
    title: "Screening"
  },
  {
    name: "tenancies",
    title: "Tenancies"
  },
  {
    name: "accounting",
    title: "Accounting"
  },
  {
    name: "report",
    title: "Report"
  },
  {
    name: "fixit",
    title: "Fix It"
  }
];

let ServicePro = [
  {
    name: "dashboard",
    title: "Dashboard"
  },
  {
    name: "findit",
    title: "Find It"
  },
  {
    name: "fixit",
    title: "Fix It"
  },
  {
    name: "invoice",
    title: "Invoice"
  },
  {
    name: "mymoney",
    title: "My Money"
  },
  {
    name: "myclient",
    title: "My Client"
  },
  {
    name: "diary",
    title: "Diary"
  }
];

let Renter = [
  {
    name: "dashboard",
    title: "Dashboard"
  },
  {
    name: "managerent",
    title: "Manage Rent"
  },
  {
    name: "myrental",
    title: "My Rental"
  },
  {
    name: "fixit",
    title: "Fix It"
  },
  {
    name: "applications",
    title: "Applicaitons"
  },
  {
    name: "screening",
    title: "Screening"
  },
  {
    name: "wishlist",
    title: "Rental Wishlist"
  }
];

export default { Renter, Landlord, ServicePro };
