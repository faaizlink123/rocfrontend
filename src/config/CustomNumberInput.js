import NumberFormat from "react-number-format";
import React from "react";

class MyNumberInput extends React.Component {
  state = {
    value: ""
  };
  render() {
    return (
      <NumberFormat
        placeholder="Number Format Input looses focus"
        isNumericString={true}
        allowNegative={false}
        thousandSeparator={true}
        value={this.state.value}
        onValueChange={vals => this.setState({ value: vals.formattedValue })}
        {...this.props}
      />
    );
  }
}

export default MyNumberInput;
