import { gql } from "apollo-boost";

const registerUser = gql`
  mutation registerUser($email: String!, $password: String!, $role: String!) {
    registration(email: $email, password: $password, role: $role) {
      success
      message
      data {
        firstName
        lastName
        password
        email
        role
      }
    }
  }
`;

export default registerUser;
