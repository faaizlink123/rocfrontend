import { gql } from "apollo-boost";

const checkEmail = gql`
  query checkEmail($email: String!) {
    checkEmail(email: $email) {
      success
      message
      email
      firstName
      isMFA
      otp
    }
  }
`;

let checkAuth = gql`
  {
    authentication {
      success
      isImpersonate
      permissions
      message
      data {
        firstName
        lastName
        email

        dob
        invitedOn
        selectedPlan
        gender
        avatar
        nationality
        role
        password
      }
    }
  }
`;

let checkOtp = gql`
  query checkOtp($email: String!, $otp: String!) {
    checkOTP(email: $email, otp: $otp) {
      success
      message
      isOtpCorrect
    }
  }
`;

let resendOTP = gql`
  query resendOTP($email: String!) {
    resendOTP(email: $email) {
      success
      message
      isMFA
      email
      firstName
      otp
    }
  }
`;

let loginUser = gql`
  query login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      success
      message
      token
      data {
        firstName
        lastName
        email
        role
      }
    }
  }
`;

let socialLogin = gql`
  mutation socialLogin(
    $firstName: String!
    $lastName: String!
    $email: String!
    $socialId: String!
  ) {
    socialLogin(
      firstName: $firstName
      lastName: $lastName
      email: $email
      socialId: $socialId
    ) {
      success
      message
      data {
        firstName
        lastName
        email
        role
      }
    }
  }
`;

let checkPassword = gql`
  query checkPassword($password: String!) {
    checkPassword(password: $password) {
      success
      message
    }
  }
`;

let forgotPassword = gql`
  mutation forgotPassword($email: String!) {
    forgotPassword(email: $email) {
      success
      message
    }
  }
`;

let resetPassword = gql`
  mutation resetPassword($password: String!, $token: String!) {
    resetPassword(password: $password, token: $token) {
      success
      message
    }
  }
`;

let getUserInformation = gql`
  query getUserInformation($token: String!) {
    getUserInformation(token: $token) {
      success
      message
      data
    }
  }
`;

let obj = {
  checkEmail,
  checkAuth,
  checkOtp,
  loginUser,
  socialLogin,
  checkPassword,
  resendOTP,
  forgotPassword,
  resetPassword,
  getUserInformation
};

export default obj;
