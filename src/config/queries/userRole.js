import { gql } from "apollo-boost";

const fetchUserRoles = gql`
  query getAccessRoles {
    getAccessRoles {
      success
      data {
        _id
        name
        title
        description
        permissions {
          tab
          read
          write
        }
      }
      message
    }
  }
`;

const inviteUser = gql`
  mutation sendInvitation($name: String!, $email: String!, $roleId: String!) {
    sendInvitation(name: $name, email: $email, roleId: $roleId) {
      success
      message
      data
    }
  }
`;

const acceptInvitation = gql`
  query acceptInvitation($token: String!) {
    acceptInvitation(token: $token) {
      success
      data
      message
    }
  }
`;

const impersonateUser = gql`
  query impersonateUser($userId: String!) {
    impersonateUser(userId: $userId) {
      success
      data {
        firstName
        avatar
        lastName
        invitedOn
        phoneNumber
        email
        gender
        selectedPlan
        nationality
        defaultRole
        profileCompleteness
        role
        facebookLink
        linkedInLink
      }
      token
      message
    }
  }
`;

export default {
  fetchUserRoles,
  inviteUser,
  acceptInvitation,
  impersonateUser
};
