import { gql } from "apollo-boost";

let resetPassword = gql`
  mutation changePassword($currentPassword: String!, $newPassword: String!) {
    changePassword(
      currentPassword: $currentPassword
      newPassword: $newPassword
    ) {
      success
      message
    }
  }
`;

let changeRole = gql`
  query changeRole($role: String!) {
    changeRole(role: $role) {
      success
      message
      token
      data {
        firstName
        lastName
      }
    }
  }
`;

let setNotificationPreferences = gql`
  mutation(
    $appointment: Notification!
    $taskReminder: Notification!
    $screeningReport: Notification!
    $newsLetter: Notification!
  ) {
    updateNotificationInformation(
      appointment: $appointment
      taskReminder: $taskReminder
      screeningReport: $screeningReport
      newsLetter: $newsLetter
    ) {
      message
      success
      data
    }
  }
`;

let fetchNotificationPreferences = gql`
  query fetchNotificationPreferences {
    notifications {
      success
      message
      data
    }
  }
`;

let updateConnectInformation = gql`
  mutation updateConnectInformation($connectInput: ConnectInput!) {
    updateConnectInformation(connectInput: $connectInput) {
      success
      message
      data {
        linkedInLink
        facebookLink
        twitterLink
        telegramLink
        addressLine1
        addressLine2
        city
        zip
        state
        country
      }
    }
  }
`;

let fetchConnectProfile = gql`
  query getConnectInformation {
    getConnectInformation {
      success
      message
      data {
        linkedInLink
        facebookLink
        twitterLink
        telegramLink
        addressLine1
        addressLine2
        city
        zip
        state
        country
      }
    }
  }
`;

let updateProfileAbout = gql`
  mutation updateProfileAbout(
    $firstName: String!
    $lastName: String!
    $avatar: String!
    $middleName: String
    $email: String!
    $gender: String!
    $isCompany: Boolean
    $dob: String!
    $companyName: String
    $companyRegistrationNumber: String
    $phoneNumber: String!
    $nationality: String!
  ) {
    updateProfileInformation(
      profileInput: {
        firstName: $firstName
        avatar: $avatar
        middleName: $middleName
        lastName: $lastName
        email: $email
        gender: $gender
        isCompany: $isCompany
        dob: $dob
        companyName: $companyName
        companyRegistrationNumber: $companyRegistrationNumber
        phoneNumber: $phoneNumber
        nationality: $nationality
      }
    ) {
      success
      message
      data {
        firstName
        lastName
      }
    }
  }
`;

let fetchProfileAbout = gql`
  query getProfileInformation {
    getProfileInformation {
      success
      message
      data {
        firstName
        avatar
        middleName
        lastName
        email
        gender
        isCompany
        dob
        companyName
        companyRegistrationNumber
        phoneNumber
        nationality
      }
    }
  }
`;

let updateAccountPreferences = gql`
  mutation updateAccountSetting(
    $currency: String
    $timeZone: String
    $timeFormat: String
    $dateFormat: String
    $measurementUnit: String
  ) {
    updateAccountSetting(
      currency: $currency
      timeZone: $timeZone
      timeFormat: $timeFormat
      dateFormat: $dateFormat
      measurementUnit: $measurementUnit
    ) {
      message
      success
      data
    }
  }
`;

let fetchAccountPreferences = gql`
  query fetchAccountPreferences {
    getAccountSetting {
      success
      message
      data
    }
  }
`;

let fetchPrivacyInformation = gql`
  query getPrivacyInformation {
    getPrivacyInformation {
      success
      message
      data
    }
  }
`;

let updatePrivacyData = gql`
  mutation updatePrivacyInformation(
    $profilePicture: Privacy!
    $gender: Privacy!
    $age: Privacy!
    $work: Privacy!
    $references: Privacy!
    $selfDeclaration: Privacy!
    $socialConnect: Privacy!
  ) {
    updatePrivacyInformation(
      profilePicture: $profilePicture
      gender: $gender
      age: $age
      references: $references
      work: $work
      selfDeclaration: $selfDeclaration
      socialConnect: $socialConnect
    ) {
      success
      message
      data
    }
  }
`;

let addCardDetails = gql`
  query verifyCardDetail(
    $cardNumber: String!
    $expireMonth: Int!
    $expireYear: Int!
    $cvc: String!
  ) {
    verifyCardDetail(
      cardNumber: $cardNumber
      expireMonth: $expireMonth
      expireYear: $expireYear
      cvc: $cvc
    ) {
      success
      message
    }
  }
`;

let saveBankDetails = gql`
  mutation updateBankDetail($bankCode: String!, $accountNumber: String!) {
    updateBankDetail(bankCode: $bankCode, accountNumber: $accountNumber) {
      success
      message
    }
  }
`;

let getBankDetail = gql`
  query getBankDetail {
    getBankDetail {
      success
      message
      data
    }
  }
`;

let getSubscriptionPlans = gql`
  query getPlanDetails {
    getPlanDetails {
      success
      message
      data {
        _id
        planId
        amount
        interval
        displayName
      }
    }
  }
`;

let buySubscription = gql`
  mutation createUserSubscription($planId: String!) {
    createUserSubscription(planId: $planId) {
      success
      message
      data
    }
  }
`;

let addChartOfAccount = gql`
  mutation createChartOfAccount(
    $accountType: String!
    $accountName: String!
    $category: String!
  ) {
    createChartOfAccount(
      accountType: $accountType
      accountName: $accountName
      category: $category
    ) {
      success
      message
    }
  }
`;

let fetchChartOfAccount = gql`
  query getChartOfAccount {
    getChartOfAccount {
      message
      success
      data {
        capitalInflow {
          _id
          accountType
          accountName
          category
          isCreatedByAdmin
        }
        businessExpenses {
          _id
          accountType
          accountName
          category
          isCreatedByAdmin
        }

        capitalOverflow {
          _id
          accountType
          accountName
          category
          isCreatedByAdmin
        }
        businessIncome {
          _id
          accountType
          accountName
          category
          isCreatedByAdmin
        }
      }
    }
  }
`;

let updateChartOfAccount = gql`
  mutation updateChartOfAccount(
    $chartId: String!
    $accountType: String!
    $accountName: String!
    $category: String!
  ) {
    updateChartOfAccount(
      chartId: $chartId
      accountType: $accountType
      accountName: $accountName
      category: $category
    ) {
      message
      success
      data {
        capitalInflow {
          accountType
          accountName
          category
          isCreatedByAdmin
        }
        businessExpenses {
          accountType
          accountName
          category
          isCreatedByAdmin
        }

        capitalOverflow {
          accountType
          accountName
          category
          isCreatedByAdmin
        }
        businessIncome {
          accountType
          accountName
          category
          isCreatedByAdmin
        }
      }
    }
  }
`;

let fetchProfileCompleteness = gql`
  query getProfileCompleteness {
    getProfileCompleteness {
      success
      message
      data
    }
  }
`;

let deactivateAccount = gql`
  query deactivateAccount {
    deactivateAccount {
      success
      message
      data
    }
  }
`;

let deleteAccount = gql`
  query deleteAccount {
    deleteAccount {
      success
      message
      data
    }
  }
`;

export default {
  fetchProfileCompleteness,
  resetPassword,
  changeRole,
  setNotificationPreferences,
  fetchNotificationPreferences,
  updateProfileAbout,
  updateAccountPreferences,
  fetchAccountPreferences,
  fetchProfileAbout,
  updatePrivacyData,
  fetchPrivacyInformation,
  updateConnectInformation,
  fetchConnectProfile,
  addCardDetails,
  saveBankDetails,
  getBankDetail,
  getSubscriptionPlans,
  buySubscription,
  addChartOfAccount,
  fetchChartOfAccount,
  updateChartOfAccount,
  deactivateAccount,
  deleteAccount
};
