let ar = [
  {
    title: "Dashboard",

    htmlString: `
              <Link to={"/landlord/dashboard"}>
                        <i className="mdi mdi-home" />
                        <span>Dashboard</span>
                      </Link>
            `
  },
  {
    title: "Properties",
    htmlString: `
                                  <i className="mdi mdi-office-building" />
                      <span>Properties</span>
            `
  },
  {
    title: "Listings",
    htmlString: `
                                  <i className="mdi mdi-format-list-numbered"></i>
                      <span>Listings</span>
            `
  },
  {
    title: "Applications",
    htmlString: `
                                  <i className="mdi mdi-account-box-outline"></i>
                      <span>Applications</span>
            `
  },
  {
    title: "Screening",
    htmlString: `
                                  <i className="mdi mdi-file-search-outline"></i>
                      <span>Screening</span>
            `
  },
  {
    title: "Tenancies",
    htmlString: `
                                <SubMenu
                      key="sub1"
                      title={
                        <span>
                          <i className="mdi mdi-briefcase-account"></i>
                          <span>Tenancies</span>
                        </span>
                      }
                    >
                      <Menu.Item key="6">My Renter</Menu.Item>
                      <Menu.Item key="7">Agreement</Menu.Item>
                      <Menu.Item key="8">Documents</Menu.Item>
                      <Menu.Item key="9">Inventory</Menu.Item>
                    </SubMenu>
            `
  },
  {
    title: "Accounting",
    htmlString: `
                                <SubMenu
                      key="sub2"
                      title={
                        <span>
                          <i className="mdi mdi-scale-balance"></i>
                          <span>Accounting</span>
                        </span>
                      }
                    >
                      <Menu.Item key="10">Rental Invoice</Menu.Item>
                      <Menu.Item key="11">Transactions</Menu.Item>
                      <Menu.Item key="12">Deposits</Menu.Item>
                    </SubMenu>
            `
  },
  {
    title: "Report",
    htmlString: `
                              <i className="mdi mdi-chart-bar"></i>
                      <span>Report</span>
            `
  },
  {
    title: "Settings",
    htmlString: `
                                  <Link to={"/landlord/settings"}>
                        <i className="mdi mdi-settings"></i>
                        <span>Settings</span>
                      </Link>
            `
  }
];

export default ar;
