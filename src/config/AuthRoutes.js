import React from "react";
import { Route, Redirect } from "react-router-dom";
import PacmanLoader from "react-spinners/PropagateLoader";
import _ from "lodash";
import PropTypes from "prop-types";
let currentRole = localStorage.getItem("currentRole");

let loaderStyling = {
  height: "100vh",
  width: "100vw",
  display: "flex",
  justifyContent: "center",
  alignItems: "center"
};

const LandlordRoute = ({ component, ...rest }) => {
  let userData;
  let isAuthed;
  let userType;
  if (!_.isEmpty(rest.userData)) {
    userData = _.get(rest, "userData.authentication");

    isAuthed = _.get(rest, "userData.authentication.success");
    userType = _.get(rest, "userData.authentication.data.role");
  }

  if (!_.isEmpty(rest.userData)) {
    return (
      <Route
        {...rest}
        exact
        render={props =>
          userData && userType === "landlord" && isAuthed ? (
            <div>{React.createElement(component, props)}</div>
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: props.location }
              }}
            />
          )
        }
      />
    );
  } else {
    return (
      <div style={loaderStyling}>
        <PacmanLoader
          sizeUnit={"px"}
          size={18}
          color={"#123abc"}
          loading={true}
        />
      </div>
    );
  }
};

LandlordRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.func]).isRequired,
  location: PropTypes.object
};

// Renter Route
const RenterRoute = ({ component, ...rest }) => {
  let userData;
  let isAuthed;
  let userType;
  if (!_.isEmpty(rest.userData)) {
    userData = _.get(rest, "userData.authentication");

    isAuthed = _.get(rest, "userData.authentication.success");
    userType = _.get(rest, "userData.authentication.data.role");
  }

  if (!_.isEmpty(rest.userData)) {
    return (
      <Route
        {...rest}
        exact
        render={props =>
          userData && userType === "renter" && isAuthed ? (
            <div>{React.createElement(component, props)}</div>
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: props.location }
              }}
            />
          )
        }
      />
    );
  } else {
    return (
      <div style={loaderStyling}>
        <PacmanLoader
          sizeUnit={"px"}
          size={150}
          color={"#123abc"}
          loading={true}
        />
      </div>
    );
  }
};

RenterRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.func]).isRequired,
  location: PropTypes.object
};

// Service Pro Route

const ServiceProRoute = ({ component, ...rest }) => {
  let userData;
  let isAuthed;
  let userType;
  if (!_.isEmpty(rest.userData)) {
    userData = _.get(rest, "userData.authentication");

    isAuthed = _.get(rest, "userData.authentication.success");
    userType = _.get(rest, "userData.authentication.data.role");
  }

  if (!_.isEmpty(rest.userData)) {
    return (
      <Route
        {...rest}
        exact
        render={props =>
          userData && userType === "servicepro" && isAuthed ? (
            <div>{React.createElement(component, props)}</div>
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: props.location }
              }}
            />
          )
        }
      />
    );
  } else {
    return (
      <div style={loaderStyling}>
        <PacmanLoader
          sizeUnit={"px"}
          size={150}
          color={"#123abc"}
          loading={true}
        />
      </div>
    );
  }
};

ServiceProRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.func]).isRequired,
  location: PropTypes.object
};

// Site Admin Route

const SiteAdminRoute = ({ component, ...rest }) => {
  let userData;
  let isAuthed;
  let userType;
  if (!_.isEmpty(rest.userData)) {
    userData = _.get(rest, "userData.authentication");

    isAuthed = _.get(rest, "userData.authentication.success");
    userType = _.get(rest, "userData.authentication.data.role");
  }

  if (!_.isEmpty(rest.userData)) {
    return (
      <Route
        {...rest}
        exact
        render={props =>
          userData && userType === "admin" && isAuthed ? (
            <div>{React.createElement(component, props)}</div>
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: props.location }
              }}
            />
          )
        }
      />
    );
  } else {
    return (
      <div style={loaderStyling}>
        <PacmanLoader
          sizeUnit={"px"}
          size={150}
          color={"#123abc"}
          loading={true}
        />
      </div>
    );
  }
};

SiteAdminRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.func]).isRequired,
  location: PropTypes.object
};

// Impoersonator Route

const ImpoersonatorRoute = ({ component, ...rest }) => {
  let userData;
  let isAuthed;
  let userType;
  if (!_.isEmpty(rest.userData)) {
    userData = _.get(rest, "userData.authentication");

    isAuthed = _.get(rest, "userData.authentication.success");
    userType = _.get(rest, "userData.authentication.data.role");
  }

  if (!_.isEmpty(rest.userData)) {
    return (
      <Route
        {...rest}
        exact
        render={props =>
          userData && userType === "invitee" && isAuthed ? (
            <div>{React.createElement(component, props)}</div>
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: props.location }
              }}
            />
          )
        }
      />
    );
  } else {
    return (
      <div style={loaderStyling}>
        <PacmanLoader
          sizeUnit={"px"}
          size={150}
          color={"#123abc"}
          loading={true}
        />
      </div>
    );
  }
};

ImpoersonatorRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.func]).isRequired,
  location: PropTypes.object
};

export default {
  LandlordRoute,
  RenterRoute,
  ServiceProRoute,
  SiteAdminRoute,
  ImpoersonatorRoute
};
