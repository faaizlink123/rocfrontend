import axios from "axios";
const findAddress = async address => {
  let addressResponse = await axios.get(
    `https://api.addressy.com/Capture/Interactive/Find/v1.10/json3ex.ws?Key=${process.env.REACT_APP_LOQATE_API_KEY}&Text=${address}&IsMiddleware=True&Limit=10&Language=`,
    {
      headers: {
        "Content-Type": "application/json"
      }
    }
  );

  return addressResponse.data;
};

export default findAddress;
