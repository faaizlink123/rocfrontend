import React from "react";
import { Field } from "formik";

const CustomInput = props => {
  return (
    <Field
      {...props}
      className={
        props.errors && props.errors[props.name]
          ? "tab__deatils--input error__field_show"
          : "tab__deatils--input"
      }
    />
  );
};

export default CustomInput;
