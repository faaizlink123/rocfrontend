import * as Yup from "yup";

const EmailCheckSchema = Yup.object().shape({
  email: Yup.string()
    .email("Invalid email")
    .required("Email is Required!")
});

const PasswordSchema = Yup.object().shape({
  password: Yup.string()
    .min(6, "Too Short!")
    .required("Required")
});

export default { PasswordSchema, EmailCheckSchema };
