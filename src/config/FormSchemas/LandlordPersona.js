import * as Yup from "yup";

const ProfessionSchema = Yup.object().shape({
  profession: Yup.string().required("Profession is Required!"),
  professionCustom: Yup.string().optional(),

  jobType: Yup.string().required("Job type is Required!"),
  jobTitle: Yup.string().required("Job title is required!"),
  companyName: Yup.string().required("Company Name is required!"),
  companyWebsite: Yup.string()
    .url()
    .required("Company Website is required"),
  companyTelephone: Yup.string()
    .required("Phone Number is required!")
    .min(10, "Too Short!"),
  startDate: Yup.date().required("Date of Birth is required!")
});

const AccrediationSchema = Yup.object().shape({
  organization: Yup.string().required("Organisation is Required!"),
  organizationCustom: Yup.string().optional(),

  documentNumber: Yup.string().required("Document Name is Required!"),
  startDate: Yup.date().required("Date of Birth is required!")
});

const ProfessionSchemaServicePro = Yup.object().shape({
  profession: Yup.string().required("Profession is Required!"),
  professionCustom: Yup.string().optional(),
  businessType: Yup.string().required("Business type is Required!"),
  UTR: Yup.string().required("UTR is Required!"),
  VAT: Yup.string().required("VAT is required!"),
  companyName: Yup.string().required("Company Name is required!"),
  startDate: Yup.date().required("Start date is required!")
});

export default {
  ProfessionSchema,
  AccrediationSchema,
  ProfessionSchemaServicePro
};
