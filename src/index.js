import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { ApolloProvider } from "react-apollo";
import ApolloClient from "apollo-boost";
import { withClientState } from "apollo-link-state";
import cookie from "react-cookies";
import { ApolloProvider as ApolloHooksProvider } from "@apollo/react-hooks";

import { InMemoryCache } from "apollo-cache-inmemory";
import "antd/dist/antd.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import "./index.scss";

const BASE_URL = `${process.env.REACT_APP_SERVER}/graphql`;
const cache = new InMemoryCache({ addTypename: false });

const localClientStates = withClientState({});

export const client = new ApolloClient({
  uri: BASE_URL,
  request: async operation => {
    const token = await cookie.load("token");
    operation.setContext({
      headers: {
        authorization: token ? `${token}` : ""
      }
    });
  },
  cache,
  clientState: {
    localClientStates
  }
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <ApolloHooksProvider client={client}>
      <App />
    </ApolloHooksProvider>
  </ApolloProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
