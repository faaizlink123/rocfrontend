import React from "react";
import { Router, Switch, Route } from "react-router-dom";
import { createBrowserHistory } from "history";
import Login from "./components/Public/Login";
import NoMatch from "./components/Public/NoMatch";
import Register from "./components/Public/Register/";
import SiteAdmin from "./components/Private/MainAdmin";
import LandlordMain from "./components/Private/Landlord";
import RenterMain from "./components/Private/Renter";
import ServiceProMain from "./components/Private/ServicePro";
import UserContext from "./config/UserContext";
import TestAdd from "./components/Common/SettingsTabs/ProfileInfo/Connect";
import ResetPasswordInvite from "./components/Public/ResetPasswordInvite";
import ResetPasswordMain from "./components/Public/ResetPassword";
import AcceptInvitation from "./components/Public/AcceptInvitationSupport";
import SupportUser from "./components/Private/Impersonators";
import { LinkedInPopUp } from "react-linkedin-login-oauth2";
import { useQuery } from "@apollo/react-hooks";
import UserQuery from "./config/queries/login";
import AuthRoutes from "./config/AuthRoutes";
import NProgress from "nprogress";
import _ from "lodash";
import { withApollo } from "react-apollo";

const history = createBrowserHistory();

const App = props => {
  const { loading, data } = useQuery(UserQuery.checkAuth);

  const startLoading = val => {
    NProgress.start();
  };

  const endLoading = val => {
    NProgress.done();
  };

  const checkUser = async () => {
    const checkUserQuery = await props.client.query({
      query: UserQuery.checkAuth
    });

    if (
      !_.isEmpty(checkUserQuery.data) &&
      _.get(checkUserQuery, "data.authentication.success")
    ) {
      let userData = _.get(checkUserQuery, "data.authentication.data");

      window.location.pathname = `/${userData.role}`;
    }
  };

  // const [isLoaderVisible, setLoaderState] = useState(false);

  return (
    <UserContext.Provider value={{ userData: data, startLoading, endLoading }}>
      <div className="App">
        <Router history={history}>
          <Switch>
            {/* Public Routes */}
            <Route exact path="/">
              <Login
                checkUser={checkUser}
                startLoading={startLoading}
                endLoading={endLoading}
                {...props}
              />
            </Route>
            <Route exact path="/login">
              <Login
                checkUser={checkUser}
                startLoading={startLoading}
                endLoading={endLoading}
                {...props}
              />
            </Route>

            <Route exact path="/address">
              <TestAdd
                checkUser={checkUser}
                startLoading={startLoading}
                endLoading={endLoading}
                {...props}
              />
            </Route>

            <Route exact path="/register">
              <Register
                startLoading={startLoading}
                endLoading={endLoading}
                {...props}
              />
            </Route>

            <Route exact path="/forgotpassword">
              <ResetPasswordInvite
                startLoading={startLoading}
                endLoading={endLoading}
                {...props}
              />
            </Route>

            <Route exact path="/auth/reset-password/:token">
              <ResetPasswordMain
                startLoading={startLoading}
                endLoading={endLoading}
                {...props}
              />
            </Route>

            <Route exact path="/auth/accept-invitation/:token">
              <AcceptInvitation
                startLoading={startLoading}
                endLoading={endLoading}
                {...props}
              />
            </Route>

            <Route path="/linkedin" component={LinkedInPopUp} />

            {/* Admin Routes */}
            <AuthRoutes.LandlordRoute
              userData={data ? data : {}}
              path="/landlord"
              loading={loading}
              component={LandlordMain}
              {...props}
            />

            <AuthRoutes.RenterRoute
              userData={data ? data : {}}
              path="/renter"
              loading={loading}
              component={RenterMain}
              {...props}
            />

            <AuthRoutes.ServiceProRoute
              userData={data ? data : {}}
              path="/servicepro"
              loading={loading}
              component={ServiceProMain}
              {...props}
            />

            <AuthRoutes.SiteAdminRoute
              userData={data ? data : {}}
              path="/siteadmin"
              loading={loading}
              component={SiteAdmin}
              {...props}
            />

            <AuthRoutes.ImpoersonatorRoute
              userData={data ? data : {}}
              path="/invitee"
              loading={loading}
              component={SupportUser}
              {...props}
            />

            <Route component={NoMatch} />
          </Switch>
        </Router>
      </div>
    </UserContext.Provider>
  );
};

export default withApollo(App);
